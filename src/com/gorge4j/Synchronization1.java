package com.gorge4j;

/**
 * @Title: Synchronization1.java
 * @Description: [线程-线程同步] | 线程同步
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 23:51:46
 * @version v1.0
 */

class PrintThread extends Thread {
    char ch;

    public PrintThread(char ch) { // 构造函数，ch 为要输出的字符
        this.ch = ch;
    }

    void printCh10() {
        for (int i = 1; i <= 10; i++)
            System.out.print(ch);
    }

    public void run() {
        for (int i = 1; i <= 10; i++) {
            printCh10();
            System.out.println();
        }
    }
}


public class Synchronization1 {
    public static void main(String[] args) {
        PrintThread pt = new PrintThread('A');
        pt.start();
    }
}
