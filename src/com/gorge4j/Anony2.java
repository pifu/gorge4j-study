package com.gorge4j;

/**
 * @Title: Anony2.java
 * @Description: [内部类 -> 匿名类] | 匿名类定义
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

class Anony2_1 {
    public void hi() {
        System.out.println("hi?");
    }

    public void hello() {
        System.out.println("hello?");
    }
}


public class Anony2 {
    public static void main(String[] args) {
        Anony2_1 ob = new Anony2_1() { // 继承 Anony2_1 类的对象
            public void hi() {
                System.out.println("您好？");
            }
        };
        ob.hi();
        ob.hello();
    }
}
