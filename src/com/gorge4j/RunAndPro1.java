package com.gorge4j;

import com.gorge4j.util.OSUtil;

/**
 * @Title: RunAndPro1.java
 * @Description: [线程 -> Runtime类与Process类] | Runtime 类的常用方法
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class RunAndPro1 {
    public static void main(String[] args) throws Exception {
        Runtime rt = Runtime.getRuntime();
        Process pr = null;
        // 因为 Windows 系统跟 Mac OS 系统在文件路径上有差异，此处判断一下并做针对性的处理
        if (OSUtil.isWindows()) {
            pr = rt.exec("C:\\windows\\notepad.exe"); // Windows 系统下，打开“记事本”
        } else if (OSUtil.isMacOSX()) {
            pr = rt.exec("open /Applications/TextEdit.app"); // Mac OS 系统下，打开“文本编辑”，进入到创建文稿页面
        } else {
            throw new RuntimeException("抱歉，不支持的操作系统，目前仅支持 Windows 和 Mac OS X 平台");
        }

        System.out.println("最大内存：" + rt.maxMemory() + "bytes");
        System.out.println("总内存：" + rt.totalMemory() + "bytes");
        System.out.println("空闲内存：" + rt.freeMemory() + "bytes");
    }
}
