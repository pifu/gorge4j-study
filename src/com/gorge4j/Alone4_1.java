package com.gorge4j;

/**
 * @Title: Alone4_1.java
 * @Description: 需求：设计程序，计算语文（90）、数学（90）、英语（75）、科学（85）4 门课程的平均分数，并根据平均分作出相应评价
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 10:15:10
 * @version v1.0
 */

public class Alone4_1 {
    public static void main(String[] args) {
        int chinese = 90; // 语文分数
        int mathmatics = 90; // 数学分数
        int english = 75; // 英语分数
        int science = 85; // 科学分数
        int average = (chinese + mathmatics + english + science) / 4; // 计算各个科目的平均数
        String judgement = ""; // 评价
        if (average >= 95 && average <= 100) {
            judgement = "学霸";
        } else if (average >= 85 && average < 95) {
            judgement = "优秀";
        } else if (average >= 75 && average < 85) {
            judgement = "加油";
        } else if (average >= 65 && average < 75) {
            judgement = "努力";
        } else if (average >= 60 && average < 65) {
            judgement = "及格";
        } else {
            judgement = "不及格或无法判断";
        }
        System.out.println(judgement);
    }
}
