package com.gorge4j;

/**
 * @Title: Local2.java
 * @Description: [内部类 -> 局部类] | 在包含自身方法的局部变量与形式参数中，局部类仅能引用由 final 修饰符修饰得变量
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 12:55:54
 * @version v1.0
 */

public class Local2 {
    void f(final int a, int b) {
        int c = 30;
        final int d = 40;
        class Inner {
            void hi() {
                System.out.println(a);
                // System.out.println(b); // 错误，非final变量
                // System.out.println(c); // 错误
                System.out.println(d);
            }
        }
        Inner in = new Inner();
        in.hi();
    }

    public static void main(String[] args) {
        Local2 local = new Local2();
        local.f(10, 20);
    }
}
