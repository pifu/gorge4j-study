package com.gorge4j;

/**
 * @Title: MultiplicationTable2.java
 * @Description: [程序流程控制语句 -> for语句] | 非标准示例，九九乘法口诀表（格式更标准）
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 13:22:58
 * @version v1.0
 */

public class MultiplicationTable2 {
    public static void main(String[] args) {
        for (int a = 1; a <= 9; a++) {
            for (int b = 1; b <= a; b++) {
                if ((a == 3|| a == 4) && b == 2) {
                    System.out.print(b + "*" + a + "=" + a * b + "  ");
                } else {
                    System.out.print(b + "*" + a + "=" + a * b + " ");
                }
            }
            System.out.println("  ");
        }
    }
}
