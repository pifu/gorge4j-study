package com.gorge4j;

/**
 * @Title: If1.java
 * @Description: [程序流程控制语句 -> if语句格式1]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class If1 {
    public static void main(String[] args) {
        int a = 5;
        if (a > 0 && a < 10) {
            System.out.println("i 大于 0 且小于 10");
        }
        if (a == 6) {
            System.out.println("i 等于 6");
        }
    }
}
