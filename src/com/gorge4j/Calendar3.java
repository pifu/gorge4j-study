package com.gorge4j;

import java.util.*;

/**
 * @Title: Calendar3.java
 * @Description: [常用API之二 -> java.util.Calender类] | java.util.Calendar 日历时间类使用
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Calendar3 {
    public static void main(String[] args) {
        GregorianCalendar gc = new GregorianCalendar();
        final char[] CHINA_WEEK = {'日', '一', '二', '三', '四', '五', '六'}; // 重点理解，星期从 0 开始
        // 假如某个人的生日是阳历 9 月 16 日，输出 2002-2020 年之间他过生日时是星期几
        for (int i = 2012; i <= 2020; i++) {
            gc.set(i, Calendar.SEPTEMBER, 16);

            char week = CHINA_WEEK[gc.get(Calendar.DAY_OF_WEEK) - 1];
            System.out.println(i + "年的生日是星期" + week);
        }

    }
}
