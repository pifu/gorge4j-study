package com.gorge4j;

/**
 * @Title: Member2.java
 * @Description: [内部类 -> 成员类] | 成员类
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

class Outer2 {
    void f() {
        Inner in = new Inner(); // Outer 类的对象已经被创建
        System.out.println(in.a);
    }

    class Inner {
        int a = 100;
    }
}


public class Member2 {
    public static void main(String[] args) {
        Outer2 ob = new Outer2(); // Outer 类的对象再次被创建
        ob.f();
    }
}
