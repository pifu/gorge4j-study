package com.gorge4j;

/**
 * @Title: Arrays2.java
 * @Description: [常用API之二 -> java.util.Arrays类] | java.util.Arrays 类示例，对象比较
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

class Sagak implements Comparable<Object> { // 矩形类，实现 Comparable 接口
    private int width; // 宽
    private int height; // 高

    Sagak(int width, int height) { // 构造函数
        this.width = width;
        this.height = height;
    }

    int getArea() { // 求面积
        return width * height;
    }

    public int compareTo(Object o) { // 覆盖 CompareTo() 方法
        Sagak s = (Sagak) o; // 转换为矩形对象，此处着重理解
        return getArea() - s.getArea(); // 返回面积之差
    }

}


public class Arrays2 {
    public static void main(String[] args) {
        Sagak a = new Sagak(1, 2); // 面积为 1 * 2 的矩形
        Sagak b = new Sagak(2, 3); // 面积为 2 * 3 的矩形
        System.out.println(a.compareTo(b));
    }
}
