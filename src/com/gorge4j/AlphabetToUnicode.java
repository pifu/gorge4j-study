package com.gorge4j;

/**
 * @Title: AlphabetToUnicode.java
 * @Description: [Java语言的基本语法 -> 数据类型 -> 基本数据类型 -> 字符型] | 非标准示例，大小写字母转 Unicode 编码
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class AlphabetToUnicode {

    public static void main(String[] args) {
        String uppercase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String Lowercase = "abcdefghijklmnopqrstuvwxyz";
        System.out.println(stringToUnicode(uppercase));
        System.out.println(stringToUnicode(Lowercase));
    }

    public static String stringToUnicode(String s) {
        String str = "";
        for (int i = 0; i < s.length(); i++) {
            int ch = (int) s.charAt(i);
            if (ch > 255) {
                str += s.charAt(i) + ": " + "\\u" + Integer.toHexString(ch) + "\n";
            } else {
                str += s.charAt(i) + ": " + "\\u00" + Integer.toHexString(ch) + "\n";
            }
        }
        return str;
    }
}
