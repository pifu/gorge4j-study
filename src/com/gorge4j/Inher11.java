package com.gorge4j;

/**
 * @Title: Inher11.java
 * @Description: [继承 -> “==”与equals()] | 验证 “==” 与 “equals” 的区别
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 12:55:54
 * @version v1.0
 */

public class Inher11 {
    public static void main(String[] args) {
        String s1 = new String("abc");
        String s2 = new String("abc");
        System.out.println(s1 == s2); // a==b —> 如果a与b为同一对象，则返回true
        System.out.println(s1.equals(s2)); // a.equals(b) —> 如果a对象的内容与b对象的内容相同，则返回true

        String s3 = "abc";
        String s4 = "abc";
        System.out.println(s3 == s4);
        System.out.println(s3.equals(s4));
    }
}
