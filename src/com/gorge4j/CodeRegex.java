package com.gorge4j;

/**
 * @Title: CodeRegex.java
 * @Description: [Java语言的基本语法 - 代码编写规则]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 21:15:46
 * @version v1.0
 */

public class CodeRegex {

    // 块（block）开始
    public static void main(String[] args) { // 块（block）是指括在一对 {} 里的部分，关键字之间的多个空格“ “视作一个
        System.out.println("CodeRegex Example!"); // 一条命令以分号结束
    }
    // 块（block）结束
    
}
