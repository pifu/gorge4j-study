package com.gorge4j;

import java.io.*;

/**
 * @Title: Keyboard3.java
 * @Description: [Java输入与输出 -> 流 -> 字符流 -> 从System.in获取数据] | 从键盘读取一行数据，并输出至文件
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 12:55:54
 * @version v1.0
 */

public class Keyboard3 {
    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr); // 键盘输入流
        FileWriter fw = new FileWriter("C:\\abc.txt");
        BufferedWriter bw = new BufferedWriter(fw); // 文件输入流
        System.out.println("请输入内容（结束：eof）..");
        String data;
        while (true) { // 循环
            data = br.readLine(); // 从键盘读取一行数据，存入 data 中
            if (data.equals("eof")) {
                break; // 若输入字符串"eof"，则退出while循环
            }
            bw.write(data); // 将 data 中的数据存入文件
            bw.newLine();
        }
        bw.close();
        System.out.println("文件创建完毕");
    }
}
