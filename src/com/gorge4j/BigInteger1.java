package com.gorge4j;

import java.math.BigInteger;

/**
 * @Title: BigInteger1.java
 * @Description: [常用API之一 -> java.lang.Math.BigInteger] | BigInteger 常用方法
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class BigInteger1 {
    public static void main(String[] args) {
        BigInteger a = new BigInteger("123456789123456789");
        BigInteger b = new BigInteger("123456789123456789");

        BigInteger c = a.add(b); // 加
        System.out.println(c);

        BigInteger d = a.subtract(b); // 减
        System.out.println(d);

        BigInteger e = a.multiply(b); // 乘
        System.out.println(e);

        BigInteger f = a.divide(b); // 除
        System.out.println(f);

        BigInteger g = new BigInteger("2");
        System.out.println(g.pow(3)); // 2 的 3 次方
    }
}
