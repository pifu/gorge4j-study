package com.gorge4j;

import java.util.*;

/**
 * @Title: Random2.java
 * @Description: [常用API之二 -> java.util.Random类] | Random 使用，扑克牌洗牌
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Random2 {
    public static void main(String[] args) {
        Random rnd = new Random();
        String[] card = {"红桃1", "红桃2", "红桃3", "红桃4", "红桃5", "红桃6", "红桃7", "红桃8", "红桃9", "红桃10"};
        String temp;
        int a, b;
        for (int i = 0; i <= 100; i++) {
            a = rnd.nextInt(10);
            b = rnd.nextInt(10);
            temp = card[a];
            card[a] = card[b];
            card[b] = temp;
        }
        for (int i = 0; i < 10; i++) {
            System.out.println(card[i]);
        }

    }
}
