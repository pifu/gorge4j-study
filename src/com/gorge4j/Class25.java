package com.gorge4j;

/**
 * @Title: Class25.java
 * @Description: [类与对象 -> 构造函数]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 09:59:51
 * @version v1.0
 */

public class Class25 {
    int a;
    String b;

    public static void main(String[] args) {
        Class25 ob1 = new Class25();
        Class25 ob2 = new Class25();
        Class25 ob3 = new Class25();
        System.out.println(ob1.a + " " + ob1.b);
        System.out.println(ob2.a + " " + ob2.b);
        System.out.println(ob3.a + " " + ob3.b);
    }
}
