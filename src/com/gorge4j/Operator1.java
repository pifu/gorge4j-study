package com.gorge4j;

/**
 * @Title: Operator1.java
 * @Description: [Java语言的基本语法 -> 运算符 -> 逻辑非]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 09:16:46
 * @version v1.0
 */

public class Operator1 {
    public static void main(String[] args) {
        boolean b = true;
        System.out.println(!b); // 逻辑非（!）是单目运算符，把真（true）变为假（false），把假（false）变为真（true），例如：!true为假，!false为真
        System.out.println(!!b);
    }
}
