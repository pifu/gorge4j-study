package com.gorge4j;

/**
 * @Title: Alone4_4.java
 * @Description: 需求：编写程序，输出字母表中的所有大写字母
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:41:50
 * @version v1.0
 */

public class Alone4_4 {
    public static void main(String[] args) {
        for (char i = 'A'; i <= 'Z'; i++) {
            System.out.print(i);
        }
        System.out.println();
        // 以下是为了说明字符本质上也是数字
        for (char i = 65; i <= 90; i++) {
            System.out.print(i);
        }
    }
}
