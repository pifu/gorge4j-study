package com.gorge4j;

/**
 * @Title: Alone5_6.java
 * @Description: [类与对象 -> 构造函数] | 需求：重载构造函数初始化成员变量，并编写三个方法来获取变量的值
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 20:32:19
 * @version v1.0
 */

public class Alone5_6 {
    int a;
    double b;
    String c;
    public Alone5_6() {
        
    }
    
    public Alone5_6(int a) {
        this(); // 重载构造函数
        this.a = a;
    }
    
    public Alone5_6(int a, double b) {
        this(a); // 重载构造函数
        this.b = b;
    }
    
    public Alone5_6(int a, double b, String c) {
        this(a, b); // 重载构造函数
        this.c = c;
    }
    
    int getA() {
        return a;
    }
    
    double getB() {
        return b;
    }
    
    String getC() {
        return c;
    }
    
    public static void main(String[] args) {
        Alone5_6 ob = new Alone5_6(1, 2.0, "3.0");
        System.out.println(ob.getA());
        System.out.println(ob.getB());
        System.out.println(ob.getC());
    }
}
