package com.gorge4j;

import java.io.*;
import com.gorge4j.util.FileUtil;

/**
 * @Title: File13.java
 * @Description: [Java输入与输出 -> 流 -> 字符流 -> BufferedReader与BufferedWriter] | BufferedReader 使用
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class File13 {
    public static void main(String[] args) throws Exception {
        String filePath = FileUtil.generateFilePathByFileName("autoexec.txt"); // 文件路径
        FileInputStream fis = new FileInputStream(filePath);
        InputStreamReader isr = new InputStreamReader(fis);
        BufferedReader br = new BufferedReader(isr);
        String s;
        // 一行一行地读取数据并输出，readLine() 若无字符串可读，则输出 null
        while ((s = br.readLine()) != null) { // 只要 s 非 null，则循环往复
            System.out.println(s);
        }
        br.close();
    }
}
