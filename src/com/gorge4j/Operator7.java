package com.gorge4j;

/**
 * @Title: Operator7.java
 * @Description: [Java语言的基本语法 -> 运算符 -> 快速逻辑与和快速逻辑或]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 12:20:57
 * @version v1.0
 */

public class Operator7 {
    public static void main(String[] args) {
        System.out.println((30 > 20 || 10 != 10) && 20 < 10); // 括弧 () 里的优先级最高，关系运算符 >、!= 优先级比逻辑运算符 ||、&& 高 
        System.out.println(30 > 20 || (10 != 10 && 20 < 10));
    }
}
