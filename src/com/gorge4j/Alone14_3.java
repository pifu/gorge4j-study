package com.gorge4j;

/**
 * @Title: Alone14_3.java
 * @Description: [线程 -> 线程同步 -> 同步块]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 22:44:17
 * @version v1.0
 */

public class Alone14_3 extends Thread {

    int[] array; // 整型数组
    static StringBuffer sb = new StringBuffer(); // 公有对象

    public Alone14_3(int[] array) {
        this.array = array;
    }

    public void addLeft() { // 把数组的左半部分添加到 sb
        synchronized (sb) {
            for (int i = 0; i < array.length / 2; i++) {
                sb.append(array[i]);
                try {
                    sleep(10);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void addRight() { // 把数组的右半部分添加到 sb
        synchronized (sb) {
            for (int i = array.length / 2; i < array.length; i++) {
                sb.append(array[i]);
                try {
                    sleep(10);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void run() {
        addLeft();
        addRight();
    }

    public static void main(String[] args) {
        Alone14_3 a = new Alone14_3(new int[] {1, 1, 1, 1, 1, 2, 2, 2, 2, 2});
        Alone14_3 b = new Alone14_3(new int[] {3, 3, 3, 3, 3, 4, 4, 4, 4, 4});
        a.start();
        b.start();
        try {
            a.join();
            b.join();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(sb);
    }
}
