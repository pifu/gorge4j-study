package com.gorge4j;

/**
 * @Title: Diamond1.java
 * @Description: [程序流程控制语句 -> for语句] | 非标准示例，多重 for 循环示例，反色空心菱形图案打印
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Diamond1 {
    private static String ASTERISK = "*";
    private static String SPACE = " ";
    
    public static void main(String[] args) {
        for (int i = 1; i <= 9; i++) {
            for (int j = i; j <= 9; j++) {
                System.out.print(ASTERISK);
            }
            for (int k = 1; k <= i * 2 - 1; k++) {
                System.out.print(SPACE);
            }
            for (int n = i; n <= 9; n++) {
                System.out.print(ASTERISK);
            }
            System.out.println();
        }

        for (int i = 1; i <= 9; i++) {
            for (int j = i; j <= i * 2 - 1; j++) {
                System.out.print(ASTERISK);
            }
            for (int k = i; k <= 2 * 9 - i; k++) {
                System.out.print(SPACE);
            }
            for (int m = 1; m <= i; m++) {
                System.out.print(ASTERISK);
            }
            System.out.println();
        }
    }
}
