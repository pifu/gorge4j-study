package com.gorge4j;

/**
 * @Title: Alone4_5.java
 * @Description: 需求：编写程序，用于判断某一个整数 n （ = 29）是素数还是和数
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:41:50
 * @version v1.0
 */

public class Alone4_5 {
    public static void main(String[] args) {
        // 说明：若 n 为素数，则 n 不能除尽从 2～n-1 间的任一整数
        int n = 28;
        for (int i = 2; i <= n - 1; i++) {
            if (n % i == 0) {
                System.out.println(n + " 是和数");
                break; // 满足一个条件即退出循环
            }
            if (i == n - 1) {
                System.out.println(n + " 是质数");
            }
        }
    }
}
