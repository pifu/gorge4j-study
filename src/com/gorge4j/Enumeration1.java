package com.gorge4j;

import java.util.*;

/**
 * @Title: Enumeration1.java
 * @Description: [抽象类与接口 -> Enumeration接口] | 枚举
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Enumeration1 implements Enumeration<Object> {
    Object[] data;
    int count = 0;

    public Enumeration1(Object[] data) {
        this.data = data;
    }

    public boolean hasMoreElements() {
        return count < data.length;
    }

    public Object nextElement() {
        if (count < data.length) {
            return data[count++];
        }
        return null;
    }

    public static void main(String[] args) {
        String[] arr = new String[] {"abc", "def", "gif", "jkl"};
        Enumeration<?> enumeration = new Enumeration1(arr);
        while (enumeration.hasMoreElements()) {
            System.out.println(enumeration.nextElement());
        }
    }
}
