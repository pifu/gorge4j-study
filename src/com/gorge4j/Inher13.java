package com.gorge4j;

/**
 * @Title: Inher13.java
 * @Description: [继承 -> Object型引用变量] | 利用 instanceof 运算符判断 Object 型数组引用对象的类型
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 12:55:54
 * @version v1.0
 */

public class Inher13 {
    public static void main(String[] args) {
        Object[] ob = new Object[3];
        ob[0] = new String("abc");
        ob[1] = new Boolean(true);
        ob[2] = new Integer(5);
        for (int i = 0; i < ob.length; i++) {
            if (ob[i] instanceof String) {
                String s = (String) ob[i]; // 强制类型转换，将 Object 类型转换为 String 类型
                System.out.println(s.toUpperCase()); // toUpperCase() 方法用于返回字符串的大写字母表现形式
            } else if (ob[i] instanceof Boolean) {
                Boolean b1 = (Boolean) ob[i];
                boolean b2 = b1.booleanValue(); // booleanValue() 方法将 Boolean 型对象的值(false or ture)转换为 boolean 型
                System.out.println(b2 && false);
            }
        }
    }
}
