package com.gorge4j;

/**
 * @Title: Inher6.java
 * @Description: [继承 -> 覆盖] | 成员方法覆盖
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 12:55:54
 * @version v1.0
 */

class Inher6_1 {
    void hello() {
        System.out.println("Hi~~~");
    }
}


public class Inher6 extends Inher6_1 {
    void hello() { // 覆盖父类的成员方法
        super.hello(); // 调用继承而来的 hello() 方法
        System.out.println("高兴"); // 功能的扩展
    }

    public static void main(String[] args) {
        Inher6 ob = new Inher6();
        ob.hello();
    }
}

