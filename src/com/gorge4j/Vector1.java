package com.gorge4j;

import java.io.Serializable;
import java.util.Vector;

/**
 * @Title: Vector2.java
 * @Description: [常用API之二 -> Collection接口 -> List接口] | 实现 List 接口的 Vector 类的常用方法示例（创建空 Vector
 *               对象，并向其添加元素，然后输出元素）
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Vector1 {
    public static void main(String[] args) {
        Vector<Serializable> v = new Vector<Serializable>();
        v.addElement("您好");
        v.addElement(new Integer(7));
        v.addElement(new Double(5.2));
        v.addElement(new StringBuffer("abc"));
        v.addElement("Vector");
        v.add(3, "添加到 3 位置，从 0 开始"); // 添加元素到指定位置
        v.removeElement("添加到 3 位置，从 0 开始"); // 删除指定元素
        v.removeElementAt(3); // 删除指定位置的元素
        for (int i = 0; i < v.size(); i++) {
            System.out.println(v.elementAt(i));
        }
        System.out.println("'您好'首次出现的位置：" + v.indexOf("您好")); // 返回元素首次出现的位置
        System.out.println(v.capacity()); // 返回 Vector 大小
        System.out.println(v.elementAt(3)); // 返回指定位置的元素
    }
}
