package com.gorge4j;

/**
 * @Title: Synchronization2.java
 * @Description: [线程-线程同步] | 线程同步
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 23:51:46
 * @version v1.0
 */

public class Synchronization2 {
    public static void main(String[] args) {
        PrintThread pt1 = new PrintThread('A');
        PrintThread pt2 = new PrintThread('B');
        pt1.start();
        pt2.start();
    }
}
