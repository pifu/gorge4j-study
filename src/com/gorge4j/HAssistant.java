package com.gorge4j;

/**
 * @Title: HAssistant.java
 * @Description: [线程 -> 线程同步 -> 生产者与消费者] | 继承类构造函数执行的顺序
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 12:55:54
 * @version v1.0
 */


class HAssistant extends Thread { // 营业员类
    void sell() { // 销售一个汉堡

        // 获取汉堡包箱子的 monitor 后，进行销售
        synchronized (Ham.box) {

            if (Ham.production == Ham.sales) { // 汉堡包箱内无汉堡时
                System.out.println("营业员：请您稍等！");
                try {
                    // wati() 非 Thread 类方法，是 Object 类成员方法，wait() 方法使线程进入等待状态，仅在同步块或同步方法中使用
                    Ham.box.wait(); // 返还汉堡包箱子的 monitor 后，等待
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                } // 厨师唤醒营业员时，异常处理
            }

            Ham.sales++; // 将汉堡包从箱中取出，销售量增加
            System.out.println("营业员：顾客朋友们，汉堡包上来了.(总 " + Ham.sales + " 个)");
        }
    }

    public void run() {
        while (Ham.sales < Ham.totalMaterial) {
            System.out.println("<顾客订购汉堡包.>");
            sell(); // 销售汉堡
            try {
                sleep(3000);
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }
        }
    }

}
