package com.gorge4j;

/**
 * @Title: Excep11.java
 * @Description: [异常处理 -> 自定义异常] | 自定义异常
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

class MyException extends Exception {
    private static final long serialVersionUID = 1L;

    MyException(String ErrorMessage) {
        super(ErrorMessage);
    }
}


public class Excep11 {
    static int avg(int a, int b) throws MyException {
        if (a < 0 || b < 0) {
            throw new MyException("不可为负数");
        } else if (a > 100 || b > 100) {
            throw new MyException("数值过大");
        } else {
            return (a + b) / 2;
        }
    }

    public static void main(String[] args) {
        try {
            System.out.println(avg(-10, 20));
        } catch (MyException e) {
            System.out.println(e);
        }
    }
}
