package com.gorge4j;

import java.io.*;
import com.gorge4j.util.FileUtil;

/**
 * @Title: RAF3.java
 * @Description: [Java输入与输出 -> 流 -> RandomAccessFile] | 通过移动文件指针，实现文件的读写
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class RAF3 {
    public static void main(String[] args) throws Exception {
        String filePath = FileUtil.generateFilePathByFileName("raf1.txt"); // 文件路径
        File f = new File(filePath);
        RandomAccessFile raf = new RandomAccessFile(f, "rw");
        String s = "刀见笑";

        System.out.println("在文件的末尾追加数据");
        long len = raf.length();
        raf.seek(len);
        raf.writeUTF(s);

        System.out.println("读取刚存入的数据");
        raf.seek(len);
        System.out.println(raf.readUTF());

        System.out.println("读取最初的数据");
        raf.seek(0);
        System.out.println(raf.readInt());

        raf.close();
    }
}
