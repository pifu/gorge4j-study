package com.gorge4j;

import java.util.*;

/**
 * @Title: Arrays3.java
 * @Description: [常用API之二 -> java.util.Arrays类] | java.util.Arrays 类示例，矩形对象比较
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Arrays3 {
    public static void main(String[] args) {
        Sagak[] a = {new Sagak(2, 5), new Sagak(3, 3), new Sagak(1, 10), new Sagak(2, 2)}; // 矩形数组

        // 整理，使用 compareTo() 方法进行比较,此处的 compareTo() 方法并非 java 自带的方法，是 Arrays2 中覆盖后的方法
        Arrays.sort(a);

        for (int i = 0; i < a.length; i++) { // 逐一输出面积
            System.out.println(a[i].getArea());
            System.out.println(a[i]);
        }

    }
}
