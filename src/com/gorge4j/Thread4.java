package com.gorge4j;

/**
 * @Title: Thread4.java
 * @Description: [线程 -> 多线程] | 多线程
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

class MyThread4 extends Thread {
    public void run() {
        for (int i = 1; i <= 5; i++) {
            System.out.println(super.getName() + "：" + i);
        }
    }
}


public class Thread4 {
    public static void main(String[] args) {
        MyThread4 mt1 = new MyThread4();
        MyThread4 mt2 = new MyThread4();
        mt1.start();
        mt2.start();
    }
}
