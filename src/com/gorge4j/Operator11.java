package com.gorge4j;

/**
 * @Title: Operator11.java
 * @Description: [Java语言的基本语法 -> 运算符 -> 三目运算符]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 17:34:29
 * @version v1.0
 */

public class Operator11 {
    public static void main(String[] args) {
        System.out.println(true ? 10 : 20);
        System.out.println(false ? 10 : 20);
        System.out.println((3 > 2) && (2 < 3) ? 10 : 20);
    }
}
