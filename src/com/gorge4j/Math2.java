package com.gorge4j;

/**
 * @Title: Math2.java
 * @Description: [常用API之一 -> java.lang.Math] | Math 类常用方法
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Math2 {
    public static void main(String[] args) {
        System.out.println(Math.ceil(3.4));
        System.out.println(Math.ceil(-3.4));
        System.out.println(Math.floor(3.4));
        System.out.println(Math.floor(-3.4));
        System.out.println(Math.rint(3.4));
        System.out.println(Math.rint(-3.4));
        System.out.println(Math.pow(2, 3));
        System.out.println(Math.round(3.7));
        System.out.println(Math.round(-3.7));
        System.out.println(Math.random());
        System.out.println(Math.random());
    }
}
