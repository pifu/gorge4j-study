package com.gorge4j;

import java.io.*;
import com.gorge4j.util.FileUtil;

/**
 * @Title: File7.java
 * @Description: [Java输入与输出 -> 流 -> FileInputStream和FileOutputStream] | FileInputStream 类常用构造函数示例
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class File7 {
    public static void main(String[] args) throws Exception {
        String filePath = FileUtil.generateFilePathByFileName("alphabet.txt"); // 文件路径
        File f = new File(filePath);
        FileInputStream fis = new FileInputStream(f);
        int len = (int) f.length(); // 文件长度为 long 型数据，故需将其转换为 int 型数据
        byte[] b = new byte[len]; // 创建容纳文件大小的字节数组
        fis.read(b); // 从文件读取所有的数据，存入字节数组 b 中
        fis.close();
        for (int i = 0; i < b.length; i++) { // 输出
            System.out.print((char) b[i]);
        }
    }
}
