package com.gorge4j;

/**
 * @Title: Interface6.java
 * @Description: [抽象类与接口 -> 接口] | 接口中 default 及 static 关键字的使用，JDK 1.8 版本后适用
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 12:55:54
 * @version v1.0
 * @since JDK 1.8
 */

interface MyInter6_1 {
    // 与接口 MyInter6_2 包含同名的方法 method1
    void method1();

    void method2();

    // default 修饰的方法不强制要求重写，可以自由选择重写或者不重写，实现类 Interface6 没有重写了此方法
    default void method4() {
        System.out.println("MyInter6_1 method4");
    }

    // 与接口 MyInter6_2 中存在 default 修饰的同名的 method6 方法，实现类 Interface6 中需重写此方法
    default void method6() {
        System.out.println("MyInter6_1 method6");
    }

    // 与接口 MyInter6_2 中存在抽象的同名的 method6 方法，实现类 Interface6 中需重写 method6 方法
    default void method7() {
        System.out.println("MyInter6_1 method7");
    }

    // 与接口 MyInter6_2 中存在抽象的同名但签名不同的 method8 方法，实现类 Interface6 中需重写 method8 方法
    void method8();

    // 接口里的静态方法 staticMethod 只能通过 MyInter6_1.staticMethod 来访问
    static void staticMethod() {
        System.out.println("MyInter6_1 staticMethod");
    }

}


interface MyInter6_2 {
    // 与接口 MyInter6_1 包含同名的方法 method1
    void method1();

    void method3();

    // default 修饰的方法不强制要求重写，可以自由选择重写或者不重写，实现类 Interface6 重写了此方法
    default void method5() {
        System.out.println("MyInter6_2 method5");
    }

    // 与接口 MyInter6_1 中存在 default 修饰的同名的 method6 方法，实现类 Interface6 中需重写 method6 方法
    default void method6() {
        System.out.println("MyInter6_2 method6");
    }

    // 与接口 MyInter6_1 中存在 default 修饰的同名 method6 方法，实现类 Interface6 中需重写此方法
    void method7();

    // 与接口 MyInter6_2 中存在抽象的同名但签名不同的 method8 方法，实现类 Interface6 中需重写 method8 方法
    void method8(String s);

    // 接口里的静态方法 staticMethod 只能通过 MyInter6_2.staticMethod 来访问
    static void staticMethod() {
        System.out.println("MyInter6_2 staticMethod");
    }
}


public class Interface6 implements MyInter6_1, MyInter6_2 {
    // Inteface6 实现了两个接口 MyInter6_1 和 MyInter6_2，两接口中有同名的方法 method1，实现类 Interface6 中只需重写一次 method1 方法
    @Override
    public void method1() {
        System.out.println("method1 override");
    }

    // 重写接口抽象方法
    @Override
    public void method2() {
        System.out.println("method2 override");
    }

    // 重写接口抽象方法
    @Override
    public void method3() {
        System.out.println("method3 override");
    }

    // 重写接口是 default 修饰的方法
    @Override
    public void method5() {
        System.out.println("method5 override");
    }

    // 重写不同接口中同名的 default 方法
    @Override
    public void method6() {
        System.out.println("method6 override");
    }

    // 重写不同接口中同名的 default 和 abstract 方法
    @Override
    public void method7() {
        System.out.println("method7 override");
    }

    // 实现不同接口中的同名但签名不同的方法，构成重载
    @Override
    public void method8(String s) {
        System.out.println("method8 override " + s);
    }

    // 实现不同接口中的同名但签名不同的方法，构成重载
    @Override
    public void method8() {
        System.out.println("method8 override");
    }


    public static void main(String[] args) {
        Interface6 ob = new Interface6();
        ob.method1();
        ob.method2();
        ob.method3();
        ob.method4();
        ob.method5();
        ob.method6();
        ob.method7();
        ob.method8();
        ob.method8("重载");
        MyInter6_1.staticMethod();
        MyInter6_2.staticMethod();
    }

}
