package com.gorge4j;

/**
 * @Title: HMaker.java
 * @Description: [线程 -> 线程同步 -> 生产者与消费者]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 12:55:54
 * @version v1.0
 */

class HMaker extends Thread { // 厨师类

    void make() { // 制作一个汉堡包，然后通知营业员

        // 拥有了汉堡包箱子的 monitor 后，开始制作汉堡包
        synchronized (Ham.box) {
            Ham.production++; // 制作一个汉堡包，就装入箱子中
            System.out.println("厨师：这里有汉堡!!(总" + Ham.production + "个)");
            // notify() 非 Thread 类方法，是 Object 类成员方法,用于通知处于等待中的线程
            Ham.box.notify(); // 通知营业员
        }
    }

    public void run() {
        while (Ham.production < Ham.totalMaterial) { // 有多少原料，做多少汉堡
            try {
                sleep(10000); // 做一个汉堡，等待 10 秒钟（厨师的能力）
            } catch (InterruptedException ie) {
            }
            make(); // 做一个汉堡
        }
    }
}
