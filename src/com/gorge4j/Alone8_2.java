package com.gorge4j;

/**
 * @Title: Alone8_2.java
 * @Description: [继承 -> 多态] | 需求：在“动物”类中，新增一个 eat() 方法，并在“大象”类与“鼹鼠”类中对其进行覆盖
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 00:35:50
 * @version v1.0
 */

class Animal {
    String name;

    Animal() {}

    Animal(String name) {
        this.name = name;
    }

    void move() {
        System.out.println(name + "行走");
    }

    void eat() {
        System.out.println(name + "吃");
    }
}


class Elephant extends Animal {
    Elephant() {}

    Elephant(String name) {
        super(name);
    }

    void move() {
        System.out.println(name + "慢悠悠，一步步地行走");
    }

    // 覆盖父类的 eat() 方法
    void eat() {
        System.out.println(name + "咯吱咯吱地吃");
    }
}


class Mouse extends Animal {
    Mouse() {}

    Mouse(String name) {
        super(name);
    }

    void move() {
        System.out.println(name + "静悄悄地，蹑手蹑脚地行走");
    }

    // 覆盖父类的 eat() 方法
    void eat() {
        System.out.println(name + "嘎吱嘎吱地吃");
    }
}


public class Alone8_2 {
    public static void main(String[] args) {
        Elephant a = new Elephant("Jacky");
        Mouse b = new Mouse("Jerry");
        a.move();
        a.eat();
        b.move();
        b.eat();
    }
}
