package com.gorge4j;

/**
 * @Title: Alone7_1.java
 * @Description: [对象与方法 -> 获取命令行参数] | 需求：编写一程序，从命令行读取两个整数，并输出这两个整数的积。若给出的参数少于或多于2个，则输入相应的错误提示信息
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 12:17:51
 * @version v1.0
 */

public class Alone7_1 {
    public static void main(String[] args) {
        if (args.length != 2) {
            System.out.println("您的输入有误，请输入2个整数");
        } else {
            System.out.println("您输入的两个数 " + args[0] + " 和 " + args[1] + " 的乘积 = "
                    + Integer.parseInt(args[0]) * Integer.parseInt(args[1]));
        }
    }
}
