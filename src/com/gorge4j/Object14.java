package com.gorge4j;

/**
 * @Title: Object14.java
 * @Description: [对象与方法 -> 返回新对象]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Object14 {
    int a, b;

    Object14(int a, int b) {
        this.a = a;
        this.b = b;
    }

    Object14 twice() {
        return new Object14(a * 2, b * 2);
    }

    public static void main(String[] args) {
        Object14 ob1 = new Object14(10, 20);
        Object14 ob2 = ob1.twice();
        System.out.println(ob1.a + " " + ob1.b);
        System.out.println(ob2.a + " " + ob2.b);
    }
}
