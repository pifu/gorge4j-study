package com.gorge4j;

import java.util.*;

/**
 * @Title: Tokenizer1.java
 * @Description: [常用API之二 -> java.util.StringTokenizer类] | java.util.StringTokenizer 类使用
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Tokenizer1 {
    public static void main(String[] args) {
        String str = new String("Thinking Java Programing With Mr.Seo ");
        StringTokenizer stok = new StringTokenizer(str);
        for (int i = 1; stok.hasMoreElements(); i++) {
            System.out.println("第" + i + "个token: " + stok.nextToken());
        }
    }
}
