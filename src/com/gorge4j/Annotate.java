package com.gorge4j;

/**
 * @Title: Annotate.java
 * @Description: [Java语言的基本语法 - 注释] 这里是 javadoc 注释，生成 API 文档时使用，注释在编译时会被忽略
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 20:59:43
 * @version v1.0
 */

public class Annotate {
    
    /* 
     * 这里是多行注释 
     */
    public static void main(String[] args) {
        System.out.println("Annotate Example!"); // 这里是单行注释
    }
    
}
