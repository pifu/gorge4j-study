package com.gorge4j;

/**
 * @Title: Class19.java
 * @Description: [类与对象 -> 访问控制符] ｜ 注意：此例为错误的演示示例，编译无法通过
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

class Class19_A {
    private int num;

    private void hi() {
        System.out.println("Hi~~~");
    }

    public void hello() {
        System.out.println("Hello~~~");
    }
}


public class Class19 {
    public static void main(String[] args) {
        Class19_A ob = new Class19_A();
        ob.num = 10; // 不能访问其它类的私有（private）变量
        ob.hi(); // 不能访问其它类的私有（private）方法
        ob.hello(); // public 方法可以访问
    }
}
