package com.gorge4j;

/**
 * @Title: Interface1.java
 * @Description: [抽象类与接口 -> 接口] | 接口
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 12:55:54
 * @version v1.0
 */

interface MyInter {
    public static final int MAX = 100;
    public static final int MIN = 1;

    public void method1();

    public void method2(int a);
}


public class Interface1 implements MyInter {
    public void method1() {
        System.out.println("method1 override");
    }

    public void method2(int a) {
        System.out.println(a + ":method2 override");
    }

    public static void main(String[] args) {
        Interface1 ob = new Interface1();
        ob.method1();
        ob.method2(123);
        System.out.println(Interface1.MAX);
        System.out.println(MyInter.MIN);
    }
}
