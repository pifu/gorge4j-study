package com.gorge4j;

/**
 * @Title: Alone12_1.java
 * @Description: [常用API之一 -> java.lang.String] | 需求：利用字符串“abcdefg”实现下列结果输出：abcdEFG、AbCdEfG、gfedcba
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-10 00:55:40
 * @version v1.0
 */

class Alone12_1 {
    public static void main(String[] args) {
        String str1 = new String("abcdefg");
        String str2 = str1.replaceAll("efg", "EFG");
        String str3 = str2.replace('a', 'A');
        str3 = str3.replace('c', 'C');
        str3 = str3.replace('F', 'f');
        char[] value = new char[str1.length()];

        for (int i = 0, j = str1.length() - 1; i < str1.length(); i++, j--) {
            value[j] = str1.charAt(i);
        }

        System.out.println(str2);
        System.out.println(str3);
        System.out.print(String.copyValueOf(value));
    }
}
