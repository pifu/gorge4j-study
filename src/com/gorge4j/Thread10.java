package com.gorge4j;

/**
 * @Title: Thread10.java
 * @Description: [线程 -> 线程让步] | 线程让步
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Thread10 extends Thread {
    public void run() {
        for (int i = 1; i <= 5; i++) {
            System.out.println(getName() + ":" + 1);
            yield();
            try {
                sleep(100);
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        Thread10 t1 = new Thread10();
        Thread10 t2 = new Thread10();
        t1.start();
        t2.start();
    }
}
