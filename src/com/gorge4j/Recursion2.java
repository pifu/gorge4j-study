package com.gorge4j;

/**
 * @Title: Recursion2.java
 * @Description: [对象与方法 -> 递归调用]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Recursion2 {
    int[][] map = {
            {0, 0, 0, 0, 0, 0, 0, 1, 1, 0}, 
            {0, 1, 1, 1, 1, 1, 0, 1, 0, 0}, 
            {1, 0, 0, 0, 0, 1, 0, 0, 0, 0},
            {0, 0, 0, 0, 1, 1, 0, 0, 1, 1}, 
            {0, 1, 1, 0, 0, 1, 0, 0, 1, 0}, 
            {0, 0, 1, 1, 1, 1, 0, 0, 1, 0},
            {1, 0, 0, 0, 0, 1, 1, 1, 1, 0}, 
            {0, 1, 0, 0, 0, 1, 0, 0, 0, 0}, 
            {0, 0, 1, 1, 1, 1, 1, 0, 0, 1},
            {1, 0, 0, 0, 0, 0, 0, 0, 1, 0}};

    void checkXY(int x, int y) {
        if (x >= 0 && x <= 9 && y >= 0 && y <= 9 && map[x][y] == 1) {
            map[x][y] = 2;
            check(x, y);
        }
    }

    void check(int x, int y) {
        if (map[x][y] == 0) {
            return;
        }
        checkXY(x - 1, y);
        checkXY(x + 1, y);
        checkXY(x, y - 1);
        checkXY(x, y + 1);
    }

    void printMap() {
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                System.out.print(map[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        Recursion2 ob = new Recursion2();
        ob.check(1, 3); // 从这个坐标处开始搜索，将所有相邻的数字 1 全部换成 2
        ob.printMap();
    }
}

// 运行结果：
// 0 0 0 0 0 0 0 1 1 0
// 0 2 2 2 2 2 0 1 0 0
// 1 0 0 0 0 2 0 0 0 0
// 0 0 0 0 2 2 0 0 2 2
// 0 2 2 0 0 2 0 0 2 0
// 0 0 2 2 2 2 0 0 2 0
// 1 0 0 0 0 2 2 2 2 0
// 0 1 0 0 0 2 0 0 0 0
// 0 0 2 2 2 2 2 0 0 1
// 1 0 0 0 0 0 0 0 1 0
