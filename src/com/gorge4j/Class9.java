package com.gorge4j;

/**
 * @Title: Class9.java
 * @Description: [类与对象 -> 局部变量和成员变量]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Class9 {
    int a;
    int b = 100;
    String c;
    String d = "Hi~~~~";

    public static void main(String[] args) {
        Class9 ob = new Class9();
        System.out.println(ob.a); // 对于类的整型成员变量 a，如果没有初始化，系统会自动赋值 0
        System.out.println(ob.b);
        System.out.println(ob.c); // 对于类的对象型成员变量 c，如果没有初始化，系统会自动赋值 null
        System.out.println(ob.d);
    }
}
