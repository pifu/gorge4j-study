package com.gorge4j;

/**
 * @Title: Class8.java
 * @Description: [类与对象 -> 局部变量和成员变量] | 注意：此例为错误的演示示例，编译无法通过
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Class8 {
    public static void main(String[] args) {
        int a; // 此处如果不赋值会提示变量未初始化，无法编译通过
        System.out.println(a);
    }
}
