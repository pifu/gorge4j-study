package com.gorge4j;

import java.io.*;
import com.gorge4j.util.FileUtil;

/**
 * @Title: File8.java
 * @Description: [Java输入与输出 -> 流 -> FilterStream] | FilterOutputStream 使用
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class File8 {
    public static void main(String[] args) throws Exception {
        String filePath = FileUtil.generateFilePathByFileName("大写字母.txt"); // 文件路径
        File f = new File(filePath);
        FileOutputStream fos = new FileOutputStream(f);
        FilterOutputStream filter = new FilterOutputStream(fos);
        for (int i = 'A'; i <= 'Z'; i++) {
            filter.write(i);
        }
        filter.close();
    }
}
