package com.gorge4j;

import java.util.Date;
// import java.lang.*; // 注意：此包是自动引入的，即默认加载的

/**
 * @Title: Class18.java
 * @Description: [类与对象 -> 包与导入]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Class18 {
    public static void main(String[] args) {
        Date date = new Date();
        System.out.println(date);
    }
}
