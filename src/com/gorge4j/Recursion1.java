package com.gorge4j;

/**
 * @Title: Recursion1.java
 * @Description: [对象与方法 -> 递归调用]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Recursion1 {
    static void hi(int n) {
        if (n <= 0) {
            return;
        }
        hi(n - 1);
        System.out.println("您好" + n);
    }

    public static void main(String[] args) {
        hi(5);
    }
}
