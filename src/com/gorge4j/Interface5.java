package com.gorge4j;

import java.io.*;

/**
 * @Title: Interface5.java
 * @Description: [抽象类与接口 -> 接口] | 注意：此例为错误的演示示例，编译无法通过。验证接口中的方法不能使用 private 与 protected 关键字修饰
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 12:55:54
 * @version v1.0
 */

interface MyInter5 {
    private void method1(); // 接口中的方法不能使用 private 修饰，因为接口方法必须要被覆盖

    protected void method2(); // 接口中的方法不能使用 protected 修饰，因为接口方法必须要被覆盖
}


public class Interface5 implements MyInter5 {
    public void method1() {
        System.out.println("method override");
    }

    public void method2() {
        System.out.println("method override");
    }

    public static void main(String[] args) {
        Interface5 ob = new Interface5();
        ob.method1();
        ob.method2();
    }
}
