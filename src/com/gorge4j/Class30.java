package com.gorge4j;

/**
 * @Title: Class30.java
 * @Description: [类与对象 -> 构造函数]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 09:59:51
 * @version v1.0
 */

public class Class30 {
    public Class30() {
        System.out.println("您好~~~");
    }

    public Class30(String a) {
        this(); // 调用其它构造函数
        System.out.println(a);
    }

    public Class30(int a) {
        this("高兴~~~"); // 调用其它构造函数
        System.out.println(a);
    }

    public static void main(String[] args) {
        Class30 ob = new Class30(20);
    }
}
