package com.gorge4j;

/**
 * @Title: Alone14_1.java
 * @Description: [线程 -> 多线程] | 需求：编写程序，创建 10 个线程，用于求取 1～1000 的和，使用线程数组来组织线程，提高效率
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 19:22:09
 * @version v1.0
 */

class Alone14_1 {
    public static void main(String[] args) {
        long sum = 0;
        SumThread[] st = new SumThread[10];
        try {
            for (int i = 0, j = 1; i <= 9; i++, j += 100) {
                st[i] = new SumThread(j, (i * 100) + 100);
                st[i].start();
                st[i].join();
                sum += st[i].getSum();
                System.out.println(j + "~" + ((i * 100) + 100) + " 的和 = " + st[i].getSum());
            }
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
        System.out.println("总和 = " + sum);
    }
}


class SumThread extends Thread {
    int from, to;
    long sum;

    SumThread(int from, int to) {
        this.from = from;
        this.to = to;
    }

    long getSum() {
        return sum;
    }

    public void run() {
        for (int i = from; i <= to; i++)
            sum += i;
    }
}
