package com.gorge4j;

/**
 * @Title: Wrapper1.java
 * @Description: [常用API之一 -> wrapper class] | 基本数据类型包装成对象类 Wrapper
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Wrapper1 {
    public static void main(String[] args) {
        int a = 7;
        String b = "123";
        Integer w1 = new Integer(a); // 创建内含整型数字 7 的 Integer 对象
        Integer w2 = new Integer(b); // 将字符串“123”转换为数字 123，并创建含有此数字的 Integer 对象
        System.out.println(w1); // 7
        System.out.println(w2); // 123
    }
}
