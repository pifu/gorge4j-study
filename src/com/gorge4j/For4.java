package com.gorge4j;

/**
 * @Title: For4.java
 * @Description: [程序流程控制语句 -> for语句]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class For4 {
    public static void main(String[] args) {
        for (int a = 10; a >= 1; a--) {
            System.out.print(a + " ");
        }
    }
}
