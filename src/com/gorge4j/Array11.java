package com.gorge4j;

/**
 * @Title: Array11.java
 * @Description: [数组 -> 二维数组]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Array11 {
    public static void main(String[] args) {
        String[][] s = new String[2][];
        s[0] = new String[2];
        s[1] = new String[2];
        s[0][0] = new String("中国");
        s[0][1] = new String("ABC");
        s[1][0] = new String("俄罗斯");
        s[1][1] = new String("美国");
        System.out.println(s[0][0]);
        System.out.println(s[1][1]);
    }
}
