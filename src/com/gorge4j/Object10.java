package com.gorge4j;

/**
 * @Title: Object10.java
 * @Description: [对象与方法 -> 值传递调用]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Object10 {
    static void play(String name) {
        String a = name + "一起玩吧！";
        System.out.println(a);
    }

    public static void main(String[] args) {
        play(new String("天下"));
    }
}
