package com.gorge4j;

/**
 * @Title: Synchronization6.java
 * @Description: [线程 -> 线程同步 -> 生产者与消费者] | 单消费者示例（单个营业员）
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 23:51:46
 * @version v1.0
 */

public class Synchronization6 {
    public static void main(String[] args) {
        HMaker maker = new HMaker();
        HAssistant assistant = new HAssistant();
        maker.start();
        assistant.start();
    }
}
