package com.gorge4j;

/**
 * @Title: For8.java
 * @Description: [程序流程控制语句 -> for语句]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 12:55:54
 * @version v1.0
 */

public class For8 {
    public static void main(String[] args) {
        for (int i = 1; true; i++) {
            if (i == 6) {
                break; // 若 i 为 6，则退出 for 循环
            }
            System.out.print(i + " ");
        }
    }
}
