package com.gorge4j;

/**
 * @Title: Inher9.java
 * @Description: [继承 -> 引用的范围] | 父类型引用变量的引用范围
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 12:55:54
 * @version v1.0
 */

class Super {
    int a = 10;

    void hi() {
        System.out.println("您好~~");
    }
}


class Sub extends Super {
    int b = 20;

    void hi() {
        System.out.println("Hi~~");
    }

    void bye() {
        System.out.println("bye~~");
    }
}


public class Inher9 {
    public static void main(String[] args) {
        Super ob = new Sub();
        ob.a = 20;
        ob.hi();
        // System.out.println(ob.a);
        // ob.b=30; // 错误，不能引用
        // ob.bye(); // 错误，不能引用
    }
}
