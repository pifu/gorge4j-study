package com.gorge4j;

import java.time.LocalTime;

/**
 * @Title: LocalTime1.java
 * @Description: [常用API之三 -> java.time.LocalTime类] ｜ 只含时分秒的时间对象，LocalTime: 表示没有时区的时间, 是不可变且线程安全的
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-11-12 10:41:33
 * @version v1.0
 */

public class LocalTime1 {

    public static void main(String[] args) {

        // 获取当前时间的毫秒值
        LocalTime localTime = LocalTime.now();
        System.out.println(localTime);
        
        // 根据指定日期/时间创建对象
        LocalTime ofLocalTime = LocalTime.of(23, 45, 55);
        System.out.println(ofLocalTime);

        // 获取当前时间（不含毫秒值）
        LocalTime todayTimeWithNoMillisTime = LocalTime.now().withNano(0);
        System.out.println(todayTimeWithNoMillisTime);

        // 字符串格式化为时间
        LocalTime parseTime = LocalTime.parse("23:59:59");
        System.out.println(parseTime);

    }

}
