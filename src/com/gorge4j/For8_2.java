package com.gorge4j;

/**
 * @Title: For8_2.java
 * @Description: [程序流程控制语句 -> for语句]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 12:55:54
 * @version v1.0
 */

public class For8_2 {
    public static void main(String[] args) {
        for (int i = 1; i <= 10; i++) {
            if (i <= 5)
                continue;
            System.out.print(i + " ");
        }
    }
}
