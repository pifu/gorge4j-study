package com.gorge4j;

/**
 * @Title: Thread7.java
 * @Description: [线程 -> 线程的休眠与唤醒] | 线程的休眠与唤醒
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Thread7 extends Thread {
    public void run() {
        for (int i = 1; i <= 5; i++) {
            System.out.println(getName() + "：" + i);
            try {
                sleep(1000);
            } catch (InterruptedException ie) {
            }
        }
    }

    public static void main(String[] args) {
        Thread7 t1 = new Thread7();
        t1.start();
    }
}
