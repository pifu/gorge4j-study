package com.gorge4j;

/**
 * @Title: Operator12.java
 * @Description: [Java语言的基本语法 -> 运算符 -> 赋值运算符]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 21:47:39
 * @version v1.0
 */

public class Operator12 {
    public static void main(String[] args) {
        int a = 2;
        a += 2;
        System.out.println(a);
        a ^= 2;
        System.out.println(a);
        a <<= 1;
        System.out.println(a);
        a |= 2;
        System.out.println(a);
        a %= 2;
        System.out.println(a);
    }
}
