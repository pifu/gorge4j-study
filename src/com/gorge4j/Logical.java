package com.gorge4j;

/**
 * @Title: Logical.java
 * @Description: [Java语言的基本语法 -> 数据类型 -> 基本数据类型 -> 逻辑型] 逻辑型也称为布尔型
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 13:44:17
 * @version v1.0
 */

public class Logical {
    public static void main(String[] args) {
        boolean a = true;
        boolean b = false;
        System.out.println(a);
        System.out.println(b);
    }
}
