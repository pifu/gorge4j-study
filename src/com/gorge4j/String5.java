package com.gorge4j;

/**
 * @Title: String5.java
 * @Description: [常用API之一 -> java.lang.String] | String 常用方法之四
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 23:51:46
 * @version v1.0
 */

public class String5 {
    public static void main(String[] args) {
        String s1 = new String("This is a String");
        // String s2=new String(" def ");
        System.out.println(s1.toLowerCase()); // 字符串转小写
        System.out.println(s1.toUpperCase()); // 字符串转大写
        System.out.println(new String("  abc  ").trim()); // 删除字符串两端的空格
        // System.out.println(s2.trim());
        // System.out.println(" abc ".trim());
        // System.out.println(s2.trim());
        System.out.println(String.valueOf(1234));
        System.out.println(String.valueOf(12.345));
    }
}
