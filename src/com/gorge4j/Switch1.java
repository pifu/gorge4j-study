package com.gorge4j;

/**
 * @Title: Switch1.java
 * @Description: [程序流程控制语句 -> switch语句]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 13:18:18
 * @version v1.0
 */

public class Switch1 {
    public static void main(String[] args) {
        int n = 10;
        switch (n) {
            case 0:
                System.out.println("000");
            case 2:
                System.out.println("222");
            case 7:
                System.out.println("777");
            case 9:
                System.out.println("999");
            default:
                System.out.println("未知");
        }
    }
}
