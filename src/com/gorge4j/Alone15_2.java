package com.gorge4j;

/**
 * @Title: Alone15_2.java
 * @Description: [常用API之二 -> java.util.Random类]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 21:58:38
 * @version v1.0
 */

import java.util.*;
import java.io.*;

public class Alone15_2 {
    public static void main(String[] args) throws IOException {
        Random rnd = new Random(); // 产生随机数，让计算机随机选择道具
        String[] daoju = {"剪子", "锤子", "布"}; // 定义三个道具
        int user, computer; // 定义用户、计算机，选择道具用
        int score1 = 0; // 初始化用户分数
        int score2 = 0; // 初始化电脑分数
        String select; // 定义选择器变量
        System.out.println("开始游戏"); // 打印出“开始游戏”
        try {
            while (true) { // 死循环，如果不break;跳出，一直循环
                System.out.println("剪刀(0),锤子(1),包袱(2),选择哪一个？"); // 提示用户选择道具
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); // 获取用户输入的数字
                user = Integer.parseInt(br.readLine()); // 将用户输入的字符串转换为整数
                if (!(user == 0 || user == 1 || user == 2)) {
                    System.out.println("您输入的数字有误，请重新输入");
                    continue;
                }
                computer = rnd.nextInt(3); // 产生随机数给电脑用户
                System.out.println("玩家：" + daoju[user]); // 打印出玩家选择的道具
                System.out.println("计算机：" + daoju[computer]); // 打印出计算机选择的道具
                if ((user == 0 && computer == 0) || (user == 1 && computer == 1) || (user == 2 && computer == 2)) { // 如果用户和计算机选择的道具相同，分数自加0，打印出比分
                    score1 += 0;
                    score2 += 0;
                    System.out.println("平局");
                    System.out.println("比分：" + score1 + ":" + score2);
                } else if ((user == 0 && computer == 1) || (user == 1 && computer == 2)
                        || (user == 2 && computer == 0)) { // 如果计算机赢了，处理逻辑，分数自加，打印出比分
                    score1 += 0;
                    score2 += 1;
                    System.out.println("此局计算机赢了");
                    System.out.println("比分：" + score1 + ":" + score2);
                } else if ((user == 0 && computer == 2) || (user == 1 && computer == 0)
                        || (user == 2 && computer == 1)) { // 如果用户赢了，处理逻辑，分数自加，打印出比分
                    score1 += 1;
                    score2 += 0;
                    System.out.println("此局用户赢了");
                    System.out.println("比分：" + score1 + ":" + score2);
                } else {
                    System.out.println("未知错误！");
                }
                System.out.println("继续游戏吗？[y/n]"); // 玩完一局，是否继续游戏
                BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in)); // 获取用户的输入，y/n
                select = br.readLine(); // 将用户输入的值赋给定义的选择变量select
                if (select.equals("y"))
                    continue; // 如果选择的是y,继续
                if (select.equals("n"))
                    break; // 如果选择的是n,退出循环体‘
                if (!(select.equals("y") || select.equals("n"))) { // 如果输入的字符不是y或者n，提示重新输入，并切换到输入的环境
                    System.out.println("您输入的字符有误，请重新输入");
                    continue;
                }
            }
        } catch (Exception e) {
            System.out.println("抱歉，产生了异常，异常信息是：" + e);
        }

    }

}
