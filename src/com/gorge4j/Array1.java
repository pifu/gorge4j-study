package com.gorge4j;

/**
 * @Title: Array1.java
 * @Description: [数组 -> 基本数据类型数组]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Array1 {
    public static void main(String[] args) {
        int[] a;
        a = new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        System.out.println(a.length);
    }
}
