package com.gorge4j;

import java.io.*;
import com.gorge4j.util.FileUtil;

/**
 * @Title: File9.java
 * @Description: [Java输入与输出 -> 流 -> FilterStream-DataInputStream和DataOutputStream] |
 *               DataOutputStream 使用
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class File9 {
    public static void main(String[] args) throws Exception {
        String filePath = FileUtil.generateFilePathByFileName("hjb.dat"); // 文件路径
        File f = new File(filePath);
        FileOutputStream fos = new FileOutputStream(f);
        DataOutputStream dos = new DataOutputStream(fos);
        dos.writeUTF("洪金宝"); // 将字符串"洪金宝"按 UTF 格式写到输出流
        dos.writeInt(20); // 将 int 型数据 20 写到输出流
        dos.writeInt(180); // 将 int 型数据 180 写到输出流
        dos.writeUTF("中国"); // 将字符串"中国"按 UTF 格式写到输出流
        dos.close();
        System.out.println("文件已创建完毕");
    }
}
