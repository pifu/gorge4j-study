package com.gorge4j;

/**
 * @Title: String1.java
 * @Description: [常用API之一 -> java.lang.String] | String 类常用构造函数
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 23:51:46
 * @version v1.0
 */

public class String1 {
    public static void main(String[] args) {
        String s1 = new String("abc");
        String s2 = new String(s1); // 含有与 s1 相同的字符串

        char[] value = new char[] {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'};
        String s3 = new String(value); // 复制整个数组
        String s4 = new String(value, 3, 4); // 创建 “DEFG” 字符串

        System.out.println(s1);
        System.out.println(s2);
        System.out.println(s3);
        System.out.println(s4);
    }
}
