package com.gorge4j;

/**
 * @Title: Alone8_3.java
 * @Description: [继承 -> Object型引用变量]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 00:42:30
 * @version v1.0
 */

public class Alone8_3 {
    static void whatClass(Object obj) {
        if (obj instanceof String) {
            String s = (String) obj;
            System.out.println("String:" + s);
        } else if (obj instanceof Boolean) {
            Boolean b = (Boolean) obj;
            System.out.println("Boolean:" + b);
        } else if (obj instanceof Integer) {
            Integer i = (Integer) obj;
            System.out.println("Integer:" + i);
        } else if (obj instanceof Double) {
            Double d = (Double) obj;
            System.out.println("Double:" + d);
        } else {
            System.out.println("未知类型！");
        }
    }

    public static void main(String[] args) {
        whatClass("abc");
        whatClass(new Boolean(true));
        whatClass(new Integer(10));
        whatClass(new Double(5.5));
        whatClass(new Float(5));
    }
}
