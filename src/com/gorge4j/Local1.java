package com.gorge4j;

/**
 * @Title: Local1.java
 * @Description: [内部类 -> 局部类]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 12:55:54
 * @version v1.0
 */

public class Local1 {
    int a = 10;

    void f() {
        class Inner {
            int c = 20;

            void hi() {
                System.out.println(a);
                System.out.println(c);
            }
        }
        Inner in = new Inner();
        in.hi();
    }

    public static void main(String[] args) {
        Local1 local = new Local1();
        local.f();
    }
}
