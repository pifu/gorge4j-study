package com.gorge4j;

import java.util.*;

/**
 * @Title: Calendar2.java
 * @Description: [常用API之二 -> java.util.Calender类] | java.util.Calendar 日历时间类使用
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Calendar2 {
    public static void main(String[] args) {
        GregorianCalendar gc = new GregorianCalendar(); // GregorianCalendar 类继承 Calendar 类，部分方法定义为 final 和 static
        String now = "现在：" + gc.get(Calendar.YEAR) + "年" + (gc.get(Calendar.MONTH) + 1) + "月" // 月份从 0 开始记，月份请加 1
                + gc.get(Calendar.DATE) + "日";
        System.out.println(now);

        gc.add(Calendar.DATE, 1003); // 月份从 0 开始记，月份请加 1

        String future =
                "未来：" + gc.get(Calendar.YEAR) + "年" + (gc.get(Calendar.MONTH) + 1) + "月" + gc.get(Calendar.DATE) + "日";

        System.out.println(future);

        System.out.println(gc.get(Calendar.DAY_OF_WEEK) - 1); // 星期从周天 0 开始，西方习惯
    }
}
