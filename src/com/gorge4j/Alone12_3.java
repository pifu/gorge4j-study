package com.gorge4j;

/**
 * @Title: Alone12_3.java
 * @Description: [常用API之一 -> wrapper class] | 需求：编写 isNumberic，判断某个字符串是否由数字构成
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 01:04:21
 * @version v1.0
 */

public class Alone12_3 {
    public static void main(String[] args) {
        System.out.println(isNumberic("123"));
        System.out.println(isNumberic("-123.45"));
        System.out.println(isNumberic("0x12"));
        System.out.println(isNumberic("1abc"));
        System.out.println(isNumberic("-1a33"));
    }

    static boolean isNumberic(String num) {
        try {
            Short.parseShort(num);
            return true;
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        try {
            Integer.parseInt(num);
            return true;
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        try {
            Long.parseLong(num);
            return true;
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        try {
            Float.parseFloat(num);
            return true;
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        try {
            Double.parseDouble(num);
            return true;
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        return false;
    }
}
