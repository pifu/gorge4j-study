package com.gorge4j;

/**
 * @Title: Wrapper2.java
 * @Description: [常用API之一 -> wrapper class] | Integer 类的常用方法
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Wrapper2 {
    public static void main(String[] args) {
        int a = 20;
        Integer w = new Integer(10);

        System.out.println(a + w.intValue());

        int b = Integer.parseInt("123");
        System.out.println(b);

        int c = Integer.parseInt("12", 8);
        System.out.println(c);

        System.out.println(Integer.toBinaryString(-1));
    }
}
