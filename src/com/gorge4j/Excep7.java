package com.gorge4j;

/**
 * @Title: Excep7.java
 * @Description: [异常处理 -> 可抛出异常的方法] | 多次抛出异常示例
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Excep7 {
    // 异常往调用处的上层抛
    static void go() throws NegativeArraySizeException {
        int[] a = new int[-1];
    }

    // 异常继续往调用处的上层抛，如果想在这个方法里处理异常，则不用继续往上抛
    static void hi() throws NegativeArraySizeException {
        go();
    }

    public static void main(String[] args) {
        try {
            hi(); // 异常对象被抛出至此，在此进行异常的处理
        } catch (NegativeArraySizeException e) {
            System.out.println("来自远方的异常");
        }
    }
}
