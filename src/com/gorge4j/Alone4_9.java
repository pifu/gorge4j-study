package com.gorge4j;

/**
 * @Title: Alone4_9.java
 * @Description: 需求：编写程序，求所有满足等式 AB + CD = DC 的所有 A、B、C、D，且 A、B、C、D 必须为一位的自然数（A、B、C、D 不能为 0）
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 12:07:15
 * @version v1.0
 */

public class Alone4_9 {
    public static void main(String[] args) {
        for (int a = 1; a <= 9; a++) {
            for (int b = 1; b <= 9; b++) {
                for (int c = 1; c <= 9; c++) {
                    for (int d = 1; d <= 9; d++) {
                        if (a * 10 + b + c * 10 + d == 10 * d + c) {
                            System.out.println("式子：" + a + b + " + " + c + d + " = " + d + c);
                        }
                    }
                }
            }
        }
    }
}
