package com.gorge4j;

/**
 * @Title: Class24.java
 * @Description: [类与对象 -> 重载]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Class24 {
    static boolean isSame(int a, int b) {
        return a == b;
    }

    static boolean isSame(double a, double b) {
        return a == b;
    }

    public static void main(String[] args) {
        System.out.println(isSame(1, 2));
        System.out.println(isSame(1.2, 1.2));
    }
}
