package com.gorge4j;

/**
 * @Title: Class20.java
 * @Description: [类与对象 -> 访问控制符]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

class Class20_A {
    private int num; // 私有成员变量，仅内部可访问

    void setNum(int a) { // 对外提供接口，以便于设置 num 的值
        num = a;
    }

    int getNum() { // 对外提供接口，以便于获得 num 的值
        return num;
    }
}

public class Class20 {
    public static void main(String[] args) {
        Class20_A ob = new Class20_A();
        ob.setNum(10); // 改变 num 值，但 num 对外不可见（意思就是直接访问 ob.num 是不行的）
        System.out.println(ob.getNum());
    }
}
