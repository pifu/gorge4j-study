package com.gorge4j;

/**
 * @Title: Alone4_3.java
 * @Description: 需求：设计一个程序，用于判断某个整数是正数、负数，还是零
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:21:02
 * @version v1.0
 */

public class Alone4_3 {
    public static void main(String[] args) {
        int n = -20;
        if (n > 0) {
            System.out.println(n + " 为正数");
        } else if (n < 0) {
            System.out.println(n + " 为负数");
        } else {
            System.out.println(n + " 为零（0）");
        }
    }
}
