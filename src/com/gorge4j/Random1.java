package com.gorge4j;

import java.util.*;

/**
 * @Title: Random1.java
 * @Description: [常用API之二 -> java.util.Random类] | Random 常用方法
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Random1 {
    public static void main(String[] args) {
        Random rnd = new Random();
        for (int i = 0; i <= 5; i++) {
            System.out.print(rnd.nextInt(10) + " "); // 返回 0~9 之间的任意整数
        }

        System.out.print("\n");
        System.out.println(rnd.nextBoolean()); // 返回 true 或 false 中的其中一个
        System.out.println(rnd.nextDouble()); // 返回 0.0~1.0 之间的 double 型小数
        System.out.println(rnd.nextFloat()); // 返回 0.0~1.0 之间的 float 型小数
        System.out.println(rnd.nextInt()); // 返回 int 型整数
        System.out.println(rnd.nextLong()); // 放回 long 型整数

        System.out.println(rnd.nextInt(10)); // 返回 0~9（10-1） 之间的整数
        System.out.println(rnd.nextInt(10) + 5); // 返回 5~14（10+5-1） 间的任意整数
        System.out.println(2 * rnd.nextInt(10)); // 返回 0-18 间的任意整数
    }
}
