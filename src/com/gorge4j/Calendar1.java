package com.gorge4j;

import java.util.*;

/**
 * @Title: Calendar1.java
 * @Description: [常用API之二 -> java.util.Calender类] | java.util.Calendar 日历时间类使用
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Calendar1 {
    public static void main(String[] args) {
        GregorianCalendar gc = new GregorianCalendar();
        String now = gc.get(Calendar.YEAR) + "年" + (gc.get(Calendar.MONTH) + 1) + "月" + gc.get(Calendar.DATE) + "日"
                + gc.get(Calendar.AM_PM) + "[0:上午;1:下午]  " + gc.get(Calendar.HOUR) + "时" + gc.get(Calendar.MINUTE) + "分"
                + gc.get(Calendar.SECOND) + "." + gc.get(Calendar.MILLISECOND) + "秒";
        System.out.println("当前的时间是：" + now);
    }
}
