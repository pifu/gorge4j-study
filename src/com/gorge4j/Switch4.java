package com.gorge4j;

/**
 * @Title: Switch4.java
 * @Description: [程序流程控制语句 -> switch语句]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 13:22:58
 * @version v1.0
 */

public class Switch4 {
    public static void main(String[] args) {
        char a = 'A', b = 97;

        switch (a) {
            case 'A':
                System.out.println("A"); // 此行会输出 A
            case 'B':
                System.out.println("B");
                break; // 此行会输出 B，然后终止
            case 'C':
                System.out.println("C");
            default:
                System.out.println("未知");
        }

        System.out.println((int) a); // 此行会输出 65
        System.out.println((char) (a + 1)); // 此行会输出 B
        System.out.println(b); // 此行会输出 a
    }
}
