package com.gorge4j;

/**
 * @Title: Deprecation2.java
 * @Description: [线程 -> stop()、suspend()和resume()] | 终止进程
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Deprecation2 extends Thread {
    boolean runnable = true;

    void stopThread() {
        runnable = false;
    }

    public void run() {
        while (runnable)
            System.out.println("Hi~~~");
    }

    public static void main(String[] args) {
        Deprecation2 dt = new Deprecation2();
        dt.start();
        try {
            Thread.sleep(1);
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
        dt.stopThread();
        System.out.println("终止线程");
    }

}
