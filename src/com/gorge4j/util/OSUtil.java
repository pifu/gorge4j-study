package com.gorge4j.util;

/**
 * @Title: OSUtil.java
 * @Description: 操作系统工具类
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 14:09:25
 * @version v1.0
 */

public class OSUtil {

    // 此写法表示仅支持在类内部的示例化，因为是私有
    private OSUtil() {}

    // 获取操作系统名
    private static String OS = System.getProperty("os.name").toLowerCase();

    private static OSUtil OS_UTIL = new OSUtil();

    private EPlatform platform;

    public static boolean isWindows() {
        return OS.indexOf("windows") >= 0;
    }

    public static boolean isMacOS() {
        return OS.indexOf("mac") >= 0 && OS.indexOf("os") > 0 && OS.indexOf("x") < 0;
    }

    public static boolean isMacOSX() {
        return OS.indexOf("mac") >= 0 && OS.indexOf("os") > 0 && OS.indexOf("x") > 0;
    }

    public static boolean isLinux() {
        return OS.indexOf("linux") >= 0;
    }

    /**
     * 获取操作系统名
     * 
     * @return
     */
    public static EPlatform getOSName() {
        if (isWindows()) {
            OS_UTIL.platform = EPlatform.WINDOWS;
        } else if (isLinux()) {
            OS_UTIL.platform = EPlatform.LINUX;
        } else if (isMacOS()) {
            OS_UTIL.platform = EPlatform.MAC_OS;
        } else if (isMacOSX()) {
            OS_UTIL.platform = EPlatform.MAC_OS_X;
        } else {
            OS_UTIL.platform = EPlatform.OTHERS;
        }
        return OS_UTIL.platform;
    }

}


/**
 * 定义操作系统的枚举
 */
enum EPlatform {
    WINDOWS("Windows"), MAC_OS("Mac OS"), MAC_OS_X("Mac OS X"), LINUX("Linux"), OTHERS("Others");

    private EPlatform(String desc) {
        this.description = desc;
    }

    @Override
    public String toString() {
        return description;
    }

    private String description;
}
