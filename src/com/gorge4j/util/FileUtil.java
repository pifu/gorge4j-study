package com.gorge4j.util;

import java.io.File;

/**
 * @Title: FileUtil.java
 * @Description: 文件操作工具类
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 16:05:20
 * @version v1.0
 */

public class FileUtil {

    // File.separator 为目录分隔符，Windows 和 Linux 平台下不一样，Windows 下是 "\\"，Linux 下是 "/"，使用 File.separator 会自适应
    public static final String FILE_PATH = File.separator + "jstudy" + File.separator;

    public static final String WINDOWS_PATH = "D:" + FILE_PATH; // Windows 平台下默认的文件生成路径

    public static final String MAC_PATH = "/Users/" + System.getProperty("user.name") + FILE_PATH; // Mac 平台下默认的文件生成路径

    /**
     * 通过文件名生成完整的路径
     * 
     * @param fullFileName 完整的文件名，包括后缀
     * @return
     */
    public static String generateFilePathByFileName(String fullFileName) {
        // 如果传的文件名为空，则返回基础的路径
        if (fullFileName == null || fullFileName.isEmpty()) {
            throw new RuntimeException("文件名不能为空");
        }
        String filePath = ""; // 文件路径
        // 因为 Windows 系统跟 Mac OS 系统在文件路径上有差异，此处判断一下并做针对性的处理
        if (OSUtil.isWindows()) {
            filePath = FileUtil.WINDOWS_PATH; // Windows 系统下
        } else if (OSUtil.isMacOSX()) {
            filePath = FileUtil.MAC_PATH; // Mac OS 系统下
        } else {
            throw new RuntimeException("抱歉，不支持的操作系统，目前仅支持 Windows 和 Mac OS X 平台");
        }

        return filePath + fullFileName;
    }

    /**
     * 生成基础的路径
     * 
     * @return
     */
    public static String generateBaseFilePath() {
        String filePath = ""; // 文件路径
        // 因为 Windows 系统跟 Mac OS 系统在文件路径上有差异，此处判断一下并做针对性的处理
        if (OSUtil.isWindows()) {
            filePath = FileUtil.WINDOWS_PATH; // Windows 系统下
        } else if (OSUtil.isMacOSX()) {
            filePath = FileUtil.MAC_PATH; // Mac OS 系统下
        } else {
            throw new RuntimeException("抱歉，不支持的操作系统，目前仅支持 Windows 和 Mac OS X 平台");
        }

        return filePath;
    }

}
