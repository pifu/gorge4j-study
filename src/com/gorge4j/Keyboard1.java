package com.gorge4j;

import java.io.*;

/**
 * @Title: Keyboard1.java
 * @Description: [Java输入与输出 -> 流 -> 字符流 -> 从System.in获取数据] | System.in 结合 BufferedReader
 *               从键盘输入一行数据并打印出来
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 12:55:54
 * @version v1.0
 */

public class Keyboard1 {
    public static void main(String[] args) throws IOException {
        System.out.println("请输入一个字符，并按 Enter 键来结束");
        int r = System.in.read(); // 以 byte 为单位输入
        System.out.println("输入的字符为" + (char) r);
    }
}
