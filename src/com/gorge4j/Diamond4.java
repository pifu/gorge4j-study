package com.gorge4j;

import java.util.Scanner;

/**
 * @Title: Diamond4.java
 * @Description: [程序流程控制语句 -> for语句] | 非标准示例，多重 for 循环示例，手工输入边长，空心菱形图案打印
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Diamond4 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("请输入你要打印的空心菱形的边长（* 的数量）：");
        int s = sc.nextInt();
        int a = s + 1;
        int b = s - 1;
        int c = 2 * b + a;
        int d = s * 2 - 1;
        for (int i = 1; i <= d; i++) {
            if (i <= b) {
                for (int j = 1; j <= d; j++) {
                    if (j == b + i || j == a - i) {
                        System.out.print("*");
                    } else {
                        System.out.print(" ");
                    }
                }
            } else {
                for (int k = 1; k <= d; k++) {
                    if (k == i - b || k == c - i) {
                        System.out.print("*");
                    } else {
                        System.out.print(" ");
                    }
                }
            }
            System.out.println();
        }
    }

}
