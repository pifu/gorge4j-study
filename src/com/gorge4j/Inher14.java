package com.gorge4j;

/**
 * @Title: Inher14.java
 * @Description: [继承 -> 类的层级图] | 继承类构造函数执行的顺序
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 12:55:54
 * @version v1.0
 */

class A {
    int a = 1;

    A() {
        System.out.println("ִ执行类 A 的构造函数");
    }
}


class B extends A {
    int b = 2;

    B() {
        System.out.println("执行类 B 的构造函数");
    }
}


class C extends B {
    int c = 3;

    C() {
        System.out.println("执行类 C 的构造函数");
    }
}


public class Inher14 {
    public static void main(String[] args) {
        C ob = new C();
        System.out.println(ob.a);
        System.out.println(ob.b);
        System.out.println(ob.c);
        System.out.println(ob.toString());
    }
}
