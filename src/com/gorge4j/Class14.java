package com.gorge4j;

/**
 * @Title: Class14.java
 * @Description: [类与对象 -> 成员与静态方法的关系] ｜ 注意：此例为错误的演示示例，编译无法通过
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Class14 {
    int a; // 非静态变量

    static void setA(int b) {
        a = b; // 不能对非静态字段 a 进行静态引用
    }

    public static void main(String[] args) {
        Class14.setA(10);
        System.out.println(a);
    }
}
