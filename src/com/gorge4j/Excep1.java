package com.gorge4j;

/**
 * @Title: Excep1.java
 * @Description: [异常处理] | 异常之除 0 异常
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Excep1 {
    public static void main(String[] args) {
        System.out.println("开始程序");
        int a = 1, b = 0;
        int c = a / b; // a 被 0 除，此处产生异常
        System.out.println(c);
        System.out.println("退出程序");
    }
}
