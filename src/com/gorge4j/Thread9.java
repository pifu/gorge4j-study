package com.gorge4j;

/**
 * @Title: Thread9.java
 * @Description: [线程 -> 线程的休眠与唤醒] | 线程的休眠与唤醒
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Thread9 extends Thread {
    public void run() {
        System.out.println("休息，勿扰！");
        try {
            sleep(1000000);
        } catch (InterruptedException ie) {
            System.out.println("啊，谁唤醒我的？");
        }
    }

    public static void main(String[] args) {
        Thread9 t = new Thread9();
        t.start();
        t.interrupt();
    }
}
