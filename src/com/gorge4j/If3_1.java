package com.gorge4j;

/**
 * @Title: If3_1.java
 * @Description: [程序流程控制语句 -> if语句格式3、4] | if、else if、else 块内只有一条语句存在时，可以将花括弧省掉，但是不推荐这样做
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 10:00:18
 * @version v1.0
 */

public class If3_1 {
    public static void main(String[] args) {
        int 平均 = 75; // Java 使用 Unicode 字符集，故可以使用汉字来作为变量的名称
        String 评价;
        if (平均 >= 90) // if 块内只有一条语句存在时，可以将花括弧省掉，但是不推荐这样做
            评价 = "A";
        else if (平均 >= 80) // else if 块内只有一条语句存在时，可以将花括弧省掉，但是不推荐这样做
            评价 = "B";
        else if (平均 >= 70)
            评价 = "C";
        else if (平均 >= 60)
            评价 = "D";
        else // else 块内只有一条语句存在时，可以将花括弧省掉，但是不推荐这样做
            评价 = "E";

        System.out.println("平均=" + 平均);
        System.out.println("评价=" + 评价);
    }
}
