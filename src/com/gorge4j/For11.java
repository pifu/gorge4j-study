package com.gorge4j;

/**
 * @Title: For11.java
 * @Description: [程序流程控制语句 -> for语句]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 10:38:55
 * @version v1.0
 */

public class For11 {
    public static void main(String[] args) {
        for (int a = 1; a <= 3; a++) {
            for (int b = 1; b <= 2; b++) {
                if (a <= 2)
                    continue; // 在多重嵌套 for 循环中，continue 只能跳出当前所在的 for 循环
                System.out.println(a + " " + b);
            }
            System.out.println("您好 " + a);
        }
    }
}
