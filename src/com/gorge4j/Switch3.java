package com.gorge4j;

/**
 * @Title: Switch3.java
 * @Description: [程序流程控制语句 -> switch语句]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 13:22:58
 * @version v1.0
 */

public class Switch3 {
    public static void main(String[] args) {
        int a = 1;
        switch (a) {
            case 1:
                System.out.println("111");
            case 2:
                System.out.println("222");
                break;
            case 3:
                System.out.println("333");
            default:
                System.out.println("未知");
        }
    }
}
