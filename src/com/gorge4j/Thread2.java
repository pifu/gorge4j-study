package com.gorge4j;

/**
 * @Title: Thread2.java
 * @Description: [线程 -> 创建线程 -> 创建线程的另一种方法] | 创建线程的另外一种方法
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

class MyThread extends Thread {
    public void run() {
        for (int i = 1; i <= 5; i++) {
            System.out.println(super.getName() + ":" + i);
        }
    }
}


public class Thread2 {
    public static void main(String[] args) {
        MyThread mt = new MyThread();
        mt.start();
    }
}
