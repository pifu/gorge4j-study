package com.gorge4j;

/**
 * @Title: Alone4_6.java
 * @Description: 需求：编写程序，用于输出九九乘法表中的第 n （ =7 ）层
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 20:17:03
 * @version v1.0
 */

public class Alone4_6 {
    public static void main(String[] args) {
        int n = 7;
        for (int i = 1; i <= 9; i++) {
            System.out.println(n + " * " + i + " = " + n * i);
        }
    }
}
