package com.gorge4j;

/**
 * @Title: Inher5.java
 * @Description: [继承 -> 覆盖]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 12:55:54
 * @version v1.0
 */

class Inher5_1 {
    int a = 10;
}


public class Inher5 extends Inher5_1 {
    int a = 20; // 覆盖

    void f() {
        System.out.println(super.a); // 继承自父类的成员，输出 10 | 父类成员并未消失
        System.out.println(this.a); // 子类重定义的成员，输出 20
    }

    public static void main(String[] args) {
        Inher5 ob = new Inher5();
        ob.f();
    }
}
