package com.gorge4j;

/**
 * @Title: Member3.java
 * @Description: [内部类 -> 成员类] | 外部类和成员类的成员重名
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

class Outer3 {
    int a = 10;

    class Inner {
        static final int a = 100;

        void f() {
            System.out.println(this.a);
            System.out.println(Outer3.this.a);
        }
    }
}


public class Member3 {
    public static void main(String[] args) {
        Outer3 out = new Outer3();
        Outer3.Inner in = out.new Inner();
        in.f();
    }
}
