package com.gorge4j;

/**
 * @Title: Integral1.java
 * @Description: [Java语言的基本语法 -> 数据类型 -> 基本数据类型 -> 整型]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 13:50:00
 * @version v1.0
 */

public class Integral1 {
    public static void main(String[] args) {
        byte b = 1; // 1byte，取值范围：-128~127  
        short s = 32000; // 2byte，取值范围：-32768~32767
        int i = -2100000000; // 4byte，取值范围：-2147483648~21474836477
        long l = 1234567890123456890L; // 8byte，取值范围：-9223372036854775808~9223372036854775807。定义时 L 不能漏，也可用小写 "l"

        System.out.println(b);
        System.out.println(s);
        System.out.println(i);
        System.out.println(l);
    }
}
