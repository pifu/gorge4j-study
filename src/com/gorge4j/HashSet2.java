package com.gorge4j;

import java.util.*;

/**
 * @Title: HashSet2.java
 * @Description: [常用API之二 -> Collection接口 -> Set接口] | Set 不允许集合内添加重复的元素，此例测试
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 12:55:54
 * @version v1.0
 */

public class HashSet2 {
    public static void main(String[] args) {
        HashSet<String> hset = new HashSet<String>();
        System.out.println("添加");
        hset.add("您好");
        hset.add("Hello");
        hset.add("您好"); // 重复的元素将不被添加
        System.out.println("元素个数：" + hset.size());

        System.out.println("是否为空：" + hset.isEmpty()); // 测试集合是否为空，不为空返回 true，为空返回 false

        System.out.println("移除集合内的所有元素");
        hset.clear(); // 移除集合中的所有元素
        System.out.println("是否为空：" + hset.isEmpty());
    }
}
