package com.gorge4j;

/**
 * @Title: Object13.java
 * @Description: [对象与方法 -> 返回对象引用]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Object13 {
    String key;

    Object13(String key) {
        this.key = key;
    }

    Object13 getObject() { // 返回 Object13 型对象
        return this;
    }

    public static void main(String[] args) {
        Object13 ob1 = new Object13("hello");
        Object13 ob2 = ob1.getObject();
        ob2.key = "Hi";
        System.out.println(ob1.key);
    }
}
