package com.gorge4j;

/**
 * @Title: Object3.java
 * @Description: [对象与方法 -> 对象的创建与销毁]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Object3 {
    Object3() {
        System.out.println("对象创建");
    }

    void hi() {
        System.out.println("您好~~~");
    }

    public static void main(String[] args) {
        Object3 ob = null; // 引用变量 ob 初始化为null，并无语法上的错误，因此顺利通过了编译。
        ob.hi(); // 实际上 ob 所引用的对象并不存在，所以运行此行必定发生错误。
    }
}
