package com.gorge4j;

/**
 * @Title: String3.java
 * @Description: [常用API之一 -> java.lang.String] | String 常用方法
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 23:51:46
 * @version v1.0
 */

public class String2 {
    public static void main(String[] args) {
        String s1 = new String("ABCDEFGH");
        System.out.println(s1.charAt(4)); // E
        System.out.println(s1.compareTo("abc")); // -32; compareTo，长度不等，位数相减；长度相等，比较第一个字符，不相同 Unicode 码相减，相同接着往下比较
        System.out.println(s1.compareToIgnoreCase("abcdefgh")); // 0
        System.out.println(s1.concat("abc")); // ABCDEFGHabc
        System.out.println(s1.endsWith("FGH")); // true
        System.out.println(s1.equals("ABCDEFGH")); // true
        System.out.println(s1.equalsIgnoreCase("abcdefgh")); // true
    }
}
