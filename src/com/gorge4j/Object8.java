package com.gorge4j;

/**
 * @Title: Object8.java
 * @Description: [对象与方法 -> 值传递调用]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Object8 {
    private int a;

    public Object8(int a) {
        this.a = a;
    }

    public void twice(Object8 o) {
        o.a *= 2;
    }

    public int getA() {
        return a;
    }

    public static void main(String[] args) {
        Object8 ob = new Object8(100);
        ob.twice(ob);
        System.out.println(ob.getA());
    }
}
