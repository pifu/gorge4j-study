package com.gorge4j;

/**
 * @Title: Alone5_5.java
 * @Description: [类与对象 -> 重载] | 需求：编写三个求平均数的方法，并调用重载它们
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 20:25:58
 * @version v1.0
 */

public class Alone5_5 {
    public static void main(String[] args) {
        Alone5_5 ob = new Alone5_5();
        System.out.println(ob.average(10, 20));
        System.out.println(ob.average(10, 20, 30));
        System.out.println(ob.average(10, 20, 30, 40));
    }
    
    double average(int a, int b) {
        return (a + b) / 2.0;
    }
    
    double average(int a, int b, int c) {
        return (a + b + c) / 3.0;
    }
    
    double average(int a, int b, int c, int d) {
        return (a + b + c + d) / 4.0;
    }
}
