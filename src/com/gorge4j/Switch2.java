package com.gorge4j;

/**
 * @Title: Switch2.java
 * @Description: [程序流程控制语句 -> switch语句]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 13:18:18
 * @version v1.0
 */

public class Switch2 {
    public static void main(String[] args) {
        int a = 1, b = 4, c;
        switch (a + b) {
            case 1:
                c = 1;
                break;
            case 2:
                c = 2;
                break;
            case 3:
                c = 3;
                break;
            case 4:
                c = 4;
                break;
            default:
                c = 999;
        }
        System.out.println("c=" + c);
    }
}
