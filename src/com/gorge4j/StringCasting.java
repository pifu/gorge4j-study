package com.gorge4j;

/**
 * @Title: StringCasting.java
 * @Description: [Java语言的基本语法 -> 类型转换 -> String型与基本数据类型之间的运算]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 21:51:41
 * @version v1.0
 */

public class StringCasting {
    
    public static void main(String[] args) {
        
        String s = "您好" + 12; // String 类型跟其它类型的 “+” 号运算，结果是 String 类型
        System.out.println(s);
        
    }
    
}
