package com.gorge4j;

/**
 * @Title: Math3.java
 * @Description: [常用API之一 -> java.lang.Math] | Math 类常用方法
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Math3 {
    public static void main(String[] args) {
        System.out.println(Math.abs(3.4)); // 3.4
        System.out.println(Math.abs(-3.4)); // 3.4
        System.out.println(Math.max(10, 20)); // 20
        System.out.println(Math.min(10, 20)); // 10
    }
}
