package com.gorge4j;

/**
 * @Title: Excep10.java
 * @Description: [异常处理 -> 可抛出异常的方法] | 异常之 IOException 异常
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

import java.io.IOException;

public class Excep10 {
    public static void main(String[] args) {
        int a = 0;
        System.out.println("请输入字符>>");
        try {
            a = System.in.read();
        } catch (IOException e) {
            System.out.println("错误~~~~~~");
        }
        System.out.println("输入的字符为：" + (char) a);
    }
}
