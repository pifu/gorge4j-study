package com.gorge4j;

/**
 * @Title: Array5.java
 * @Description: [数组 -> 对象数组]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Array5 {
    int a, b;

    public static void main(String[] args) {
        Array5[] arr = new Array5[2]; // arr 是一个包含自定义的对象 Array5 类型的数组，数组里只能存储 Array5 类型的实例
        arr[0] = new Array5();
        arr[1] = new Array5();
        arr[0].a = 10;
        arr[0].b = 20;
        arr[1].a = 30;
        arr[1].b = 40;
        System.out.println(arr[0].a + arr[0].b);
        System.out.println(arr[1].a + arr[1].b);
    }
}
