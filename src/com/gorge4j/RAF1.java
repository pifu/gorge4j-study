package com.gorge4j;

import java.io.File;
import java.io.RandomAccessFile;
import com.gorge4j.util.FileUtil;

/**
 * @Title: RAF1.java
 * @Description: [Java输入与输出 -> 流 -> RandomAccessFile] | RandomAccessFile 类常用构造函数及方法
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class RAF1 {
    public static void main(String[] args) throws Exception {
        String filePath = FileUtil.generateFilePathByFileName("raf1.txt"); // 文件路径
        File f = new File(filePath);
        RandomAccessFile raf = new RandomAccessFile(f, "rw");
        int a = 10;
        double b = 12.34;
        String c = "abc";
        raf.writeInt(a);
        raf.writeDouble(b);
        raf.writeUTF(c);
        raf.close();
        System.out.println("文件创建完毕");
    }
}
