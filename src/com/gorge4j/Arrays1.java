package com.gorge4j;

import java.util.*;

/**
 * @Title: Arrays1.java
 * @Description: [常用API之二 -> java.util.Arrays类] | java.util.Arrays 类示例,一系列整形数组进行排序以及搜索元素所在位置
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Arrays1 {
    public static void main(String[] args) {
        int[] a = new int[] {52, 38, 43, 21, 15, 27, 32, 54, 36, 79}; // 定义整形数组
        // 说明：Arrays 类被定义为 final 类，同 Math 类一样无法创建其对象，所有的方法也都是 static 方法
        Arrays.sort(a); // 整理数组，对给定的数组a进行升序排列

        for (int i = 0; i < a.length; i++) { // 循环输出数组 a 里的值
            System.out.print(a[i] + " ");
        }

        System.out.println();
        System.out.println("54所在位置为：" + Arrays.binarySearch(a, 54)); // 搜索元素的位置,存在返回元素所在位置，从0开始
        System.out.println("41所在位置为：" + Arrays.binarySearch(a, 41)); // 搜索元素的位置，若元素不存在返回负数

    }
}
