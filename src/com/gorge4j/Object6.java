package com.gorge4j;

/**
 * @Title: Object6.java
 * @Description: [对象与方法 -> 值传递调用]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Object6 {
    static void swap(int a, int b) {
        int temp = a;
        a = b;
        b = temp;
    }

    public static void main(String[] args) {
        int x = 10, y = 20;
        swap(x, y);
        System.out.println(x + " " + y);
    }
}
