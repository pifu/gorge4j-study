package com.gorge4j;

/**
 * @Title: Thread8.java
 * @Description: [线程 -> 线程的休眠与唤醒] | 线程的休眠
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

class NewThred extends Thread {
    public void run() {
        for (int i = 1; i <= 10; i++) {
            System.out.println(); // 输出换行符
            try {
                sleep(1000); // 休眠1秒钟
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }
        }
    }
}


public class Thread8 extends Thread {
    public void run() {
        for (int i = 1; i <= 100; i++) {
            System.out.print(i); // print() 方法不会输出换行符
            try {
                sleep(100); // 休眠0.1秒钟
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        Thread8 t = new Thread8();
        NewThred n = new NewThred();
        // 同时启动两个线程
        t.start();
        n.start();
    }
}
