package com.gorge4j;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import com.gorge4j.util.FileUtil;

/**
 * @Title: Serialize1.java
 * @Description: [Java输入与输出 -> 流 -> 对象系列化 -> ObjectOutputStream] | ObjectOutputStream 类使用
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

class Man1 implements Serializable {
    private static final long serialVersionUID = 1L;

    String name;
    int age;
    double height;
}


public class Serialize1 {
    public static void main(String[] args) throws Exception {
        String filePath = FileUtil.generateFilePathByFileName("obj.dat"); // 文件路径
        FileOutputStream fos = new FileOutputStream(filePath);
        ObjectOutputStream oos = new ObjectOutputStream(fos);

        Man1 m = new Man1();
        m.name = "刀见笑";
        m.age = 26;
        m.height = 168.0;

        oos.writeObject(m);

        oos.close();
        System.out.println("已将对象输出至文件");
    }
}
