package com.gorge4j;

import java.io.File;
import java.io.RandomAccessFile;
import com.gorge4j.util.FileUtil;

/**
 * @Title: RAF2.java
 * @Description: [Java输入与输出 -> 流 -> RandomAccessFile] | RandomAccessFile 读取文件中的数据
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class RAF2 {
    public static void main(String[] args) throws Exception {
        String filePath = FileUtil.generateFilePathByFileName("raf1.txt"); // 文件路径
        File f = new File(filePath);

        RandomAccessFile raf = new RandomAccessFile(f, "r");

        System.out.println("从文件读取数据");
        System.out.println(raf.readInt());
        System.out.println(raf.readDouble());
        System.out.println(raf.readUTF());

        raf.close();
    }
}
