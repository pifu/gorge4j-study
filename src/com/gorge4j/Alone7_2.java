package com.gorge4j;

/**
 * @Title: Alone7_2.java
 * @Description: [对象与方法 -> 递归调用] | 需求：使用递归调用，编写一个求 1～n 的和的方法
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 23:35:01
 * @version v1.0
 */

public class Alone7_2 {
    public static void main(String[] args) {
        System.out.println("Sum(100) = " + sum(100));
    }

    static long sum(int n) {
        if (n == 1) {
            return 1;
        }
        return sum(n - 1) + n;
    }
}
