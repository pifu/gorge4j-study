package com.gorge4j;

/**
 * @Title: Array2.java
 * @Description: [数组 -> 基本数据类型数组]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Array2 {
    public static void main(String[] args) {
        int[] a = new int[4];
        int sum = 0;
        a[0] = 90;
        a[1] = 80;
        a[2] = 75;
        a[3] = 85;
        for (int i = 0; i < a.length; i++) {
            sum += a[i];
        }
        System.out.println("总分：" + sum);
        System.out.println("平均分：" + (double) sum / a.length);
    }
}
