package com.gorge4j;

import java.io.*;
import com.gorge4j.util.FileUtil;

/**
 * @Title: File2.java
 * @Description: [Java输入与输出 -> File类] | File 文件类的常用方法
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class File2 {
    public static void main(String[] args) {
        String filePath = FileUtil.generateFilePathByFileName("autoexec.txt"); // 文件路径
        File f = new File(filePath);
        System.out.println("是否是文件：" + f.isFile()); // 若是文件，则返回 true
        System.out.println("名称：" + f.getName()); // 文件名称
        System.out.println("目录：" + f.getParent()); // 文件所在目录
        System.out.println("路径：" + f.getPath()); // 文件路径
        System.out.println("大小：" + f.length() + " bytes"); // 文件长度
    }
}
