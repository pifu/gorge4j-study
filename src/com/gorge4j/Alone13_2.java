package com.gorge4j;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import com.gorge4j.util.FileUtil;

/**
 * @Title: Alone13_2.java
 * @Description: [Java输入与输出 -> 流 -> FilterStream-DataInputStream和DataOutputStream] | 需求：使用
 *               DataOutputStream 类，将数据存储到 stu.dat 文件中
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 14:19:11
 * @version v1.0
 */

public class Alone13_2 {
    public static void main(String[] args) throws IOException {
        String filePath = FileUtil.generateFilePathByFileName("stu.dat"); // 以生成的基础路径为示例来说明
        File file = new File(filePath);
        FileOutputStream fos = new FileOutputStream(file);
        DataOutputStream dos = new DataOutputStream(fos);

        dos.writeUTF("赵一");
        dos.writeInt(80);
        dos.writeInt(75);
        dos.writeInt(65);
        dos.writeInt(50);

        dos.writeUTF("钱二");
        dos.writeInt(90);
        dos.writeInt(100);
        dos.writeInt(100);
        dos.writeInt(100);

        dos.writeUTF("孙三");
        dos.writeInt(60);
        dos.writeInt(70);
        dos.writeInt(55);
        dos.writeInt(75);

        dos.writeUTF("李四");
        dos.writeInt(60);
        dos.writeInt(80);
        dos.writeInt(75);
        dos.writeInt(80);

        dos.writeUTF("周五");
        dos.writeInt(80);
        dos.writeInt(70);
        dos.writeInt(90);
        dos.writeInt(85);

        dos.writeUTF("吴六");
        dos.writeInt(100);
        dos.writeInt(80);
        dos.writeInt(90);
        dos.writeInt(85);

        dos.close();
    }
}
