package com.gorge4j;

/**
 * @Title: Operator3.java
 * @Description: [Java语言的基本语法 -> 运算符 -> 增减运算符]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:06:02
 * @version v1.0
 */

public class Operator3 {
    public static void main(String[] args) {
        int a = 10, b = 10;
        int c = ++a; // 前置递增运算符，先运算后赋值
        int d = b++; // 后置递增运算符，先赋值后运算
        System.out.println("a = " + a);
        System.out.println("b = " + b);
        System.out.println("c = " + c);
        System.out.println("d = " + d);
    }
}
