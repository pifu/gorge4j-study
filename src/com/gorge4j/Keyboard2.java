package com.gorge4j;

import java.io.*;

/**
 * @Title: Keyboard2.java
 * @Description: [Java输入与输出 -> 流 -> 字符流 -> 从System.in获取数据] | System.in 结合 BufferedReader
 *               从键盘输入一行数据并打印出来
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 12:55:54
 * @version v1.0
 */

public class Keyboard2 {
    public static void main(String[] args) throws IOException {
        System.out.println("输入一行数据，并按 Enter 键来结束");
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        String r = br.readLine();
        System.out.println("\\n\\n输入的字符串为：" + r);
    }
}
