package com.gorge4j;

/**
 * @Title: Class4.java
 * @Description: [类与对象 -> 成员方法 -> void返回类型]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Class4 {
    void printInt(int x) {
        System.out.println("整数= " + x);
    }

    public static void main(String[] args) {
        Class4 class4 = new Class4();
        class4.printInt(10);
        System.out.println("您好~~~");
        class4.printInt(20);
    }
}
