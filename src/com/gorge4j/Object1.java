package com.gorge4j;

/**
 * @Title: Object1.java
 * @Description: [对象与方法 -> 对象的创建与销毁]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Object1 {
    int a, b;

    Object1(int a, int b) { // 构造函数
        this.a = a;
        this.b = b;
        System.out.println("对象创建");
    }

    public static void main(String[] args) {
        Object1 ob1, ob2;
        ob1 = new Object1(10, 20);
        ob2 = new Object1(30, 40);
        ob1 = ob2;
    }
}
