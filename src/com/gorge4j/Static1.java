package com.gorge4j;

/**
 * @Title: Static1.java
 * @Description: [内部类 -> 静态类] | 静态类
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 23:51:46
 * @version v1.0
 */

class Outer {
    static int a = 10;
    int b = 20;

    static void f() {
        System.out.println("hi~~");
    }

    static class Inner {
        int c = 30;

        public void g() {
            f(); // 调用 Outer 类 static 方法
            System.out.println(a + " " + c); // 引用 Outer 类的 static 变量
        }
    }
}


public class Static1 {
    public static void main(String[] args) {
        Outer.Inner ob = new Outer.Inner();
        ob.g();
    }
}
