package com.gorge4j;

/**
 * @Title: Class15.java
 * @Description: [类与对象 -> 成员与静态方法的关系]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Class15 {
    static int a;

    static void hello() {
        System.out.println("您好~~");
    }

    static void setA(int b) {
        a = b;
        hello(); // 在静态方法中调用另外一个静态方法
    }

    void printA() {
        System.out.println(a);
        hello(); // 在非静态方法中调用静态方法是可以的，反之不行
    }

    public static void main(String[] args) {
        Class15.setA(10);
        Class15 ob = new Class15();
        ob.printA();
    }
}
