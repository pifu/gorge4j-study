package com.gorge4j;

/**
 * @Title: Thread3_1.java
 * @Description: [线程 -> 创建线程 -> 创建线程的另一种方法] | join() 方法使用
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Thread3_1 {
    public static void main(String[] args) {
        SumThread3 st = new SumThread3(1, 1000);
        st.start();
        try {
            st.join();
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
        System.out.println(st.getSum());
    }
}
