package com.gorge4j;

/**
 * @Title: Abstract1.java
 * @Description: [抽象类与接口 -> 抽象类]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

abstract class Shape { // 抽象类
    double area;

    public abstract void draw(); // 抽象方法

    public double area() {
        return area;
    }
}


class Triangle extends Shape { // 继承
    double d, h;

    public Triangle(int a, int b) { // 构造函数
        d = a;
        h = b;
        area = d * h / 2;
    }

    public void draw() { // 覆盖继承的抽象方法
        System.out.println("画三角形");
    }
}


class Rectangle extends Shape { // 继承
    double l, w;

    public Rectangle(int a, int b) { // 构造函数
        l = a;
        w = b;
        area = l * w;
    }

    public void draw() { // 覆盖抽象方法
        System.out.println("绘制矩形");
    }
}


public class Abstract1 {
    public static void main(String[] args) {
        Shape tr = new Triangle(10, 20);
        Shape re = new Rectangle(10, 20);
        System.out.println(tr.area());
        tr.draw();
        System.out.println(re.area());
        re.draw();
    }
}
