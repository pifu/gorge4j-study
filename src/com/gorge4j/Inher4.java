package com.gorge4j;

/**
 * @Title: Inher4.java
 * @Description: [继承 -> 继承中的访问控制符] ｜ 注意：此例为错误的演示示例，编译无法通过
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 12:55:54
 * @version v1.0
 */

class Inher4_1 {
    private int a; // private

    public void setA(int a) { // public
        this.a = a;
    }

    int getA() { // default
        return a;
    }
}


public class Inher4 extends Inher4_1 {
    void test() {
        a = 10; // 错误，父类的私有成员变量，子类无法访问
        setA(20);
        System.out.println(getA());
    }

    public static void main(String[] args) {
        Inher4 ob = new Inher4();
        ob.test();
    }
}
