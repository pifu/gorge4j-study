package com.gorge4j;

import java.io.*;

/**
 * @Title: Final1.java
 * @Description: [抽象类与接口 -> final关键字] ｜ 注意：此例为错误的演示示例，编译无法通过
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

final class Final1_1 {
    public static final double PI = 3.14;

    public final void fmethod() {
        System.out.println("不能覆盖此方法!");
    }
}


public class Final1 extends Final1_1 { // final 类不能被继承
    public void fmethod() { // final 方法不能被覆盖
        System.out.println("错误");
    }

    public static void main(String[] args) {
        PI = 3.15; // 不能改变常数值
    }
}
