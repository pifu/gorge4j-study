package com.gorge4j;

/**
 * @Title: Array9.java
 * @Description: [数组 -> 二维数组]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Array9 {
    public static void main(String[] args) {
        int[][] a;
        a = new int[2][];
        a[0] = new int[2];
        a[1] = new int[3];
        a[0][0] = 10;
        a[1][1] = 20;
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
    }
}
