package com.gorge4j;

/**
 * @Title: Class10.java
 * @Description: [类与对象 -> this引用]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Class10 {
    int a;

    void setA(int a) {
        this.a = a;
    }

    int getA() {
        return a;
    }

    public static void main(String[] args) {
        Class10 ob = new Class10();
        ob.setA(1000);
        System.out.println(ob.a);
        System.out.println(ob.getA());
    }
}
