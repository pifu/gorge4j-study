package com.gorge4j;

/**
 * @Title: Synchronization5.java
 * @Description: [线程 -> 线程同步 -> 同步化方法] | 多线程同步机制
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 23:51:46
 * @version v1.0
 */

class Printer {
    synchronized void printChar(char ch) {
        for (int i = 1; i <= 10; i++)
            System.out.print(ch);
    }
}


class PrinterThread extends Thread {
    Printer ptr;
    char ch;

    PrinterThread(Printer ptr, char ch) {
        this.ptr = ptr;
        this.ch = ch;
    }

    public void run() {
        for (int i = 1; i <= 5; i++) {
            ptr.printChar(ch);
            System.out.println();
        }
    }

}


public class Synchronization5 {
    public static void main(String[] args) {
        Printer ptr = new Printer();
        PrinterThread pt1 = new PrinterThread(ptr, 'A');
        PrinterThread pt2 = new PrinterThread(ptr, 'B');
        pt1.start();
        pt2.start();
    }
}
