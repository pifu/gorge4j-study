package com.gorge4j;

import com.gorge4j.util.OSUtil;

/**
 * @Title: RunAndPro2.java
 * @Description: [线程 -> Runtime类与Process类] | Process 类的常用方法
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class RunAndPro2 {
    public static void main(String[] args) throws Exception {
        Runtime rt = Runtime.getRuntime();
        System.out.println("运行文本编辑程序：");

        Process pr = null;

        // 因为 Windows 系统跟 Mac OS 系统在文件路径上有差异，此处判断一下并做针对性的处理
        if (OSUtil.isWindows()) {
            pr = rt.exec("C:\\windows\\notepad.exe"); // Windows 系统下，打开“记事本”
        } else if (OSUtil.isMacOSX()) {
            pr = rt.exec("open /Applications/TextEdit.app"); // Mac OS 系统下，打开“文本编辑”，进入到创建文稿页面
        } else {
            throw new RuntimeException("抱歉，不支持的操作系统，目前仅支持 Windows 和 Mac OS X 平台");
        }

        // 等待进程终止
        int exitValue = pr.waitFor();

        System.out.println("文本编辑程序终止！");
        System.out.println("终止值：" + exitValue);
    }
}
