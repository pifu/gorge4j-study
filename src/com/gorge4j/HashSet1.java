package com.gorge4j;

import java.util.*;

/**
 * @Title: HashSet1.java
 * @Description: [常用API之二 -> Collection接口 -> Set接口] | 实现 Set 接口的 HashSet 类常用方法示例，向 HashSet
 *               中添加元素以及从中删除
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 12:55:54
 * @version v1.0
 */

public class HashSet1 {

    public static void main(String[] args) {
        HashSet<String> hset = new HashSet<String>();
        System.out.println("添加三个元素"); // 添加成功返回 true
        System.out.println(hset.add("你好"));
        hset.add("Hello");
        hset.add("高兴");
        System.out.println("元素个数：" + hset.size());

        System.out.println("删除一个元素");
        hset.remove("你好");
        System.out.println("元素个数：" + hset.size());
    }
}
