package com.gorge4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/**
 * @Title: Alone15_3.java
 * @Description: [常用API之二 -> java.util.StringTokenizer类]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 22:05:23
 * @version v1.0
 */

public class Alone15_3 {
    public static void main(String[] args) throws IOException {
        System.out.println("请输入一段句子（英文）："); // 提示用户选择道具
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); // 获取用户输入的数字
        String s = br.readLine(); // 将用户输入的字符串转换为整数
        StringTokenizer stok = new StringTokenizer(s);
        for (int i = 1; stok.hasMoreElements(); i++) {
            System.out.println("第 " + i + " 个token: " + stok.nextToken());
        }
    }
}
