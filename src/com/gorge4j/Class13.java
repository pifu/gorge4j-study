package com.gorge4j;

/**
 * @Title: Class13.java
 * @Description: [类与对象 -> 静态变量与静态方法]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Class13 {
    static int return123() {
        return 123;
    }

    public static void main(String[] args) {
        System.out.println(Class13.return123());
        Class13 ob = new Class13();
        System.out.println(ob.return123());
    }
}
