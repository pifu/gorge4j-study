package com.gorge4j;

/**
 * @Title: Array8.java
 * @Description: [数组 -> 二维数组]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Array8 {
    public static void main(String[] args) {
        int[][] a;
        a = new int[3][];
        a[0] = new int[5];
        a[1] = new int[4];
        a[2] = new int[3];
        System.out.println(a.length);
        System.out.println(a[0].length);
        System.out.println(a[1].length);
        System.out.println(a[2].length);
    }
}
