package com.gorge4j;

/**
 * @Title: Object11.java
 * @Description: [对象与方法 -> 获取命令行参数]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Object11 {
    public static void main(String[] args) {
        for (int i = 0; i < args.length; i++)
            System.out.print(args[i]);
    }
}
