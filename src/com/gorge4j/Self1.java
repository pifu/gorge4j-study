package com.gorge4j;

/**
 * @Title: Self1.java
 * @Description: [对象与方法 -> 自引用]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

class Person {
    private String name;
    private Person friend;

    Person(String name) {
        this.name = name;
    }

    void setFriend(Person friend) {
        this.friend = friend;
    }

    String getName() {
        return name;
    }

    String getFriendName() {
        return friend.name;
    }
}


public class Self1 {
    public static void main(String[] args) {
        Person man1 = new Person("张三");
        Person man2 = new Person("李四");
        man1.setFriend(man2);
        man2.setFriend(man1);
        System.out.println(man1.getName() + "的朋友：" + man1.getFriendName());
        System.out.println(man2.getName() + "的朋友：" + man2.getFriendName());
    }
}
