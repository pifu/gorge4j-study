package com.gorge4j;

/**
 * @Title: Excep6.java
 * @Description: [异常处理 -> 可抛出异常的方法] | 调用方法抛出的异常捕获示例
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Excep6 {
    static void go() throws NegativeArraySizeException {
        int[] a = new int[-1];
    }

    public static void main(String[] args) {
        try {
            go(); // 异常对象被抛出至此
        } catch (NegativeArraySizeException e) { // x3
            System.out.println("go()方法抛出了异常");
        }
    }
}
