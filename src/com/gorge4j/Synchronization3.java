package com.gorge4j;

/**
 * @Title: Synchronization3.java
 * @Description: [线程 -> 线程同步 -> 同步块] | 同步块
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 23:51:46
 * @version v1.0
 */

class PrintThread2 extends Thread {
    char ch;
    static Object printer = new Object();

    public PrintThread2(char ch) {
        this.ch = ch;
    }

    void printCh10() {
        for (int i = 1; i <= 10; i++)
            System.out.print(ch);
    }

    public void run() {
        synchronized (printer) { // 同步块
            for (int i = 1; i <= 3; i++) {
                printCh10();
                System.out.println();
            }
        }
    }
}


public class Synchronization3 {
    public static void main(String[] args) {
        PrintThread2 pt1 = new PrintThread2('A');
        PrintThread2 pt2 = new PrintThread2('B');
        PrintThread2 pt3 = new PrintThread2('C');
        pt1.start();
        pt2.start();
        pt3.start();
    }
}
