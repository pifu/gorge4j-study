package com.gorge4j;

import java.io.File;
import com.gorge4j.util.FileUtil;

/**
 * @Title: File3.java
 * @Description: [Java输入与输出 -> File类] | File 文件类的常用方法
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class File3 {
    public static void main(String[] args) {
        String filePath = FileUtil.generateBaseFilePath(); // 基础路径
        File f = new File(filePath);
        File[] fs = f.listFiles(); // 读出文件目录
        for (int i = 0; i < fs.length; i++) {
            if (fs[i].isDirectory()) {
                System.out.print("目录：");
            } else {
                System.out.print("文件：");
            }
            System.out.println(fs[i]);
        }
    }
}
