package com.gorge4j;

/**
 * @Title: Operator2.java
 * @Description: [Java语言的基本语法 -> 运算符 -> 增减运算符]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 10:16:38
 * @version v1.0
 */

public class Operator2 {
    public static void main(String[] args) {
        int a = 10, b = 20;
        a++; // ++ 使变量值增1。需注意运算符跟变量之间不要有空格，没有语法错误，但是写法不规范，不推荐
        System.out.println("a = " + a);
        b--; // -- 使变量值减1。需注意运算符跟变量之间不要有空格，没有语法错误，但是写法不规范，不推荐
        System.out.println("b = " + b);
    }
}
