package com.gorge4j;

/**
 * @Title: AutoCasting.java
 * @Description: [Java语言的基本语法 -> 类型转换 -> 自动类型转换]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 21:10:15
 * @version v1.0
 */

public class AutoCasting {

    public static void main(String[] args) {

        byte b1 = 1;
        byte b2 = 2;
        int i1 = b1 + b2; // byte 和 byte 运算结果 int

        byte b3 = 3;
        short s1 = 4;
        int i2 = b3 + s1; // byte 和 short 运算结果 int

        byte b4 = 5;
        int i3 = 6;
        int i4 = b4 + i3; // byte 和 int 运算结果 int

        int i5 = 7;
        int i6 = 8;
        int i7 = i5 + i6; // int 和 int 运算结果 int

        int i8 = 9;
        long l1 = 10L;
        long l2 = i8 + l1; // int 和 long 运算结果 long

        long l3 = 11L;
        long l4 = 12L;
        long l5 = l3 + l4; // long 和 long 运算结果 long

        byte b5 = 13;
        float f1 = 1.5f;
        float f2 = b5 + f1; // byte 和 float 运算结果 float

        int i9 = 14;
        float f3 = 2.5f;
        float f4 = i9 + f3; // int 和 float 运算结果 float

        long l6 = 15;
        float f5 = 3.5f;
        float f6 = l6 + f5; // long 和 float 运算结果 float，特别注意：float（4个字节） 类型是比 long 类型（8个字节）更大的数据类型，主要原因是 float 能表示小数

        float f7 = 4.5f;
        double d1 = 1.5d;
        double d2 = f7 + d1; // float 和 double 运算结果 double

        double d3 = 2.5d;
        double d4 = 3.5d;
        double d5 = d3 + d4; // double 和 double 运算结果 double

        byte b6 = 33;
        int i10 = b6 >> 1; // 单个操作符也能产生强制类型转换

        System.out.println(i1);
        System.out.println(i2);
        System.out.println(i4);
        System.out.println(i7);
        System.out.println(l2);
        System.out.println(l5);
        System.out.println(f2);
        System.out.println(f4);
        System.out.println(f6);
        System.out.println(d2);
        System.out.println(d5);
        System.out.println(i10);

    }

}
