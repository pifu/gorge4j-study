package com.gorge4j;

/**
 * @Title: Floating.java
 * @Description: [Java语言的基本语法 -> 数据类型 -> 基本数据类型 -> 实数型] | 实数型示例
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 14:01:38
 * @version v1.0
 */

public class Floating {
    public static void main(String[] args) {
        // 4byte，取值范围：-1.45E~3.4028235E+38。注意 “f” 不能省略
        float f = 1.23f;
        // 8byte，取值范围：-4.9E-324~1.7976931348623157E+308。1.23 是 double 型常数，此处如定义成 float
        // 型变量是错误的，就比如不能把一个大容器放到小容器里一样
        double d = 1.23;
        System.out.println(f);
        System.out.println(d);
    }
}
