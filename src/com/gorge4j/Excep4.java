package com.gorge4j;

/**
 * @Title: Excep4.java
 * @Description: [异常处理 -> try-catch语句] | 同时处理多种异常
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Excep4 {
    public static void main(String[] args) {
        int[] a = new int[5];
        try {
            a[10] = 200;
        } catch (ArithmeticException e) { // 算术异常
            System.out.println(e);
        } catch (NullPointerException e) { // 空指针异常
            System.out.println(e);
        } catch (ArrayIndexOutOfBoundsException e) { // 数组越界异常
            System.out.println(e);
        } finally { // 兜底无条件执行
            System.out.println("无条件执行");
        }
    }
}
