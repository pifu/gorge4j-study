package com.gorge4j;

/**
 * @Title: Excep9.java
 * @Description: [异常处理 -> 可抛出异常的方法] | 注意：此例为错误的演示示例，编译无法通过。调用可能抛出异常的方法时，须对异常做出处理示例
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Excep9 {
    public static void main(String[] args) {
        int a = 0;
        System.out.println("请输入字符>>");
        a = System.in.read(); // 编译不通过，提示未处理的 IOException
        System.out.println("输入的字符为：" + (char) a);
    }
}
