package com.gorge4j;

/**
 * @Title: Symbolic.java
 * @Description: [Java语言的基本语法 -> 运算符 -> 符号运算符]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-16 15:40:12
 * @version v1.0
 */

public class Symbolic {
    public static void main(String[] args) {
        int a = 7;
        int b = -a;
        System.out.println(b);
        // 以下是一个错误的示例：
        // byte c = 7;
        // byte d = -c; // -c 运算后的结果是整型，再赋值给 byte （字节型）变量，显然是错误的
        // System.lut.println(d);
    }
}
