package com.gorge4j;

import java.io.IOException;

/**
 * @Title: Alone11_2.java
 * @Description: [异常处理 -> 可抛出异常的方法]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 00:49:41
 * @version v1.0
 */

class Alone11_2 {
    public static void main(String[] args) {
        int n = 0;
        try {
            while (true) {
                System.out.print("请输入字符:");
                while ((n = System.in.read()) != -1) {
                    if (n != 10 && n != 13) {
                        System.out.println("ASCII:" + n);
                        break;
                    }

                }
            }
        } catch (IOException ie) {
            ie.printStackTrace();
        }
    }
}
