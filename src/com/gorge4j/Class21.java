package com.gorge4j;

/**
 * @Title: Class21.java
 * @Description: [类与对象 -> 访问控制符]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

class Class21_A {
    public int num;

    public void hi() {
        System.out.println("您好~~~~");
    }
}

public class Class21 {
    public static void main(String[] args) {
        Class21_A ob = new Class21_A();
        ob.num = 10;
        ob.hi();
        System.out.println(ob.num);
    }
}
