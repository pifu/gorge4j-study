package com.gorge4j;

/**
 * @Title: For14.java
 * @Description: [程序流程控制语句 -> for语句]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class For14 {
    public static void main(String[] args) {
        outer: for (int a = 2; a <= 100; a++) {
            for (int b = 2; b <= Math.sqrt(a + 1); b++) { // b<=math.sqrt(a+1);
                if (a % b == 0) {
                    continue outer;
                }
            }
            System.out.print(a + " ");
        }
    }
}
