package com.gorge4j;

/**
 * @Title: Array13.java
 * @Description: [数组 -> 三维数组]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Array13 {
    public static void main(String[] args) {
        int[] a, b;
        b = new int[3];
        b[0] = 30;
        System.out.println(b[0]);
    }
}
