package com.gorge4j;

/**
 * @Title: For7.java
 * @Description: [程序流程控制语句 -> for语句]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class For7 {
    public static void main(String[] args) {
        for (int a = 1, b = 1; (a + b) <= 10; a++, b++) {
            System.out.print((a + b) + " ");
        }
    }
}
