package com.gorge4j;

/**
 * @Title: Array3.java
 * @Description: [数组 -> 一维数组的初始化]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Array3 {
    public static void main(String[] args) {
        int[] a = new int[] {12, 22, 37, 48, 59};
        double[] b = {1.2, -2.7, 9.53, 3.0};
        System.out.println(a.length);
        System.out.println(b.length);
        System.out.println(a[1]);
        System.out.println(b[1]);
    }
}
