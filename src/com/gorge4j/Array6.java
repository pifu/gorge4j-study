package com.gorge4j;

/**
 * @Title: Array6.java
 * @Description: [数组 -> 对象数组]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Array6 {
    public static void main(String[] args) {
        String[] s1 = {new String("abc"), new String("kor")};
        String[] s2 = new String[] {new String("您好"), new String("高兴")};
        System.out.println(s1[0]);
        System.out.println(s2[1]);
    }
}
