package com.gorge4j;

class Ham {
    static Object box = new Object(); // 汉堡包箱子

    static int totalMaterial = 10; // 制作汉堡包的材料的量（10 个汉堡包的量）
    static int sales = 0; // 汉堡包的销售量（最初为 0 个）
    static int production = 3; // 顾客光顾之前，先做好 3 个
}
