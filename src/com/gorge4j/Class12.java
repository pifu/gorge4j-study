package com.gorge4j;

/**
 * @Title: Class12.java
 * @Description: [类与对象 -> 静态变量与静态方法]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Class12 {
    static int a;
    int b;

    public static void main(String[] args) {
        Class12.a = 10;
        Class12 ob1 = new Class12();
        Class12 ob2 = new Class12();
        System.out.println(ob1.a);
        ob1.a = 20;
        System.out.println(ob2.a);
        ob2.a = 30;
        System.out.println(Class12.a);
    }
}
