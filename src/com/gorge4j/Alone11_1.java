package com.gorge4j;

/**
 * @Title: Alone11_1.java
 * @Description: [异常处理 -> try-catch语句] | 注意：此例为错误的演示示例，编译无法通过
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 00:45:40
 * @version v1.0
 */

public class Alone11_1 {
    public static void main(String[] args) {
        try {
            int[] a; // 只是定义，未初始化，即未引用对象
            a[10] = 100; // 引用未初始化的对象，必然报异常
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
