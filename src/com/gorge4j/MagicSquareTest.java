package com.gorge4j;

/**
 * @Title: MagicSquareTest.java
 * @Description: [数组 -> 二维数组] | 非标准示例，数组综合应用示例，多维魔方数
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class MagicSquareTest {
    public static void main(String[] args) {
        MagicSquare o1 = new MagicSquare(3);
        o1.makeMagicSquare();
        o1.printMagicSquare();

        MagicSquare o2 = new MagicSquare(4);
        o2.makeMagicSquare();
        o2.printMagicSquare();

        MagicSquare o3 = new MagicSquare(5);
        o3.makeMagicSquare();
        o3.printMagicSquare();
    }
}



class MagicSquare {
    private int size;
    private int[][] magicSquare;

    public MagicSquare(int size) {
        setSize(size);
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    public void makeMagicSquare() {
        if (size % 2 == 1) {
            int x = 0, y = 0;
            int count = 0;

            magicSquare = new int[size + 1][size + 1];

            for (int inx = 0; inx <= size; inx++) {
                for (int jnx = 0; jnx <= size; jnx++) {
                    magicSquare[inx][jnx] = 0;
                }
            }

            x = size;
            y = size / 2 + 1;

            while (count < (size * size)) {
                count++;

                magicSquare[x][y] = count;

                x++;
                y++;

                if (x > size) {
                    x = 1;
                }

                if (y > size) {
                    y = 1;
                }

                if (magicSquare[x][y] != 0) {
                    x--;
                    y--;

                    if (x == 0) {
                        x = size;
                    }

                    if (y == 0) {
                        y = size;
                    }

                    x--;

                    if (x == 0) {
                        x = size;
                    }
                }
            }
        } else {
            magicSquare = null;
        }
    }

    public void printMagicSquare() {
        if (magicSquare != null) {
            for (int inx = 1; inx <= size; inx++) {
                for (int jnx = 1; jnx <= size; jnx++) {
                    if (magicSquare[inx][jnx] < 10) {
                        System.out.print(" " + magicSquare[inx][jnx] + "  ");
                    } else {
                        System.out.print(magicSquare[inx][jnx] + "  ");
                    }
                }
                System.out.println();
            }
        } else {
            System.out.println("5 维魔方数.");
        }
        System.out.println();
    }
}

