package com.gorge4j;

/**
 * @Title: String7.java
 * @Description: [常用API之一 -> java.lang.StringBuffer] | StringBuffer 类的常用方法使用
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 23:51:46
 * @version v1.0
 */

public class String7 {
    public static void main(String[] args) {
        StringBuffer sb = new StringBuffer("ABCDEFG");
        sb.replace(0, 3, "abc");
        System.out.println(sb);
        sb.reverse();
        System.out.println(sb);
    }
}
