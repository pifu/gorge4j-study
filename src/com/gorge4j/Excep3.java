package com.gorge4j;

/**
 * @Title: Excep3.java
 * @Description: [异常处理 -> try-catch语句] | 异常之空指针异常
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Excep3 {
    void hi() {
        System.out.println("Hi~~");
    }

    public static void main(String[] args) {
        Excep3 ob = null;
        try {
            ob.hi(); // NullPointerException异常
        } catch (NullPointerException ne) {
            ob = new Excep3(); // 创建对象
        }
        ob.hi(); // 调用函数，进行输出
    }
}
