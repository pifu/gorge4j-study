package com.gorge4j;

/**
 * @Title: Alone14_2.java
 * @Description: [线程 -> 线程的休眠与唤醒] | 需求：编写程序，使得字符串 “文字的打印效果” 每隔一秒钟输出一个字符
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 19:28:06
 * @version v1.0
 */

class NewLine extends Thread {
    public void run() {
        for (int i = 1; i < 7; i++) {
            System.out.print(i + " 秒后: ");
            try {
                sleep(1000);
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }
        }
    }
}


class Alone14_2 extends Thread {
    public void run() {
        String str = new String("文字的打印效果");
        for (int i = 1; i <= str.length(); i++) {
            System.out.println(str.substring(0, i));
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        Alone14_2 al = new Alone14_2();
        NewLine n = new NewLine();
        try {
            System.out.print("起初: ");
            Thread.sleep(1000);
            al.start();
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
        n.start();
    }
}
