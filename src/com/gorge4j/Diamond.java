package com.gorge4j;

import java.util.Scanner;

/**
 * @Title: Diamond.java
 * @Description: [程序流程控制语句 -> for语句] | 非标准示例，多重 for 循环示例，空心菱形图案打印
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Diamond {
    private static String ASTERISK = "*";
    private static String SPACE = " ";
    
    public static void main(String[] args) {
        int length = 1; // 默认菱形的边长（* 的数量）

        Scanner sc = new Scanner(System.in); // 输入数据的扫描对象
        System.out.println("请输入菱形的边长：");
        length = sc.nextInt(); // 获取输入的边长数值
        /* 打印菱形的上半部分三角形 行数为 length 行 */
        for (int i = 1; i <= length; i++) { // 需要打印的行数
            for (int j = 1; j <= length - i; j++) {
                System.out.print(SPACE);
            }
            for (int k = 1; k <= 2 * i - 1; k++) { // 打三角形的区域
                if (k == 1 || k == 2 * i - 1) { // 空心的获取 除每行第一个以及最后一个外其余为空白
                    System.out.print(ASTERISK);
                } else {
                    System.out.print(SPACE);
                }
            }
            System.out.println();
        }

        /* 打印菱形的下半部分三角形，中间行共用，故行数为 length-1 */
        for (int i = length - 1; i > 0; i--) {
            for (int j = 1; j <= length - i; j++) {
                System.out.print(SPACE);
            }
            for (int k = 1; k <= 2 * i - 1; k++) {
                if (k == 1 || k == 2 * i - 1) {
                    System.out.print(ASTERISK);
                } else {
                    System.out.print(SPACE);
                }
            }
            System.out.println();
        }
    }
}
