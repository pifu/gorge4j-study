package com.gorge4j;

/**
 * @Title: To.java
 * @Description: [Java语言的基本语法 -> 数据类型 -> 基本数据类型 -> 整型] | 非标准示例，进制转换
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class To {
    private static char[] a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    public static void main(String[] args) {
        int number = 20;
        System.out.println(number + " 转二进制: " + toNumber(number, 2));
        System.out.println(number + " 转八进制: " + toNumber(number, 8));
        System.out.println(number + " 转十六进制: " + toNumber(number, 16));
    }

    public static String toNumber(int number, int n) {
        String str = "";
        for (int i = 0; i < n; i++) {
            if (number == i) {
                str = a[i] + str;
                return str;
            }
        }
        str = a[number % n] + str;
        str = toNumber(number / n, n) + str;
        return str;
    }
}
