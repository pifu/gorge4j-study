package com.gorge4j;

/**
 * @Title: Object4.java
 * @Description: [对象与方法 -> 对象的创建与销毁]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Object4 {
    Object4() {
        System.out.println("对象创建");
    }

    void hi() {
        System.out.println("您好~~~");
    }

    public static void main(String[] args) {
        Object4 ob = null;
        if (ob == null) { // 若对象不存在，则创建它
            ob = new Object4();
        }
        ob.hi();
    }
}
