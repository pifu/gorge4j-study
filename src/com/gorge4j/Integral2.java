package com.gorge4j;

/**
 * @Title: Integral2.java
 * @Description: [Java语言的基本语法 -> 数据类型 -> 基本数据类型 -> 整型]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 13:57:32
 * @version v1.0
 */

public class Integral2 {
    public static void main(String[] args) {
        int i1 = 12; // 十进制（0、1、2、3、4、5、6、7、8、9，逢十进一位）
        int i2 = 012; // 八进制（0、1、2、3、4、5、6、7，逢八进一位）
        int i3 = 0x12; // 十六进制（0、1、2、3、4、5、6、7、8、9、A、B、C、D、E、F、G，逢十六进一位）
        int i4 = 0xABCD; // 十六进制（0、1、2、3、4、5、6、7、8、9、A、B、C、D、E、F、G，逢十六进一位）
        System.out.println(i1);
        System.out.println(i2);
        System.out.println(i3);
        System.out.println(i4);
    }
}
