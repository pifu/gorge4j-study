package com.gorge4j;

/**
 * @Title: While2.java
 * @Description: [程序流程控制语句 -> while语句]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class While2 {
    public static void main(String[] args) {
        int a = 1, b;
        while (a <= 9) {
            b = 1;
            while (b <= a) {
                System.out.print(b + "*" + a + "=" + a * b + " ");
                b++;
            }
            a++;
            System.out.println();
        }
    }
}
