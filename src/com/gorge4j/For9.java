package com.gorge4j;

/**
 * @Title: For9.java
 * @Description: [程序流程控制语句 -> for语句]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 12:55:54
 * @version v1.0
 */

public class For9 {
    public static void main(String[] args) {
        for (int a = 1; a <= 2; a++) {
            for (int b = 1; b <= 3; b++) {
                System.out.println("a=" + a + " b=" + b);
            }
        }
    }
}
