package com.gorge4j;

/**
 * @Title: Class1.java
 * @Description: [类与对象 -> 类的定义与对象的创建]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

class Man { // 定义 Man 类
    int height;
    int age;
}


public class Class1 { // 一个文件中，只允许存在一个 public 类，并且 public 类名要与文件名保持一致
    public static void main(String[] ages) {
        Man man1; // 声明对象，简单来说就是给对象起个名字
        man1 = new Man(); // 创建对象
        man1.height = 180; // 为 man1 对象的 height 变量赋值为 180
        man1.age = 20; // 为 man1 对象的 age 变量赋值为 20
        System.out.println(man1.height); // 打印输出 man1 对象的 height 值
        System.out.println(man1.age); // 打印输出 man1 对象的 age 值
    }
}
