package com.gorge4j;

/**
 * @Title: Object12.java
 * @Description: [对象与方法 -> 获取命令行参数]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Object12 {
    private String key;

    Object12(String key) {
        this.key = key;
    }

    String getKey() {
        return key;
    }

    void setKey(String key) {
        this.key = key;
    }

    public static void main(String[] args) {
        Object12 ob = new Object12("hello");
        String mkey = ob.getKey();
        System.out.println(mkey);
        ob.setKey("Hi");
        System.out.println(ob.getKey());
    }
}
