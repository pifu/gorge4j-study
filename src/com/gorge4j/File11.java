package com.gorge4j;

import java.io.*;
import com.gorge4j.util.FileUtil;

/**
 * @Title: File11.java
 * @Description: [Java输入与输出 -> 流 -> 字符流 -> InputStreamReader和OutputStreamWriter] | 字符流（character
 *               streams）
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class File11 {
    public static void main(String[] args) throws Exception {
        String filePath = FileUtil.generateFilePathByFileName("char.txt"); // 文件路径
        // Character Output Stream
        FileOutputStream fos = new FileOutputStream(filePath);
        OutputStreamWriter osw = new OutputStreamWriter(fos);
        osw.write("中"); // 写一字符到输入流
        osw.write("国");
        osw.close();
        FileInputStream fis = new FileInputStream(filePath);
        InputStreamReader isr = new InputStreamReader(fis);
        System.out.println((char) isr.read()); // 读取一字符串
        System.out.println((char) isr.read());
        isr.close();
    }
}
