package com.gorge4j;

/**
 * @Title: Class32.java
 * @Description: [类与对象 -> 静态块]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 09:59:51
 * @version v1.0
 */

public class Class32 {
    static int a;
    static {
        System.out.println("Hi~~~");
        a = 30;
    }

    public static void main(String[] args) {
        System.out.println("哈~~~");
        System.out.println(a);
    }
}
