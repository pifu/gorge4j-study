package com.gorge4j;

/**
 * @Title: Excep8.java
 * @Description: [异常处理 -> 可抛出异常的方法] | 异常方法抛出异常（特定条件触发）
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Excep8 {
    static void go(int size) throws NegativeArraySizeException {
        int[] a = new int[size]; // size为负值时，发生异常
        System.out.println(a.length);
    }

    public static void main(String[] args) {
        try {
            go(10); // 未发生异常
            // go(-10); // 会发生异常
        } catch (NegativeArraySizeException e) {
            System.out.println("数组长度为负");
        }
    }
}
