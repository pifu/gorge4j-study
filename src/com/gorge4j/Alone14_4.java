package com.gorge4j;

/**
 * @Title: Alone14_4.java
 * @Description: [线程 -> 线程同步 -> 同步化方法]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 21:09:18
 * @version v1.0
 */

public class Alone14_4 extends Thread {
    IntArrayBuffer iab;
    int num;
    
    public Alone14_4(IntArrayBuffer iab, int num) {
        this.iab = iab;
        this.num = num;
    }
    
    public void run() {
        iab.add(new int[] {num, num, num, num, num});
    }
    
    public static void main(String[] args) {
        IntArrayBuffer iab = new IntArrayBuffer();
        Alone14_4 ob1 = new Alone14_4(iab, 1);
        Alone14_4 ob2 = new Alone14_4(iab, 2);
        ob1.start();
        ob2.start();
        try {
            ob1.join();
            ob2.join();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(iab.sb);
    }
    
    static class IntArrayBuffer {
        StringBuffer sb = new StringBuffer();
        
        synchronized void add(int[] array) {
            for (int i = 0; i < array.length; i++) {
                sb.append(array[i]);
                try {
                    Thread.sleep(100);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
}
