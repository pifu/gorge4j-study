package com.gorge4j;

import java.io.*;
import com.gorge4j.util.FileUtil;

/**
 * @Title: Serialize2.java
 * @Description: [Java输入与输出 -> 流 -> 对象系列化 -> ObjectInputStream] | ObjectInputStream 类使用
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

class Man2 { // 定义 Man 类
    String name;
    int age;
    int height;
}


public class Serialize2 {
    public static void main(String[] args) throws Exception {
        String filePath = FileUtil.generateFilePathByFileName("obj.dat"); // 文件路径
        FileInputStream fis = new FileInputStream(filePath);
        ObjectInputStream ois = new ObjectInputStream(fis);
        Man2 m;
        m = (Man2) ois.readObject();
        ois.close();
        System.out.println(m.name);
        System.out.println(m.age);
        System.out.println(m.height);
    }
}
