package com.gorge4j;

/**
 * @Title: Alone12_2.java
 * @Description: [常用API之一 -> java.lang.StringBuffer] | 需求：使用 StringBuffer
 *               类的方法，利用字符串“abcdefg”实现下列结果输出：abcdEFG、AbCdEfG、gfedcba
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 01:00:31
 * @version v1.0
 */

class Alone12_2 {
    public static void main(String[] args) {
        StringBuffer sb = new StringBuffer("abcdefg");
        sb = sb.replace(0, sb.length(), "AbCdEfG");
        System.out.println(sb);
        sb = sb.reverse();
        System.out.println(sb);
        sb = sb.delete(1, 7);
        sb = sb.insert(1, "FEDCBA");
        System.out.println(sb);
    }
}
