package com.gorge4j;

/**
 * @Title: Alone3_2.java
 * @Description: 需求：编写一个程序，判断整数 n (=20) 的值是正数还是负数或 0
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 17:55:14
 * @version v1.0
 */

public class Alone3_2 {
    public static void main(String[] args) {
        int n = 20;
        String s1 = n > 0 ? "正数" : "负数或零";
        String s2 = n > 0 ? "正数" : (n < 0 ? "负数" : "零"); // 三目运算符可以嵌套，但是一般不推荐这么写，可读性太差
        System.out.println(s1);
        System.out.println(s2);
    }
}
