package com.gorge4j;

/**
 * @Title: Alone4_8.java
 * @Description: 需求：按要求输出数字系列（效果见下方运行结果的注释）
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:29:37
 * @version v1.0
 */

public class Alone4_8 {
    public static void main(String[] args) {
        for (int i = 1; i <= 5; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print(i + j - 1);
            }
            System.out.println();
        }
        // 另外一种写法
        // for (int i = 1; i <= 5; i++) {
        //     for (int j = i; j <= 2 * i - 1; j++) {
        //         System.out.print(j);
        //     }
        //     System.out.println();
        // }
    }
}

// 运行结果：
// 1
// 23
// 345
// 4567
// 56789