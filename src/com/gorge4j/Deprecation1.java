package com.gorge4j;

/**
 * @Title: Deprecation1.java
 * @Description: [线程 -> stop()、suspend()和resume()] | 终止进程
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Deprecation1 extends Thread {
    public void run() {
        while (true)
            System.out.println("Hi~~~");
    }

    public static void main(String[] args) {
        Deprecation1 dt = new Deprecation1();
        dt.start();
        dt.stop(); // deprecated API，新版本已停用的方法，不赞成使用
    }
}
