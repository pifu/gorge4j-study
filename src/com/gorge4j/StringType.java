package com.gorge4j;

/**
 * @Title: StringType.java
 * @Description: [Java语言的基本语法 -> 数据类型 -> 基本数据类型 -> 对象类型] | 字符串类型对象示例
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 14:03:59
 * @version v1.0
 */

public class StringType {
    public static void main(String[] args) {
        String s1 = "您好";
        String s2 = new String("Hello");
        System.out.println(s1);
        System.out.println(s2);
    }
}
