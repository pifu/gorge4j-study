package com.gorge4j;

/**
 * @Title: Class31.java
 * @Description: [类与对象 -> 类的初始化]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 09:59:51
 * @version v1.0
 */

public class Class31 {
    int a = 10;
    static int b = 20;

    public static void main(String[] arg) {
        Class31 ob = new Class31();
        System.out.println(ob.a);
        System.out.println(Class31.b);
    }
}
