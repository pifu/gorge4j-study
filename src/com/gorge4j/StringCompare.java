package com.gorge4j;

/**
 * @Title: StringCompare.java
 * @Description: [常用API之一 -> java.lang.String] | 非标准样例，字符串对象比对
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 23:51:46
 * @version v1.0
 */

public class StringCompare {
    public static void main(String[] args) {
        String s1 = "abc";
        String s2 = "abc";
        String s3 = new String("abc");
        String s4 = new String("abc");

        System.out.println(s1 == s2);
        System.out.println(s1.equals(s2));

        System.out.println(s3 == s4);
        System.out.println(s3.equals(s4));
    }
}

