package com.gorge4j;

/**
 * @Title: For16.java
 * @Description: [程序流程控制语句 -> for语句]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class For16 {
    public static void main(String[] args) {
        for (int a = 7; a <= 9; a++) {
            for (int b = 7; b <= 9; b++) {
                for (int c = 7; c <= 9; c++) {
                    if (a + b + c == 25) {
                        System.out.println(a + "+" + b + "+" + c + "=" + (a + b + c));
                    }
                }
            }
        }
    }
}
