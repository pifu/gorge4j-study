package com.gorge4j;

/**
 * @Title: Inher10.java
 * @Description: [继承 -> 引用变量的类型转换] | 引用变量的类型转换
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 12:55:54
 * @version v1.0
 */

public class Inher10 {
    public static void main(String[] args) {
        Super ob1 = new Sub();
        Sub ob2 = (Sub) ob1;
        ob2.hi();
        ob2.bye();
    }
}
