package com.gorge4j;

/**
 * @Title: Class28.java
 * @Description: [类与对象 -> 构造函数]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 09:59:51
 * @version v1.0
 */

public class Class28 {
    int a;

    public void Class28() {
        a = 10;
    }

    public static void main(String[] args) {
        Class28 ob = new Class28();
        System.out.println(ob.a);
    }
}
