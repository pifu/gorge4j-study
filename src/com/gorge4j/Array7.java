package com.gorge4j;

/**
 * @Title: Array7.java
 * @Description: [数组 -> 对象数组]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Array7 {
    public static void main(String[] args) {
        int[] a = new int[2];
        String[] b = new String[2];
        System.out.println(a[0] + " " + a[1]);
        System.out.println(b[0] + " " + b[1]);
    }
}
