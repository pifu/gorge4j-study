package com.gorge4j;

/**
 * @Title: Alone6_3.java
 * @Description: [数组 -> 二维数组] | 需求：编写程序，输出注释的运行结果的图案
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-09 00:16:32
 * @version v1.0
 */

public class Alone6_3 {
    public static void main(String[] args) {
        char[][] ch = new char[10][];
        for (int i = 0; i < ch.length; i++) {
            ch[i] = new char[10];
        }

        // 二维数组赋值
        for (int i = 0; i < ch.length; i++) {
            for (int j = 0; j < ch[i].length; j++) {
                if (i == 0 || j == 0 || i == 9 || j == 9 || i == j || i + j == 9) {
                    ch[i][j] = '☆'; // 图案边框及对角线赋值空心五角星
                } else {
                    ch[i][j] = '★'; // 非图案边框及对角线赋值实心五角星
                }
            }
        }

        // 二维数组打印
        for (int i = 0; i < ch.length; i++) {
            for (int j = 0; j < ch[i].length; j++) {
                System.out.print(ch[i][j]);
            }
            System.out.println();
        }
    }
}
