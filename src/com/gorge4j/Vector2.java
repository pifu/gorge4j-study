package com.gorge4j;

import java.util.*;
import java.io.*;

/**
 * @Title: Vector2.java
 * @Description: [常用API之二 -> Collection接口 -> List接口] | Vector 使用，提供了元素的添加、删除、输出功能
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Vector2 {

    // 输出 Vector v 中的所有元素
    static void displayVector(Vector<String> v) {
        System.out.println("\n----------目录----------");

        for (int i = 0; i < v.size(); i++) {
            System.out.print(v.elementAt(i) + " ");
        }

        System.out.println("\n------------------------\n");

    }

    public static void main(String[] args) throws IOException {
        Vector<String> v = new Vector<String>();
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        String select; // 菜单选择变量

        while (true) { // 死循环
            System.out.println("<<  1.添加 2.删除 3.退出   >>");
            System.out.println("请选择。。。");
            select = br.readLine(); // 读取用户的选择
            if (select.equals("1")) { // 若选中菜单项 1
                System.out.println("请输入追加的字符串：");
                v.add(br.readLine()); // 读取键盘输入的字符串，并添加至 v 中
                displayVector(v);
            } else if (select.equals("2")) { // 若选中菜单项 2
                System.out.println("请输入元素编号（从0开始）：");
                v.removeElementAt(Integer.parseInt(br.readLine())); // 删除指定编号的元素
                displayVector(v);
            } else if (select.equals("3")) {
                break; // 若选中了菜单项 3，则退出 while 循环
            }
        }

    }

}
