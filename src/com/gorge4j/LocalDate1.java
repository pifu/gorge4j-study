package com.gorge4j;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;

/**
 * @Title: LocalDate1.java
 * @Description: [常用API之三 -> java.time.LocalDate类] ｜ 只含年月日的日期对象，LocalDate: 表示没有时区的日期, 是不可变并且线程安全的
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:19:16
 * @version v1.0
 * @since 1.8
 */

public class LocalDate1 {

    public static void main(String[] args) {
        // 获取当前运行环境的日期，固定格式 yyyy-MM-dd
        LocalDate localDate = LocalDate.now();
        System.out.println(localDate);

        // 根据指定日期/时间创建对象，例如：根据年、月、日取日期，固定格式 yyyy-MM-dd
        LocalDate ofDate = LocalDate.of(2019, 11, 11);
        System.out.println(ofDate);

        // 根据字符串取日期，固定格式 yyyy-MM-dd
        LocalDate parseDate = LocalDate.parse("2019-11-11");
        // 关于闰年的日期，不是闰年传入29会报异常：
        // Text '2019-02-29' could not be parsed: Invalid date 'February 29' as '2019' is not a leap year
        // LocalDate parseDate = LocalDate.parse("2019-02-29");
        System.out.println(parseDate);

        // 取本月第1天：2019-11-01
        LocalDate firstDayOfThisMonth = localDate.with(TemporalAdjusters.firstDayOfMonth());
        System.out.println(firstDayOfThisMonth);

        // 取本月第2天：2019-11-02
        LocalDate secondDayOfThisMonth = localDate.withDayOfMonth(2);
        System.out.println(secondDayOfThisMonth);

        // 取本月最后一天，再也不用计算是 28、29、30 还是 31，例如 2019-11-30
        LocalDate lastDayOfThisMonth = localDate.with(TemporalAdjusters.lastDayOfMonth());
        System.out.println(lastDayOfThisMonth);

        // 取下一天，例如 2019-11-13
        LocalDate nextDay = localDate.plusDays(1);
        System.out.println(nextDay);

        // 取 2019 年 11 月第一个周三，例如 2019-11-06
        LocalDate thirdMondayOf201911 =
                LocalDate.parse("2019-11-01").with(TemporalAdjusters.firstInMonth(DayOfWeek.WEDNESDAY));
        System.out.println(thirdMondayOf201911);
    }

}
