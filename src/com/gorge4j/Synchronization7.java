package com.gorge4j;

/**
 * @Title: Synchronization7.java
 * @Description: [线程 -> 线程同步 -> 多消费者] | 多消费者示例（多营业员）
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 23:51:46
 * @version v1.0
 */

// 厨师类
class HMaker2 extends Thread {
    void make() {
        synchronized (Ham.box) {
            Ham.production++;
            System.out.println("厨师：这里有汉堡！！(总共 " + Ham.production + " 个)");
            Ham.box.notifyAll();
        }
    }

    public void run() {
        while (Ham.production < Ham.totalMaterial) {
            try {
                sleep(3000);
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }
            make();
        }
    }

}


// 营业员类
class HAssistant2 extends Thread {
    void sell() {
        synchronized (Ham.box) {
            if (Ham.production == Ham.sales) {
                System.out.println(getName() + " 先生/小姐，请您稍等！");
                try {
                    Ham.box.wait();
                } catch (InterruptedException ie) {
                }
            }

            if (Ham.production > Ham.sales) {
                Ham.sales++;
                System.out.println(getName() + ":顾客朋友们，汉堡包上来了！(总共 " + Ham.sales + " 个)");
            }

        }
    }

    public void run() {
        while (Ham.sales < Ham.totalMaterial) {
            System.out.println("<顾客向" + getName() + "订购汉堡包.>");
            sell();
            try {
                sleep(1000);
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }
        }
    }

}


// 主程序
public class Synchronization7 {
    public static void main(String[] args) {
        HMaker2 maker = new HMaker2();
        HAssistant2 assistant1 = new HAssistant2();
        HAssistant2 assistant2 = new HAssistant2();
        assistant1.setName("A");
        assistant2.setName("B");
        maker.start();
        assistant1.start();
        assistant2.start();
    }
}
