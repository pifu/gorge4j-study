package com.gorge4j;

/**
 * @Title: Operator6.java
 * @Description: [Java语言的基本语法 -> 运算符 -> 关系运算符]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Operator6 {
    public static void main(String[] args) {
        int a = 1;
        int b = 2;
        System.out.println(a == b);
        System.out.println(a > b);
        System.out.println(!(a > b));
        System.out.println(b == 2);
        System.out.println(!(a != b));
        System.out.println();
        System.out.println(true == false);
        System.out.println(true == (1 > 0));
        System.out.println(!!!!true);
    }
}
