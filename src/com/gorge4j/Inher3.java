package com.gorge4j;

/**
 * @Title: Inher3.java
 * @Description: [继承 -> this与super] | 在父类构造函数被重载的情形下，使用 super(…) 来调用父类的构造函数
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 12:55:54
 * @version v1.0
 */

class Inher3_1 {
    int a;

    Inher3_1() { // 构造函数
        a = 1;
    }

    Inher3_1(int a) { // 构造函数
        this.a = a;
    }
}


public class Inher3 extends Inher3_1 {
    int b;

    Inher3(int a, int b) { // 构造函数
        super(a); // 如无此行，程序将自动调用父类的无参构造函数Inher3_1()
        this.b = b;
    }

    public static void main(String[] args) {
        Inher3 ob = new Inher3(10, 20);
        System.out.println(ob.a + " " + ob.b);
    }
}
