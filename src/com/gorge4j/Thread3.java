package com.gorge4j;

/**
 * @Title: Thread3.java
 * @Description: [线程 -> 创建线程 -> 创建线程的另一种方法] | 继承 Thread 类创建线程
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

class SumThread3 extends Thread {
    int from;
    int to;
    long sum;

    SumThread3(int from, int to) {
        this.from = from;
        this.to = to;
    }

    long getSum() {
        return sum;
    }

    public void run() {
        for (int i = from; i <= to; i++)
            sum += i;
    }
}


public class Thread3 {
    public static void main(String[] args) {
        SumThread3 st = new SumThread3(1, 1000);
        st.start();
        System.out.println(st.getSum());
        System.out.println(st.isAlive());
        // System.out.println(st.getSum());
    }
}
