package com.gorge4j;

/**
 * @Title: If4.java
 * @Description: [程序流程控制语句 -> if语句格式3、4]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 13:18:18
 * @version v1.0
 */

public class If4 {
    public static void main(String[] args) {
        int su = 790;
        if (su < 10000 && su >= 1000)
            System.out.println("4位数");
        else if (su < 1000 && su >= 100)
            System.out.println("3位数");
        else if (su < 100 && su >= 10)
            System.out.println("2位数");
        else if (su >= 0)
            System.out.println("1位数");
        else
            System.out.println("--抱歉，无法做出判断--");
    }
}
