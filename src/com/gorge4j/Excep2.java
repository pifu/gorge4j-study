package com.gorge4j;

/**
 * @Title: Excep2.java
 * @Description: [异常处理 -> try-catch语句] | 除 0 异常
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Excep2 {
    public static void main(String[] args) {
        System.out.println("程序开始");
        int a = 1, b = 0;
        try {
            int c = a / b; // a 被 0 除了，异常对象在此生成
            System.out.println(c); // 因为上一行有异常产生，所以此行不会执行
        } catch (ArithmeticException ae) { // 此行捕获到异常
            System.out.println("不能被 0 除"); // 对产生的异常进行处理
        }
        System.out.println("退出程序"); // 执行完此行，程序正常退出
    }
}
