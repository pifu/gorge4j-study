package com.gorge4j;

/**
 * @Title: FinalTest1.java
 * @Description: [抽象类与接口 -> final关键字] | 非标准示例，final 关键字使用
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

class FinalTest1 {
    public void f1() {
        System.out.println("f1");
    }

    // 方法定义为 final，无法被子类覆盖的方法
    public final void f2() {
        System.out.println("f2");
    }

    public void f3() {
        System.out.println("f3");
    }

    private void f4() {
        System.out.println("f4");
    }
}


public class FinalTest2 extends FinalTest1 {
    public void f1() {
        System.out.println("Test1 父类方法 f1 被覆盖!");
    }

    public static void main(String[] args) {
        FinalTest2 t = new FinalTest2();
        t.f1();
        t.f2(); // 调用从父类继承过来的 final 方法
        t.f3(); // 调用从父类继承过来的方法
        // t.f4(); // 调用失败，无法从父类继承获得
    }
}
