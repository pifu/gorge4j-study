package com.gorge4j;

/**
 * @Title: Identifier.java
 * @Description: [Java语言的基本语法 -> 标识符]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 23:14:56
 * @version v1.0
 */

public class Identifier { // 类名 JavaIdentifier 是标识符

    public static void main(String[] args) { // 方法名 main 是标识符

        int a = 10; // Java 里的标识符可以是字母，变量 a 是标识符，下同
        int A = 20; // Java 里的标识符区分大小写，变量 a 和 A 是不同的变量
        int 中国 = 30; // Java 里的标识符可以是汉字（Java 里标识符支持 Unicode 编码，理论上其它国家文字也支持）
        int $b = 40; // Java 里的标识符可以是美元符号
        int _c = 50; // Java 里的标识符可以是下划线
        int d4 = 60; // Java 里的标识符可以是数字（注意不能以数字开头）
        int aA中国$_4 = 70; // Java 里的标识符只能是大/小写字母、数字、美元符号、下划线及上述组合，且不能以数字开头，不能为 Java 关键字，最大长度为 65534 个字符
        // 以下是各个变量值的打印
        System.out.println("a 的值是：" + String.valueOf(a));
        System.out.println("A 的值是：" + String.valueOf(A));
        System.out.println("中国 的值是：" + String.valueOf(中国));
        System.out.println("$b 的值是：" + String.valueOf($b));
        System.out.println("_c 的值是：" + String.valueOf(_c));
        System.out.println("d4 的值是：" + String.valueOf(d4));
        System.out.println("aA中国$_4 的值是：" + String.valueOf(aA中国$_4));

    }

}
