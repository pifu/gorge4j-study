package com.gorge4j;

import java.io.*;
import com.gorge4j.util.FileUtil;

/**
 * @Title: File4.java
 * @Description: [Java输入与输出 -> 流 -> FileInputStream和FileOutputStream] | FileOutputStream 类的常用构造函数
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class File4 {
    public static void main(String[] args) throws Exception { // 抛出异常
        String filePath = FileUtil.generateFilePathByFileName("alphabet.txt"); // 文件路径
        FileOutputStream fos = new FileOutputStream(filePath);
        for (int i = 'A'; i <= 'Z'; i++) {
            fos.write(i); // 若向 fos 传送数据，fos 会将其写入文件中
        }
        fos.close(); // 使用完毕，关闭文件输出流
    }
}
