package com.gorge4j;

/**
 * @Title: Math1.java
 * @Description: [常用API之一 -> java.lang.Math] | Math 类常用方法
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Math1 {
    public static void main(String[] args) {
        double a = Math.sin(Math.PI / 2);
        System.out.println(a);

        double b = Math.cos(Math.toRadians(60));
        System.out.println(b);

        System.out.println(Math.log(Math.E));

        System.out.println(Math.sqrt(9));
    }
}
