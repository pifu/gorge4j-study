package com.gorge4j;

/**
 * @Title: String4.java
 * @Description: [常用API之一 -> java.lang.String] | String 常用方法
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 23:51:46
 * @version v1.0
 */

public class String4 {
    public static void main(String[] args) {
        String s1 = new String("This is a String");
        System.out.println(s1.length()); // 字符串长度
        System.out.println(s1.replace('i', 'Q')); // 字符串替换
        System.out.println(s1.replaceAll("is", "IS")); // 字符串替换所有
        System.out.println(s1.startsWith("This")); // 字符串开始字符的检测
        System.out.println(s1.substring(5)); // 取字符串里的部分
        System.out.println(s1.substring(5, 13)); // 取字符串里的指定部分
    }
}
