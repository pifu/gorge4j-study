package com.gorge4j;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import com.gorge4j.util.FileUtil;

/**
 * @Title: Alone13_3.java
 * @Description: [Java输入与输出 -> 流 -> FilterStream-DataInputStream和DataOutputStream] | 需求：使用
 *               DataInputStream 类，将 Alone13_2 中 stu.dat 文件中的数据读到 Student 对象数组并打印出来
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 14:32:16
 * @version v1.0
 */

public class Alone13_3 {
    public static void main(String[] args) throws Exception {
        String filePath = FileUtil.generateFilePathByFileName("stu.dat"); // 文件路径
        File f = new File(filePath);
        FileInputStream fis = new FileInputStream(f);
        DataInputStream dis = new DataInputStream(fis);
        Student[] students = new Student[6];
        for (int i = 0; i < students.length; i++) {
            students[i] = new Student(dis.readUTF(), dis.readInt(), dis.readInt(), dis.readInt(), dis.readInt());
        }

        for (Student student : students) {
            System.out.println(student.getName() + " " + student.getChi() + " "
                    + student.getEng() + " " + student.getMat() + " " + student.getSci());
        }

        dis.close();

    }
}


class Student {
    String name;
    int chi;
    int eng;
    int mat;
    int sci;

    public Student(String name, int chi, int eng, int mat, int sci) {
        this.name = name;
        this.chi = chi;
        this.eng = eng;
        this.mat = mat;
        this.sci = sci;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getChi() {
        return chi;
    }

    public void setChi(int chi) {
        this.chi = chi;
    }

    public int getEng() {
        return eng;
    }

    public void setEng(int eng) {
        this.eng = eng;
    }

    public int getMat() {
        return mat;
    }

    public void setMat(int mat) {
        this.mat = mat;
    }

    public int getSci() {
        return sci;
    }

    public void setSci(int sci) {
        this.sci = sci;
    }

}
