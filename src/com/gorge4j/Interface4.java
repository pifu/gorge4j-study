package com.gorge4j;

/**
 * @Title: Interface4.java
 * @Description: [抽象类与接口 -> 接口] | 接口直接继承接口
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 12:55:54
 * @version v1.0
 */

interface MyInter4_1 {
    public void method1();
}


interface MyInter4_2 extends MyInter4_1 {
    public void method2();
}


public class Interface4 implements MyInter4_2 {
    public void method1() { // 覆盖
        System.out.println("method1 override");
    }

    public void method2() { // 覆盖
        System.out.println("method2 override");
    }

    public static void main(String[] args) {
        Interface4 ob = new Interface4();
        ob.method1();
        ob.method2();
    }
}
