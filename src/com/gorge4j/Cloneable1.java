package com.gorge4j;

/**
 * @Title: Cloneable1.java
 * @Description: [抽象类与接口 -> Cloneable接口] | Cloneable 接口定义
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Cloneable1 implements Cloneable {
    String s;
    int a;

    public Cloneable1(String s, int a) {
        this.s = s;
        this.a = a;
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException cse) {
        }
        return null;
    }

    public static void main(String[] args) {
        Cloneable1 ob1 = new Cloneable1("abcd", 10);
        Cloneable1 ob2 = (Cloneable1) ob1.clone();
        System.out.println("原本：" + ob1.hashCode());
        System.out.println("副本：" + ob2.hashCode());
        System.out.println("原本：" + ob1.s + " " + ob1.a);
        System.out.println("副本：" + ob2.s + " " + ob2.a);
    }
}
