package com.gorge4j;

/**
 * @Title: String3.java
 * @Description: [常用API之一 -> java.lang.String] | String 常用方法
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 23:51:46
 * @version v1.0
 */

public class String3 {
    public static void main(String[] args) {
        String s1 = new String("This is a String");
        System.out.println(s1.indexOf('i'));
        System.out.println(s1.indexOf('i', 7));
        System.out.println(s1.indexOf("is"));
        System.out.println(s1.indexOf("is", 5));
        System.out.println(s1.lastIndexOf("is"));
    }
}
