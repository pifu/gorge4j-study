package com.gorge4j;

/**
 * @Title: Class27.java
 * @Description: [类与对象 -> 构造函数]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 09:59:51
 * @version v1.0
 */

public class Class27 {
    int a;

    public Class27() {
        a = 10;
    }

    public Class27(int a) {
        this.a = a;
    }

    public static void main(String[] args) {
        Class27 ob1 = new Class27();
        Class27 ob2 = new Class27(20);
        Class27 ob3 = new Class27(30);
        System.out.println(ob1.a);
        System.out.println(ob2.a);
        System.out.println(ob3.a);
    }
}
