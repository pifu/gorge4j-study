package com.gorge4j;

/**
 * @Title: Operator4.java
 * @Description: [Java语言的基本语法 -> 运算符 -> 增减运算符]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:14:17
 * @version v1.0
 */

public class Operator4 {
    public static void main(String[] args) {
        int a = 10, b = 10;
        int c = --a; // 前置递减运算符，先运算后赋值
        int d = b--; // 后置递减运算符，先赋值后运算
        System.out.println("a = " + a);
        System.out.println("b = " + b);
        System.out.println("c = " + c);
        System.out.println("d = " + d);
    }
}
