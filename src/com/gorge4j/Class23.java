package com.gorge4j;

/**
 * @Title: Class23.java
 * @Description: [类与对象 -> 重载] | 注意：此例为错误的演示示例，编译无法通过
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Class23 {
    void hi() {
        System.out.println("您好~~");
    }

    int hi() {
        System.out.println("先生 您好~~");
        return 1;
    }

    public static void main(String[] args) {
        Class23 ob = new Class23();
        ob.hi();
    }
}
