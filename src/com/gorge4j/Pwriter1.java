package com.gorge4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

/**
 * @Title: Pwriter1.java
 * @Description: [Java输入与输出 -> 流 -> 字符流 -> PrintStream与PrintWriter] | 使用 BufferedReader 与
 *               PrintWriter 类，并分别用 System.in 与 System.out 进行输入输出
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Pwriter1 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        PrintWriter pw = new PrintWriter(System.out, true); // 自动刷新
        String s;
        pw.println("请输入字符");
        while (!(s = br.readLine()).equals("")) { // 若输出字符，条件变为假
            pw.println(s);
        }
        br.close();
        pw.close();
    }
}
