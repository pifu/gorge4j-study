package com.gorge4j;

/**
 * @Title: Casting.java
 * @Description: [Java语言的基本语法 -> 类型转换 -> 强制类型转换]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 21:01:40
 * @version v1.0
 */

public class Casting {

    public static void main(String[] args) {

        byte a = 7;
        int b = 2;
        double c = 3.15;
        double d = a / b + c; // 按照自动类型转换的规则，a / b 会按照 int/int 的形式来进行运算，就是结果还是 int，后面 int / int + double 才会转换为 double类型
        double e = (double) a / b + c; // (double) a 将变量 a 强制转换为双精度 double 类型
        System.out.println(d); // 结果：6.15
        System.out.println(e); // 结果：6.65

    }

}
