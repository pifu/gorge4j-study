package com.gorge4j;

/**
 * @Title: Inher1.java
 * @Description: [继承 -> 继承的概念]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 12:55:54
 * @version v1.0
 */

class Inher1_1 {
    int a;

    void hi() {
        System.out.println("Hi~~");
    }
}


public class Inher1 extends Inher1_1 {
    public static void main(String[] args) {
        Inher1 ob = new Inher1();
        ob.a = 10;
        ob.hi();
    }
}
