package com.gorge4j;

/**
 * @Title: For5.java
 * @Description: [程序流程控制语句 -> for语句]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:37:03
 * @version v1.0
 */

public class For5 {
    public static void main(String[] args) {
        // 注意：下面是一个死循环的示例。
        // 在 for 语句中，若条件表达式始终为真，则会出现死循环的情况，编程时要考虑周全，防止程序陷入死循环中
        for (int i = 1; i >= 1; i++) {
            System.out.println(i);
        }
    }
}
