package com.gorge4j;

/**
 * @Title: Class6.java
 * @Description: [类与对象 -> 成员方法 -> 无参方法]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Class6 {
    void hello() {
        System.out.println("您好！");
    }

    public static void main(String[] args) {
        Class6 class6 = new Class6();
        class6.hello();
        class6.hello();
    }
}
