package com.gorge4j;

/**
 * @Title: Array10.java
 * @Description: [数组 -> 二维数组]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Array10 {
    public static void main(String[] args) {
        int[][] a;
        a = new int[3][];
        int[] sum = new int[3];
        a[0] = new int[] {100, 80, 80, 75, 90};
        a[1] = new int[] {50, 60, 60, 65, 55};
        a[2] = new int[] {100, 100, 100, 100};

        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                sum[i] += a[i][j];
            }
        }

        for (int i = 0; i < sum.length; i++) {
            System.out.println("合计= " + sum[i]);
        }
    }
}
