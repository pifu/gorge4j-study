package com.gorge4j;

/**
 * @Title: Alone3_1.java
 * @Description: 需求：编写一个程序，用来求 20 除以 3 的商与余数
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:26:20
 * @version v1.0
 */

public class Alone3_1 {
    public static void main(String[] args) {
        int a = 20;
        int b = 3;
        int c = a / b;
        int d = a % b;
        System.out.println("20 除以 3 的商是：" + c);
        System.out.println("20 除以 3 的余数是：" + d);
    }
}
