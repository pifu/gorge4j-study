package com.gorge4j;

/**
 * @Title: Alone4_7.java
 * @Description: 需求：输出实心五角星组成的倒直角三角形图案（具体效果见下方运行结果的注释）
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 23:59:22
 * @version v1.0
 */

public class Alone4_7 {
    public static void main(String[] args) {
        for (int i = 1; i <= 5; i++) {
            for (int j = 1; j <= 6 - i; j++) {
                System.out.print('★');
            }
            System.out.println(); // 打印完一行输出一个回车
        }
    }
}

// 运行结果：
// ★★★★★
// ★★★★
// ★★★
// ★★
// ★