package com.gorge4j;

/**
 * @Title: MultiplicationTable1.java
 * @Description: [程序流程控制语句 -> for语句] | 非标准示例，九九乘法口诀表
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 13:22:58
 * @version v1.0
 */

public class MultiplicationTable1 {
    public static void main(String[] args) {
        for (int a = 1; a <= 9; a++) {
            for (int b = 1; b <= 9; b++) {
                System.out.print(a + "*" + b + "=" + a * b + " ");
            }
            System.out.println();
        }
    }
}
