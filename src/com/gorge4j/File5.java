package com.gorge4j;

import java.io.*;
import com.gorge4j.util.FileUtil;

/**
 * @Title: File5.java
 * @Description: [Java输入与输出 -> 流 -> FileInputStream和FileOutputStream] | FileOutputStream 类常用构造函数
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class File5 {
    public static void main(String[] args) throws Exception {
        String filePath = FileUtil.generateFilePathByFileName("alphabet.txt"); // 文件路径
        // 以追加的模式打开文件
        FileOutputStream fos = new FileOutputStream(filePath, true);
        for (int i = 'a'; i <= 'z'; i++) {
            fos.write(i);
        }
        fos.close();
    }
}
