package com.gorge4j;

/**
 * @Title: For13.java
 * @Description: [程序流程控制语句 -> for语句]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class For13 {
    public static void main(String[] args) {
        for (int a = 1; a <= 9; a++) {
            for (int b = 1; b <= 9; b++) {
                System.out.print(a + "*" + b + "=" + a * b + " ");
            }
            System.out.println();
        }

    }
}
