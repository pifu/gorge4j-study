package com.gorge4j;

/**
 * @Title: Operator5.java
 * @Description: [Java语言的基本语法 -> 运算符 -> 位移运算符]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:32:03
 * @version v1.0
 */

public class Operator5 {
    public static void main(String[] args) {
        int a = 100;
        System.out.println(a << 1); // 表示左移 1 位，数值增加 1 倍，相当于 a * 2
        System.out.println(a << 2); // 表示左移 2 位，数值增加 2 倍，相当于 a * 4
        System.out.println(a >> 1); // 表示右移 1 位，数值减半，相当于 a / 2 
        System.out.println(a >> 2); // 表示右移 2 位，数值减半再减半，相当于 a / 4
        System.out.println(a >>> 1); // 表示无符号右移 1 位，无论被移的数是正数还是负数，最左端均填 0 
        System.out.println();
        int b = -100;
        System.out.println(b << 1); // 表示左移 1 位，数值增加 1 倍，相当于 b * 2
        System.out.println(b << 2); // 表示左移 2 位，数值增加 2 倍，相当于 b * 4
        System.out.println(b >> 1); // 表示右移 1 位，数值减半，相当于 b / 2
        System.out.println(b >> 2); // 表示右移 2 位，数值减半再减半，相当于 b / 4
        System.out.println(b >>> 1); // 表示无符号右移 1 位，无论被移的数是正数还是负数，最左端均填 0，因此负数无符号右移后将得到正数 
        System.out.println();
        // 特别注意，位移运算符操作的只是变量的副本，变量的原值不会发生变化
        System.out.println("a 的值是：" + a);
        System.out.println("b 的值是：" + b);
    }
}
