package com.gorge4j;

/**
 * @Title: HashCodeTest.java
 * @Description: [继承 -> Object类] | 返回查看对象的 Hash 码
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 12:55:54
 * @version v1.0
 */


public class HashCodeTest {
    public static void main(String[] args) {
        String ob1 = new String("abc");
        String ob2 = ob1;
        String ob3 = ob2;
        String ob4 = new String("abc");
        System.out.println("ob1的HashCode：" + ob1.hashCode());
        System.out.println("ob2的HashCode：" + ob2.hashCode());
        System.out.println("ob3的HashCode：" + ob3.hashCode());
        System.out.println("ob4的HashCode：" + ob4.hashCode());
        System.out.println(ob1 == ob4);
    }
}
