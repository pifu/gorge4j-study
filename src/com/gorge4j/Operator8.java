package com.gorge4j;

/**
 * @Title: Operator8.java
 * @Description: [Java语言的基本语法 -> 运算符 -> 快速逻辑与和快速逻辑或]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 16:01:57
 * @version v1.0
 */

public class Operator8 {
    public static void main(String[] args) {
        int a = 10;
        System.out.println((a = 20) == 20);
        System.out.println(a);
    }
}
