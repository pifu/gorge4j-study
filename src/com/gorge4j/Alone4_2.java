package com.gorge4j;

/**
 * @Title: Alone4_2.java
 * @Description: 需求：设计一个程序，用于判断某个整数 n 的奇偶性
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:16:46
 * @version v1.0
 */

public class Alone4_2 {
    public static void main(String[] args) {
        int n = 10;
        if (n % 2 == 0) {
            System.out.println(n + " 为偶数");
        } else {
            System.out.println(n + " 为奇数");
        }
    }
}
