package com.gorge4j;

/**
 * @Title: Alone5_1.java
 * @Description: [类与对象 -> 成员方法 -> 多参方法] | 需求：编写一个方法并调用它，求两个整数中的较大者
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 17:44:17
 * @version v1.0
 */

public class Alone5_1 {

    public static void main(String[] args) {
        Alone5_1 ob = new Alone5_1();
        int i = ob.max(10, 20);
        System.out.println("max(10, 20) => " + i);
    }

    public int max(int x, int y) {
        return x > y ? x : y;
    }

}
