package com.gorge4j;

/**
 * @Title: BitNot.java
 * @Description: [Java语言的基本语法 -> 运算符 -> 位非运算符]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 15:24:04
 * @version v1.0
 */

public class BitNot {
    public static void main(String[] args) {
        int a = 7;
        int b = ~a; // 结果为 -8，位非运算符 ~ 是表示位非的单目运算符，它的运算规则是：逢1变0，逢0变1
        System.out.println(b);
        
        // 看下面这个错误的示例
        // byte c = 7;
        // byte d = ~c; // ～c 运算后的结果是整型，再赋值给 byte （字节型）变量，显然是错误的
    }
}
