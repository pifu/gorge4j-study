package com.gorge4j;

/**
 * @Title: Diamond2.java
 * @Description: [程序流程控制语句 -> for语句] | 非标准示例，多重 for 循环示例，空心菱形图案打印
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Diamond2 {
    public static void main(String[] args) {
        int length = 9; // 设置空心菱形默认边长（* 的数量）
        for (int i = 1; i <= length; i++) {
            // 上半部分输出
            if (i <= 5) {
                for (int j = 1; j <= 11; j++) {
                    if (j == i + 5 || j == 7 - i) {
                        System.out.print("*");
                    } else {
                        System.out.print(" ");
                    }
                }
            }
            // 上半部分输出结束

            // 下半部分输出开始
            else {
                for (int k = 1; k <= 9; k++) {
                    if (k == i - 3 || k == 15 - i) {
                        System.out.print("*");
                    } else {
                        System.out.print(" ");
                    }
                }
            }
            // 下半部分输出结束

            System.out.println();
        }
    }
}
