package com.gorge4j;

/**
 * @Title: Object7.java
 * @Description: [对象与方法 -> 引用传递调用]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Object7 {
    public void hi(String name) {
        System.out.println(name + "先生，您好！");
    }

    public static void main(String[] args) {
        Object7 ob = new Object7();
        String s = "天下";
        ob.hi(s);
    }
}
