package com.gorge4j;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

/**
 * @Title: LocalDateTime1.java
 * @Description: [常用API之三 -> java.time.LocalDateTime类] ｜ 同时含有年月日时分秒的日期对象，LocalDateTime: 表示没有时区的日期时间,
 *               是不可变且线程安全的
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-11-12 10:55:43
 * @version v1.0
 */

public class LocalDateTime1 {

    public static void main(String[] args) {
        
        // 获取当前时间，从默认时区的系统时钟获取当前的日期时间，不用考虑时区差
        LocalDateTime localDateTime = LocalDateTime.now();
        System.out.println(localDateTime);
        System.out.println(localDateTime.getYear()); // 年份
        System.out.println(localDateTime.getMonth()); // 月份，大写的英文月份，例如11月份 NOVEMBER
        System.out.println(localDateTime.getMonthValue()); // 月份值，例如11月份就返回 11
        System.out.println(localDateTime.getDayOfMonth()); // 月日期
        System.out.println(localDateTime.getDayOfYear()); // 年日期，即今天是本年度的第多少天
        System.out.println(localDateTime.getDayOfWeek()); // 周星期，即今天是本周的星期几，大写的英文星期，例如星期三 WEDNESDAY
        System.out.println(localDateTime.getHour()); // 时
        System.out.println(localDateTime.getMinute()); // 分
        System.out.println(localDateTime.getSecond()); // 秒
        System.out.println(localDateTime.getNano()); // 微妙
        
        // 根据指定日期/时间（年,月,日,时,分,秒）创建日期对象
        LocalDateTime ofLocalDateTime = LocalDateTime.of(2019, 11, 13, 23, 45, 55);
        System.out.println(ofLocalDateTime);

        // 转化为时间戳（带毫秒值）
        long time1 = LocalDateTime.now().toInstant(ZoneOffset.of("+8")).toEpochMilli();
        long time2 = System.currentTimeMillis();
        
        System.out.println(time1);
        System.out.println(time2);
        
        System.out.println(Instant.now().toEpochMilli());
        System.out.println(Instant.now().atOffset(ZoneOffset.ofHours(8)).toInstant().toEpochMilli());

        // 时间戳转化为 LocalDateTime
        // ZoneId: 时区 ID，用来确定 Instant 和 LocalDateTime 互相转换的规则
        // Instant: 用来表示时间线上的一个点（瞬时）
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
        System.out.println(df.format(LocalDateTime.ofInstant(Instant.ofEpochMilli(time1), ZoneId.of("Asia/Shanghai"))));
        
        // 进行加操作，得到新的日期时间示例
        LocalDateTime plusDays = localDateTime.plusDays(12);
        System.out.println(plusDays);
        
        // 进行减操作，得到新的日期时间示例
        LocalDateTime minusYears = localDateTime.minusYears(2);
        System.out.println(minusYears);

    }

}
