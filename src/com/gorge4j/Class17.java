package com.gorge4j;

/**
 * @Title: Class16.java
 * @Description: [类与对象 -> 包与导入]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Class17 {
    public static void main(String[] args) {
        Class16 ob = new Class16(); // 注：public 类在同一个包内，无需 import
        ob.hi();
    }
}
