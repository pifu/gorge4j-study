package com.gorge4j;

/**
 * @Title: SinglyList1.java
 * @Description: [对象与方法 -> 自引用] | 单向链表示例
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

class Entry {
    Object element; // 定义不确定返回类型的变量
    Entry next; // 自类型引用
    // 构造函数

    Entry(Object element) {
        this.element = element;
        this.next = null;
    }

    // 获取元素内容
    Object getElement() {
        return element;
    }
}


public class SinglyList1 {
    Entry header = new Entry(null); // 链表头
    // 在链表尾添加对象o

    Entry add(Object o) {
        Entry newEntry = new Entry(o); // 创建对象
        Entry e = header; // e 引用 header
        while (e.next != null) { // 循环直到 e.next 为 null
            e = e.next; // e 引用 e.next
        }
        e.next = newEntry; // 若 e.next 为 null，则在链表尾添加 newEntry
        return newEntry; // 返回添加的 newEntry
    }

    // 删除链表尾部的元素
    Entry remove() {
        Entry e = header; // e 引用 header
        if (e.next == null) { // 若链表为空，则返回null
            return null;
        }
        while (e.next.next != null) { // 循环直到 e.next.next 为 null
            e = e.next; // e 引用 e.next
        }
        Entry temp = e.next; // 若 e.next.next 为 null，则 e.next 是最后一个元素 | 保存最后的元素
        e.next = null; // 删除最后的元素
        return temp; // 返回 temp，即最后一个元素
    }

    public static void main(String[] args) {
        SinglyList1 list = new SinglyList1();
        list.add("苹果");
        list.add("香蕉");
        list.add("柑桔");
        System.out.println(list.remove().getElement()); // 删除“柑桔”|输出删除的结点名
        System.out.println(list.remove().getElement()); // 删除“香蕉”|输出删除的结点名
        System.out.println(list.remove().getElement()); // 删除“苹果”|输出删除的结点名
    }
}
