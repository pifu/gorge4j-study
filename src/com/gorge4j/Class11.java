package com.gorge4j;

/**
 * @Title: Class11.java
 * @Description: [类与对象 -> this引用]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Class11 {
    void hi() {
        System.out.println("Hi~~~~");
        hello(); // 在一个成员方法中，可以调用另外一个成员方法，编译器在编译的时候会做如下修改：this.hello();
    }

    void hello() {
        System.out.println("Hello~~~~");
    }

    public static void main(String[] args) {
        Class11 ob = new Class11();
        ob.hi();
    }
}
