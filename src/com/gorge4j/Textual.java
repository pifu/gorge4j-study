package com.gorge4j;

/**
 * @Title: Textual.java
 * @Description: [Java语言的基本语法 -> 数据类型 -> 基本数据类型 -> 字符型]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 13:44:17
 * @version v1.0
 */

public class Textual {
    public static void main(String[] args) {
        char a, b, c;
        a = 'A';
        b = '中';
        c = ' ';
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
    }
}
