package com.gorge4j;

/**
 * @Title: Alone8_1.java
 * @Description: [继承 -> this与super] | 在父类构造函数被重载的情形下，使用super(⋯)来调用父类的构造函数
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 00:28:26
 * @version v1.0
 */

class Parent_8_1 {
    private int a;

    Parent_8_1() {}

    Parent_8_1(int a) {
        this.a = a;
    }

    int getA() {
        return a;
    }
}


public class Alone8_1 extends Parent_8_1 {
    private int b;

    public Alone8_1() {}

    public Alone8_1(int a, int b) {
        super(a); // 位于最上部。在定义构造函数时，应将调用父类构造函数的代码 super(...) 置于最上部，因为必须首先初始化继承自父类的成员
        this.b = b;
    }

    public int getB() {
        return b;
    }

    public static void main(String[] args) {
        Alone8_1 ob = new Alone8_1(3, 5);
        System.out.println(ob.getA());
        System.out.println(ob.getB());
    }
}
