package com.gorge4j;

/**
 * @Title: Class5.java
 * @Description: [类与对象 -> 成员方法 -> void返回类型]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Class5 {
    void printPlusInt(int x) {
        if (x <= 0) {
            return; // void 方法，返回的意义在于返回点后面的逻辑不用继续执行了
        }
        System.out.println("自然数= " + x);
    }

    public static void main(String[] args) {
        Class5 aaa = new Class5();
        aaa.printPlusInt(10);
        aaa.printPlusInt(-10);
    }
}
