package com.gorge4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Vector;

/**
 * @Title: Alone15_1.java
 * @Description: [常用API之二 -> Collection接口 -> List接口]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 21:54:18
 * @version v1.0
 */

public class Alone15_1 {

    // 输出Vector v中的所有元素
    static void displayVector(Vector<Object> v) {
        System.out.println("\n----------目录----------");

        for (int i = 0; i < v.size(); i++) {
            System.out.print(v.elementAt(i) + " ");
        }

        System.out.println("\n------------------------\n");

    }

    public static void main(String[] args) throws IOException {
        Vector<Object> v = new Vector<Object>();
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        String select; // 菜单选择变量

        while (true) { // 死循环
            System.out.println("<<  1.添加 2.插入 3.删除 4.退出  >>");
            System.out.println("请选择。。。");
            select = br.readLine(); // 读取用户的选择
            if (select.equals("1")) { // 若选中菜单项1
                System.out.println("请输入追加的字符串：");
                v.add(br.readLine()); // 读取键盘输入的字符串，并添加至v中
                displayVector(v);
            } else if (select.equals("2")) { // 若选中菜单项2
                System.out.println("请输入插入的位置：");
                BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));
                v.add(Integer.parseInt(br1.readLine()), br.readLine());
                displayVector(v);
            } else if (select.equals("3")) { // 若选中菜单项3
                System.out.println("请输入元素编号（从0开始）：");
                v.removeElementAt(Integer.parseInt(br.readLine())); // 删除指定编号的元素
                displayVector(v);
            } else if (select.equals("4"))
                break; // 若选中了菜单项4，则退出while循环
        }

    }

}
