package com.gorge4j;

/**
 * @Title: String6.java
 * @Description: [常用API之一 -> java.lang.StringBuffer] | StringBuffer 类的常用方法使用
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 23:51:46
 * @version v1.0
 */

public class String6 {
    public static void main(String[] args) {
        StringBuffer sb = new StringBuffer();
        sb.append("ABC");
        sb.append(123);
        sb.append(true).append(false);
        System.out.println(sb);

        sb.delete(1, 3);
        System.out.println(sb);


        sb.deleteCharAt(4);
        System.out.println(sb);

        sb.insert(5, "@@");
        System.out.println(sb);

        sb.insert(6, 7.89);
        System.out.println(sb);
    }
}
