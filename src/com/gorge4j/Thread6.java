package com.gorge4j;

/**
 * @Title: Thread6.java
 * @Description: [线程 -> 线程的优先级] | 线程优先级设置
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Thread6 extends Thread {
    public void run() {
        for (int i = 1; i <= 5; i++)
            System.out.println(getName() + "：" + i);
    }

    public static void main(String[] args) {
        Thread6 t1 = new Thread6();
        Thread6 t2 = new Thread6();
        t1.setPriority(Thread.MIN_PRIORITY);
        t2.setPriority(Thread.MAX_PRIORITY);
        t1.start();
        t2.start();
    }
}
