package com.gorge4j;

/**
 * @Title: Class22.java
 * @Description: [类与对象 -> 重载]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Class22 {
    void hi() {
        System.out.println("您好~~~");
    }

    void hi(String name) {
        System.out.println(name + "先生，您好~~");
    }

    void hi(String name1, String name2) {
        System.out.println(name1 + "先生 ," + name2 + "小姐 您好~~");
    }

    public static void main(String[] args) {
        Class22 ob = new Class22();
        ob.hi();
        ob.hi("天下");
        ob.hi("天下", "风云");
    }
}
