package com.gorge4j;

/**
 * @Title: Class3.java
 * @Description: [类与对象 -> 成员方法]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Class3 {
    double f(int x) {
        return 2.5 + x;
    }

    public static void main(String[] args) {
        Class3 aaa = new Class3();
        double y = aaa.f(3);
        System.out.println(y);
        System.out.println(aaa.f(2));
    }
}
