package com.gorge4j;

/**
 * @Title: Thread1.java
 * @Description: [Java输入与输出 -> 流 -> 对象系列化 -> ObjectOutputStream] | 创建线程
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

class MyRunnable implements Runnable {
    String name;

    MyRunnable(String name) {
        this.name = name;
    }

    public void run() {
        for (int i = 1; i <= 5; i++)
            System.out.println(name + ":" + i);
    }
}


public class Thread1 {
    public static void main(String[] args) {
        MyRunnable myr = new MyRunnable("myrunable");
        Thread t = new Thread(myr);
        t.start();
    }
}
