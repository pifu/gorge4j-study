package com.gorge4j;

/**
 * @Title: Interface2.java
 * @Description: [抽象类与接口 -> 接口] | 一次实现多个接口
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 12:55:54
 * @version v1.0
 */

interface MyInter1 { // 接口
    public void method1();
}


interface MyInter2 { // 接口
    public void method2();
}


public class Interface2 implements MyInter1, MyInter2 {
    public void method1() { // 覆盖 MyInter1 的方法
        System.out.println("method1 override");
    }

    public void method2() { // 覆盖 MyInter2 的方法
        System.out.println("method2 override");
    }

    public static void main(String[] args) {
        Interface2 ob = new Interface2();
        ob.method1();
        ob.method2();
    }
}
