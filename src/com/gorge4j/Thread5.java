package com.gorge4j;

/**
 * @Title: Thread5.java
 * @Description: [线程 -> 多线程] | 多线程
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Thread5 {
    public static void main(String[] args) {
        SumThread st1 = new SumThread(1, 500);
        SumThread st2 = new SumThread(501, 1000);
        st1.start();
        st2.start();
        try {
            st1.join();
            st2.join();
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
        long sum = st1.getSum() + st2.getSum();
        System.out.println("1~1000的和：" + sum);
    }
}
