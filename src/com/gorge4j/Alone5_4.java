package com.gorge4j;

/**
 * @Title: Alone5_4.java
 * @Description: [类与对象 -> 成员方法 -> 多参方法] | 需求：编写两个方法 sum() 与 average() 分别求三个整数的和与平均数，并在 average()
 *               方法中实现对 sum() 方法的调用
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 20:05:00
 * @version v1.0
 */

public class Alone5_4 {
    public static void main(String[] args) {
        Alone5_4 ob = new Alone5_4();
        System.out.println(ob.average(60, 87, 90));
    }

    int sum(int x, int y, int z) {
        return x + y + z;
    }

    double average(int x, int y, int z) {
        return sum(x, y, z) / 3.0; // 在方法内调用另外一个方法 sum()
    }
}
