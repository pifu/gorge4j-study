package com.gorge4j;

/**
 * @Title: Alone6_1.java
 * @Description: [数组 -> 基本数据类型数组] | 需求：创建一个拥有 10 个元素的整型数组 a，并通过 a[i] = i * i 为每个数组元素赋值，最后将结果输出
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 21:01:00
 * @version v1.0
 */

public class Alone6_1 {
    public static void main(String[] args) {
        int[] a = new int[10];
        for (int i = 0; i <= 9; i++) {
            a[i] = i * i;
            System.out.println("a[" + i + "] = " + a[i]);
        }
    }
}
