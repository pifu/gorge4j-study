package com.gorge4j;

/**
 * @Title: For2.java
 * @Description: [程序流程控制语句 -> for语句]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class For2 {
    public static void main(String[] args) {
        int sum = 0;
        for (int a = 1; a <= 10; a++) {
            sum += a;
        }
        System.out.println("1 到 10 之间的整数和是：" + sum);
    }
}
