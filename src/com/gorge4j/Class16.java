package com.gorge4j;

/**
 * @Title: Class16.java
 * @Description: [类与对象 -> 包与导入] | 注意：本示例没有 main 方法，所以无法单独启动，只能通过被其它的类引用的方式来使用
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Class16 {
    public void hi() {
        System.out.println("Hi~~~~");
    }
}
