package com.gorge4j;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * @Title: Alone12_4.java
 * @Description: [常用API之一 -> java.lang.Math] | 需求：将部分函数转换为 Java 方法
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 13:05:17
 * @version v1.0
 */

class Alone12_4 {
    public double fx(double x) {
        double d = Math.cos(Math.toRadians(x)) * Math.sin(Math.toRadians(x)) * Math.pow(x, 2);
        return d;
    }

    public double gx(double x) {
        double d = Math.log(Math.pow(x, 10)) * Math.cos(Math.toRadians(Math.sin(Math.toRadians(x))));
        return d;
    }

    public double hx(double x) {
        double d = Math.pow(x, 10) + Math.pow(x, 6) + (3 * Math.pow(x, 3)) + Math.pow(Math.E, 5);
        return d;
    }

    public static void main(String[] args) throws Exception {
        System.out.print("请输入变量值 :");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        double x = Double.parseDouble(br.readLine());
        Alone12_4 al = new Alone12_4();
        System.out.println(al.fx(x));
        System.out.println(al.gx(x));
        System.out.println(al.hx(x));
    }
}
