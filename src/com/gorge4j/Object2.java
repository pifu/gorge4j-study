package com.gorge4j;

/**
 * @Title: Object2.java
 * @Description: [对象与方法 -> 对象的创建与销毁] ｜ 注意：此例为错误的演示示例，编译无法通过
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Object2 {
    Object2() {
        System.out.println("对象创建");
    }

    void hi() {
        System.out.println("您好~~~");
    }

    public static void main(String[] args) {
        Object2 ob; // 声明了引用变量 ob，但是没有引用任何对象，属于方法内的局部变量，未初始化不能编译通过
        ob.hi();
    }
}
