package com.gorge4j;

/**
 * @Title: Operator10.java
 * @Description: [Java语言的基本语法 -> 运算符 -> 位运算符：位与、位或、位异或]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 17:12:14
 * @version v1.0
 */

public class Operator10 {
    public static void main(String[] args) {
        int a = 14;
        int b = 12;
        System.out.println(a & b);
    }
}
