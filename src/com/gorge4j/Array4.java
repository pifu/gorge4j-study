package com.gorge4j;

/**
 * @Title: Array4.java
 * @Description: [数组 -> 对象数组]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Array4 {
    public static void main(String[] args) {
        String[] s = new String[3];
        s[0] = new String("您好");
        s[1] = new String("hello");
        s[2] = new String("高兴");
        for (int i = 0; i < s.length; i++) {
            System.out.println(s[i]);
        }
    }
}
