package com.gorge4j;

import java.io.*;
import com.gorge4j.util.FileUtil;

/**
 * @Title: File12.java
 * @Description: [Java输入与输出 -> 流 -> 字符流 -> BufferedReader与BufferedWriter] | BufferedReader 与 BufferedWriter 使用
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class File12 {
    public static void main(String[] args) throws Exception {
        String filePath = FileUtil.generateFilePathByFileName("char.txt"); // 文件路径
        FileOutputStream fos = new FileOutputStream(filePath);
        OutputStreamWriter osw = new OutputStreamWriter(fos);
        BufferedWriter bw = new BufferedWriter(osw);
        bw.write("您好"); // 将字符串写入输出流中
        bw.newLine(); // 向输出流写入一个行结束标记
        bw.write("很高兴^^");
        bw.newLine();
        bw.close();
        FileInputStream fis = new FileInputStream(filePath);
        InputStreamReader isw = new InputStreamReader(fis);
        BufferedReader br = new BufferedReader(isw);
        System.out.println(br.readLine()); // 读取一行
        System.out.println(br.readLine());
        br.close();
    }
}
