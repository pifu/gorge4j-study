package com.gorge4j;

/**
 * @Title: Member1.java
 * @Description: [内部类 -> 成员类] | 成员类
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

class Outer1 {
    static int a = 10;
    int b = 20;

    void f() {
        System.out.println("hi~~");
    }

    class Inner {
        int c = 30;

        public void g() {
            b = 100;
            f();
            System.out.println(a + " " + c);
        }
    }
}


public class Member1 {
    public static void main(String[] args) {
        Outer1 out = new Outer1();
        Outer1.Inner in = out.new Inner();
        in.g();
        System.out.println(out.b);
    }
}
