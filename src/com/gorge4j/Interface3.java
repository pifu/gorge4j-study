package com.gorge4j;

/**
 * @Title: Interface3.java
 * @Description: [抽象类与接口 -> 接口] | 集成其它类的同时实现接口
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 12:55:54
 * @version v1.0
 */

interface MyInter3_1 {
    public void method1();
}


interface MyInter3_2 {
    public void method2();
}


class MyClass {
    public void hi() {
        System.out.println("你好");
    }
}


public class Interface3 extends MyClass implements MyInter3_1, MyInter3_2 {
    public void method1() { // 实现
        System.out.println("method1 override");
    }

    public void method2() { // 实现
        System.out.println("method2 override");
    }

    public static void main(String[] args) {
        Interface3 ob = new Interface3();
        ob.method1();
        ob.method2();
        ob.hi();
    }
}

