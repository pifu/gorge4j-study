package com.gorge4j;

/**
 * @Title: For3.java
 * @Description: [程序流程控制语句 -> for语句]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class For3 {
    public static void main(String[] args) {
        int sum = 0;
        for (int a = 2; a <= 100; a += 2) {
            sum += a;
        }
        System.out.println("2 到 100 之间的偶数和是：" + sum);
    }
}
