package com.gorge4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @Title: Alone13_4.java
 * @Description: [Java输入与输出 -> 流 -> 字符流 -> 从System.in获取数据] | 需求：编写程序，从键盘读取 3 个整数，并求出它们的和与平均数
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 19:55:13
 * @version v1.0
 */

public class Alone13_4 {
    public static void main(String[] args) throws NumberFormatException, IOException {
        InputStreamReader isReader = new InputStreamReader(System.in);
        BufferedReader brBufferedReader = new BufferedReader(isReader);
        System.out.println("请输入第一个整数(回车结束输入)：");
        int a = Integer.parseInt(brBufferedReader.readLine());
        System.out.println("请输入第二个整数(回车结束输入)：");
        int b = Integer.parseInt(brBufferedReader.readLine());
        System.out.println("请输入第三个整数(回车结束输入)：");
        int c = Integer.parseInt(brBufferedReader.readLine());
        int sum = a + b + c;
        int average = sum / 3;
        System.out.println("结果");
        System.out.println("合计：" + sum);
        System.out.println("平均：" + average);
    }
}
