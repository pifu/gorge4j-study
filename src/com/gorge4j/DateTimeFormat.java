package com.gorge4j;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @Title: DateTimeFormat.java
 * @Description: JDK1.8+时间日期格式化示例
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-11-30 15:43:32
 * @version v1.0
 */

public class DateTimeFormat {
    public static void main(String[] args) {

        LocalDateTime localDateTime = LocalDateTime.now();
        
        // OffsetDateTime 和 ZonedDateTime 之间的差异在于后者包括涵盖夏令时调整的规则
        ZonedDateTime zoneDateTime = ZonedDateTime.now();
        
        ZoneOffset offset = ZoneOffset.of("+08:00");
        OffsetDateTime offsetDateTime = OffsetDateTime.of(localDateTime, offset);
        // 运行结果：2019-12-15T20:00:50.837
        System.out.println(localDateTime.toString());

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy'年'MM'月'dd'日'HH'时'mm'分'ss'秒'SSS'毫秒'");
        // 运行结果：2019年12月15日20时00分50秒837毫秒
        System.out.println(dtf.format(localDateTime));

        DateTimeFormatter dtf1 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
        // 运行结果：2019-12-15 20:00:50.837
        System.out.println(dtf1.format(localDateTime));

        DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        // 运行结果：2019-12-15 20:00:50
        System.out.println(dtf2.format(localDateTime));

        DateTimeFormatter dtf3 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        // 运行结果：2019-12-15
        System.out.println(dtf3.format(localDateTime));

        DateTimeFormatter dtf4 = DateTimeFormatter.ofPattern("yy-MM-dd");
        // 运行结果：19-12-15
        System.out.println(dtf4.format(localDateTime));

        DateTimeFormatter dtf5 = DateTimeFormatter.ofPattern("y-MM-dd");
        // 运行结果：2019-12-15
        System.out.println(dtf5.format(localDateTime));

        DateTimeFormatter dtf6 = DateTimeFormatter.ofPattern("G yyyy-MM-dd");
        // 运行结果：公元 2019-12-15
        System.out.println(dtf6.format(localDateTime));

        DateTimeFormatter dtf7 = DateTimeFormatter.ofPattern("uuuu-MM-dd");
        // 运行结果：2019-12-15
        System.out.println(dtf7.format(localDateTime));

        DateTimeFormatter dtf8 = DateTimeFormatter.ofPattern("uu-MM-dd");
        // 运行结果：19-12-15
        System.out.println(dtf8.format(localDateTime));

        DateTimeFormatter dtf9 = DateTimeFormatter.ofPattern("u-MM-dd");
        // 运行结果：2019-12-15
        System.out.println(dtf9.format(localDateTime));

        DateTimeFormatter dtf10 = DateTimeFormatter.ofPattern("yyyy-MM-dd D");
        // 运行结果：2019-12-15 349
        System.out.println(dtf10.format(localDateTime));

        DateTimeFormatter dtf11 = DateTimeFormatter.ofPattern("yyyy-MM-dd DDD");
        // D最多3个字符，运行结果：2019-12-15 349
        System.out.println(dtf11.format(localDateTime));

        DateTimeFormatter dtf12 = DateTimeFormatter.ofPattern("yyyy-MM-dd M");
        // 运行结果：2019-12-15 12
        System.out.println(dtf12.format(localDateTime));

        DateTimeFormatter dtf13 = DateTimeFormatter.ofPattern("yyyy-MM-dd MMM");
        // 运行结果：2019-12-15 十二月
        System.out.println(dtf13.format(localDateTime));

        DateTimeFormatter dtf14 = DateTimeFormatter.ofPattern("yyyy-MM-dd L");
        // 运行结果：2019-12-15 12
        System.out.println(dtf14.format(localDateTime));

        DateTimeFormatter dtf15 = DateTimeFormatter.ofPattern("yyyy-MM-dd LLL");
        // 运行结果：2019-12-15 十二月
        System.out.println(dtf15.format(localDateTime));

        DateTimeFormatter dtf16 = DateTimeFormatter.ofPattern("yyyy-MM-dd d");
        // 运行结果：2019-12-15 15
        System.out.println(dtf16.format(localDateTime));

        DateTimeFormatter dtf17 = DateTimeFormatter.ofPattern("yyyy-MM-dd dd");
        // 运行结果：2019-12-15 15
        System.out.println(dtf17.format(localDateTime));

        DateTimeFormatter dtf18 = DateTimeFormatter.ofPattern("yyyy-MM-dd q");
        // 运行结果：2019-12-15 4
        System.out.println(dtf18.format(localDateTime));

        DateTimeFormatter dtf19 = DateTimeFormatter.ofPattern("yyyy-MM-dd qq");
        // 运行结果：2019-12-15 04
        System.out.println(dtf19.format(localDateTime));

        DateTimeFormatter dtf20 = DateTimeFormatter.ofPattern("yyyy-MM-dd Q");
        // 运行结果：2019-12-15 4
        System.out.println(dtf20.format(localDateTime));

        DateTimeFormatter dtf21 = DateTimeFormatter.ofPattern("yyyy-MM-dd QQ");
        // 运行结果：2019-12-15 04
        System.out.println(dtf21.format(localDateTime));

        DateTimeFormatter dtf22 = DateTimeFormatter.ofPattern("YYYY-MM-dd");
        // 运行结果：2019-12-15
        System.out.println(dtf22.format(localDateTime));

        DateTimeFormatter dtf23 = DateTimeFormatter.ofPattern("YY-MM-dd");
        // 运行结果：19-12-15
        System.out.println(dtf23.format(localDateTime));

        DateTimeFormatter dtf24 = DateTimeFormatter.ofPattern("Y-MM-dd");
        // 运行结果：2019-12-15
        System.out.println(dtf24.format(localDateTime));

        DateTimeFormatter dtf25 = DateTimeFormatter.ofPattern("yyyy-MM-dd w");
        // 运行结果：2019-12-15 51
        System.out.println(dtf25.format(localDateTime));

        DateTimeFormatter dtf26 = DateTimeFormatter.ofPattern("yyyy-MM-dd W");
        // 运行结果：2019-12-15 3
        System.out.println(dtf26.format(localDateTime));

        DateTimeFormatter dtf27 = DateTimeFormatter.ofPattern("yyyy-MM-dd E");
        // 运行结果：2019-12-15 星期日
        System.out.println(dtf27.format(localDateTime));

        DateTimeFormatter dtf28 = DateTimeFormatter.ofPattern("yyyy-MM-dd EEE");
        // 运行结果：2019-12-15 星期日
        System.out.println(dtf28.format(localDateTime));

        DateTimeFormatter dtf29 = DateTimeFormatter.ofPattern("yyyy-MM-dd e");
        // 运行结果：2019-12-15 1
        System.out.println(dtf29.format(localDateTime));

        DateTimeFormatter dtf30 = DateTimeFormatter.ofPattern("yyyy-MM-dd c");
        // 运行结果：2019-12-15 1
        System.out.println(dtf30.format(localDateTime));

        DateTimeFormatter dtf31 = DateTimeFormatter.ofPattern("yyyy-MM-dd F");
        // 运行结果：2019-12-15 1
        System.out.println(dtf31.format(localDateTime));

        DateTimeFormatter dtf32 = DateTimeFormatter.ofPattern("yyyy-MM-dd a");
        // 运行结果：2019-12-15 下午
        System.out.println(dtf32.format(localDateTime));

        DateTimeFormatter dtf33 = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
        // 运行结果：2019-12-15 08:00:50 下午
        System.out.println(dtf33.format(localDateTime));

        DateTimeFormatter dtf34 = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
        // 运行结果：2019-12-15 08:00:50 下午
        System.out.println(dtf34.format(localDateTime));

        DateTimeFormatter dtf35 = DateTimeFormatter.ofPattern("yyyy-MM-dd kk:mm:ss");
        // 运行结果：2019-12-15 20:00:50
        System.out.println(dtf35.format(localDateTime));

        DateTimeFormatter dtf36 = DateTimeFormatter.ofPattern("yyyy-MM-dd KK:mm:ss a");
        // 运行结果：2019-12-15 08:00:50 下午
        System.out.println(dtf36.format(localDateTime));

        DateTimeFormatter dtf37 = DateTimeFormatter.ofPattern("y-M-d H:m:s.S");
        // 运行结果：2019-12-15 20:0:50.8
        System.out.println(dtf37.format(localDateTime));

        DateTimeFormatter dtf38 = DateTimeFormatter.ofPattern("yyyy-MM-dd A");
        // 运行结果：2019-12-15 72050837
        System.out.println(dtf38.format(localDateTime));

        DateTimeFormatter dtf39 = DateTimeFormatter.ofPattern("yyyy-MM-dd n");
        // 运行结果：2019-12-15 837000000
        System.out.println(dtf39.format(localDateTime));

        DateTimeFormatter dtf40 = DateTimeFormatter.ofPattern("yyyy-MM-dd N");
        // 运行结果：2019-12-15 72050837000000
        System.out.println(dtf40.format(localDateTime));

        DateTimeFormatter dtf41 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS VV");
        // 运行结果：2019-12-15 20:00:50.838 Asia/Shanghai
        System.out.println(dtf41.format(zoneDateTime));

        DateTimeFormatter dtf42 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS z");
        // 运行结果：2019-12-15 20:00:50.838 CST
        System.out.println(dtf42.format(zoneDateTime));

        DateTimeFormatter dtf43 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS Z");
        // 运行结果：2019-12-15 20:00:50.838 +0800
        System.out.println(dtf43.format(zoneDateTime));

        DateTimeFormatter dtf44 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS O");
        // 运行结果：2019-12-15 20:00:50.838 GMT+8
        System.out.println(dtf44.format(zoneDateTime));

        DateTimeFormatter dtf45 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS x");
        // 运行结果：2019-12-15 20:00:50.838 +08
        System.out.println(dtf45.format(zoneDateTime));

        DateTimeFormatter dtf46 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS X");
        // 运行结果：2019-12-15 20:00:50.838 +08
        System.out.println(dtf46.format(zoneDateTime));

        DateTimeFormatter dtf47 = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        // 运行结果：20191215200050
        System.out.println(dtf47.format(zoneDateTime));

        // 输出标准的 ISO-8601 时间格式
        DateTimeFormatter dtf48 = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssXXX");
        // 运行结果：2019-12-15T20:00:50+08:00
        System.out.println(offsetDateTime.format(dtf48));
        // 运行结果：2019-12-15T20:00:50+08:00
        System.out.println(zoneDateTime.format(dtf48));
        
        // 纪元
        DateTimeFormatter dtfG = DateTimeFormatter.ofPattern("G");
        // 运行结果：公元
        System.out.println(dtfG.format(localDateTime));
        DateTimeFormatter dtfGG = DateTimeFormatter.ofPattern("GG");
        // 运行结果：公元
        System.out.println(dtfGG.format(localDateTime));
        DateTimeFormatter dtfGGG = DateTimeFormatter.ofPattern("GGG");
        // 运行结果：公元
        System.out.println(dtfGGG.format(localDateTime));
        DateTimeFormatter dtfGGGG = DateTimeFormatter.ofPattern("GGGG");
        // 运行结果：公元
        System.out.println(dtfGGGG.format(localDateTime));
        DateTimeFormatter dtfGGGGG = DateTimeFormatter.ofPattern("GGGGG");
        // 运行结果：A
        System.out.println(dtfGGGGG.format(localDateTime));
        
        // 年份
        DateTimeFormatter dtfu = DateTimeFormatter.ofPattern("u");
        // 运行结果：2019
        System.out.println(dtfu.format(localDateTime));
        DateTimeFormatter dtfuu = DateTimeFormatter.ofPattern("uu");
        // 运行结果：19
        System.out.println(dtfuu.format(localDateTime));
        DateTimeFormatter dtfuuu = DateTimeFormatter.ofPattern("uuu");
        // 运行结果：2019
        System.out.println(dtfuuu.format(localDateTime));
        DateTimeFormatter dtfuuuu = DateTimeFormatter.ofPattern("uuuu");
        // 运行结果：2019
        System.out.println(dtfuuuu.format(localDateTime));
        // 最大长度 19 位，无值数字位默认填充 0
        DateTimeFormatter dtfumax = DateTimeFormatter.ofPattern("uuuuuuuuuuuuuuuuuuu");
        // 运行结果：0000000000000002019
        System.out.println(dtfumax.format(localDateTime));
        
        // 公元纪年年份
        DateTimeFormatter dtfy = DateTimeFormatter.ofPattern("y");
        // 运行结果：2019
        System.out.println(dtfy.format(localDateTime));
        DateTimeFormatter dtfyy = DateTimeFormatter.ofPattern("yy");
        // 运行结果：19
        System.out.println(dtfyy.format(localDateTime));
        DateTimeFormatter dtfyyy = DateTimeFormatter.ofPattern("yyy");
        // 运行结果：2019
        System.out.println(dtfyyy.format(localDateTime));
        DateTimeFormatter dtfyyyy = DateTimeFormatter.ofPattern("yyyy");
        // 运行结果：2019
        System.out.println(dtfyyyy.format(localDateTime));
        // 最大长度 19 位，无数字位默认填充 0
        DateTimeFormatter dtfymax = DateTimeFormatter.ofPattern("yyyyyyyyyyyyyyyyyyy");
        // 运行结果：0000000000000002019
        System.out.println(dtfymax.format(localDateTime));
        
        // 周所在年份
        DateTimeFormatter dtfY = DateTimeFormatter.ofPattern("Y");
        // 运行结果：2019
        System.out.println(dtfY.format(localDateTime));
        DateTimeFormatter dtfYY = DateTimeFormatter.ofPattern("YY");
        // 运行结果：19
        System.out.println(dtfYY.format(localDateTime));
        DateTimeFormatter dtfYYY = DateTimeFormatter.ofPattern("YYY");
        // 运行结果：2019
        System.out.println(dtfYYY.format(localDateTime));
        DateTimeFormatter dtfYYYY = DateTimeFormatter.ofPattern("YYYY");
        // 运行结果：2019
        System.out.println(dtfYYYY.format(localDateTime));
        // 最大长度 19 位，无数字位默认填充 0
        DateTimeFormatter dtfYmax = DateTimeFormatter.ofPattern("YYYYYYYYYYYYYYYYYYY");
        // 运行结果：0000000000000002019
        System.out.println(dtfYmax.format(localDateTime));
        
        // 季度
        DateTimeFormatter dtfQ = DateTimeFormatter.ofPattern("Q");
        // 运行结果：4
        System.out.println(dtfQ.format(localDateTime));
        DateTimeFormatter dtfQQ = DateTimeFormatter.ofPattern("QQ");
        // 运行结果：04
        System.out.println(dtfQQ.format(localDateTime));
        DateTimeFormatter dtfQQQ = DateTimeFormatter.ofPattern("QQQ");
        // 运行结果：4季
        System.out.println(dtfQQQ.format(localDateTime));
        DateTimeFormatter dtfQQQQ = DateTimeFormatter.ofPattern("QQQQ");
        // 运行结果：第4季度
        System.out.println(dtfQQQQ.format(localDateTime));
        DateTimeFormatter dtfQQQQQ = DateTimeFormatter.ofPattern("QQQQQ");
        // 运行结果：4
        System.out.println(dtfQQQQQ.format(localDateTime));
        
        // 季度
        DateTimeFormatter dtfq = DateTimeFormatter.ofPattern("q");
        // 运行结果：4
        System.out.println(dtfq.format(localDateTime));
        DateTimeFormatter dtfqq = DateTimeFormatter.ofPattern("qq");
        // 运行结果：04
        System.out.println(dtfqq.format(localDateTime));
        DateTimeFormatter dtfqqq = DateTimeFormatter.ofPattern("qqq");
        // 运行结果：4
        System.out.println(dtfqqq.format(localDateTime));
        DateTimeFormatter dtfqqqq = DateTimeFormatter.ofPattern("qqqq");
        // 运行结果：4
        System.out.println(dtfqqqq.format(localDateTime));
        DateTimeFormatter dtfqqqqq = DateTimeFormatter.ofPattern("qqqqq");
        // 运行结果：4
        System.out.println(dtfqqqqq.format(localDateTime));
        
        // 月份
        DateTimeFormatter dtfM = DateTimeFormatter.ofPattern("M");
        // 运行结果：12
        System.out.println(dtfM.format(localDateTime));
        DateTimeFormatter dtfMM = DateTimeFormatter.ofPattern("MM");
        // 运行结果：12
        System.out.println(dtfMM.format(localDateTime));
        DateTimeFormatter dtfMMM = DateTimeFormatter.ofPattern("MMM");
        // 运行结果：十二月
        System.out.println(dtfMMM.format(localDateTime));
        DateTimeFormatter dtfMMMM = DateTimeFormatter.ofPattern("MMMM");
        // 运行结果：十二月
        System.out.println(dtfMMMM.format(localDateTime));
        DateTimeFormatter dtfMMMMM = DateTimeFormatter.ofPattern("MMMMM");
        // 运行结果：12
        System.out.println(dtfMMMMM.format(localDateTime));
        
        // 月份
        DateTimeFormatter dtfL = DateTimeFormatter.ofPattern("L");
        // 运行结果：12
        System.out.println(dtfL.format(localDateTime));
        DateTimeFormatter dtfLL = DateTimeFormatter.ofPattern("LL");
        // 运行结果：12
        System.out.println(dtfLL.format(localDateTime));
        DateTimeFormatter dtfLLL = DateTimeFormatter.ofPattern("LLL");
        // 运行结果：十二月
        System.out.println(dtfLLL.format(localDateTime));
        DateTimeFormatter dtfLLLL = DateTimeFormatter.ofPattern("LLLL");
        // 运行结果：十二月
        System.out.println(dtfLLLL.format(localDateTime));
        DateTimeFormatter dtfLLLLL = DateTimeFormatter.ofPattern("LLLLL");
        // 运行结果：12月
        System.out.println(dtfLLLLL.format(localDateTime));
        
        // 当前周是本年度的第多少周，一位或两位数
        DateTimeFormatter dtfw = DateTimeFormatter.ofPattern("w");
        // 运行结果：51
        System.out.println(dtfw.format(localDateTime));
        
        // 当前周是本年度的第多少周，固定两位数，无值补 0
        DateTimeFormatter dtfww = DateTimeFormatter.ofPattern("ww");
        // 运行结果：51
        System.out.println(dtfww.format(localDateTime));
        
        // 当前周是本月的第多少周，星期日是每周的第一天
        DateTimeFormatter dtfW = DateTimeFormatter.ofPattern("W");
        // 运行结果：3
        System.out.println(dtfW.format(localDateTime));
        
        // 当前日期是本月的第几天，一位或两位数
        DateTimeFormatter dtfd = DateTimeFormatter.ofPattern("d");
        // 运行结果：15
        System.out.println(dtfd.format(localDateTime));
        
        // 当前日期是本月的第几天，固定两位数，无值补 0
        DateTimeFormatter dtfdd = DateTimeFormatter.ofPattern("dd");
        // 运行结果：15
        System.out.println(dtfdd.format(localDateTime));
        
        // 当前日期是本年度的第几天，一位数、两位数或三位数，日期序号为一位数时适用
        // DateTimeFormatter dtfD = DateTimeFormatter.ofPattern("D");
        // System.out.println(dtfD.format(localDateTime));
        
        // 当前日期是本年度的第几天，固定两位数，日期序号为一位数或两位数时适用，无值补 0，超过报错
        // DateTimeFormatter dtfDD = DateTimeFormatter.ofPattern("DD");
        // System.out.println(dtfDD.format(localDateTime));
        
        // 当前日期是本年度的第几天，固定三位数，日期序号为一位数、两位数或三位数时适用，无值补 0，超过报错
        DateTimeFormatter dtfDDD = DateTimeFormatter.ofPattern("DDD");
        // 运行结果：349
        System.out.println(dtfDDD.format(localDateTime));
        
        // 当前日期是本月本周的第几天
        DateTimeFormatter dtfF = DateTimeFormatter.ofPattern("F");
        // 运行结果：1
        System.out.println(dtfF.format(localDateTime));
        
        // 当前日期是星期几
        DateTimeFormatter dtfE = DateTimeFormatter.ofPattern("E");
        // 运行结果：星期日
        System.out.println(dtfE.format(localDateTime));
        DateTimeFormatter dtfEE = DateTimeFormatter.ofPattern("EE");
        // 运行结果：星期日
        System.out.println(dtfEE.format(localDateTime));
        DateTimeFormatter dtfEEE = DateTimeFormatter.ofPattern("EEE");
        // 运行结果：星期日
        System.out.println(dtfEEE.format(localDateTime));
        DateTimeFormatter dtfEEEE = DateTimeFormatter.ofPattern("EEEE");
        // 运行结果：星期日
        System.out.println(dtfEEEE.format(localDateTime));
        DateTimeFormatter dtfEEEEE = DateTimeFormatter.ofPattern("EEEEE");
        // 运行结果：日
        System.out.println(dtfEEEEE.format(localDateTime));
        
        // 当前日期是星期几
        DateTimeFormatter dtfe = DateTimeFormatter.ofPattern("e");
        // 运行结果：1
        System.out.println(dtfe.format(localDateTime));
        DateTimeFormatter dtfee = DateTimeFormatter.ofPattern("ee");
        // 运行结果：01
        System.out.println(dtfee.format(localDateTime));
        DateTimeFormatter dtfeee = DateTimeFormatter.ofPattern("eee");
        // 运行结果：星期日
        System.out.println(dtfeee.format(localDateTime));
        DateTimeFormatter dtfeeee = DateTimeFormatter.ofPattern("eeee");
        // 运行结果：星期日
        System.out.println(dtfeeee.format(localDateTime));
        DateTimeFormatter dtfeeeee = DateTimeFormatter.ofPattern("eeeee");
        // 运行结果：日
        System.out.println(dtfeeeee.format(localDateTime));
        
        // 当前日期是本周的第几天
        DateTimeFormatter dtfc = DateTimeFormatter.ofPattern("c");
        // 运行结果：1
        System.out.println(dtfc.format(localDateTime));
        // 当前日期是星期几
        DateTimeFormatter dtfccc = DateTimeFormatter.ofPattern("ccc");
        // 运行结果：星期日
        System.out.println(dtfccc.format(localDateTime));
        DateTimeFormatter dtfcccc = DateTimeFormatter.ofPattern("cccc");
        // 运行结果：星期日
        System.out.println(dtfcccc.format(localDateTime));
        DateTimeFormatter dtfccccc = DateTimeFormatter.ofPattern("ccccc");
        // 运行结果：日
        System.out.println(dtfccccc.format(localDateTime));
        
        // 一天的上午/下午
        DateTimeFormatter dtfa = DateTimeFormatter.ofPattern("a");
        // 运行结果：下午
        System.out.println(dtfa.format(localDateTime));
        
        // 12 小时记的小时数，1-12
        DateTimeFormatter dtfh = DateTimeFormatter.ofPattern("h");
        // 运行结果：8
        System.out.println(dtfh.format(localDateTime));
        // 12 小时记的小时数，1-12，固定2位，无值补 0
        DateTimeFormatter dtfhh = DateTimeFormatter.ofPattern("hh");
        // 运行结果：08
        System.out.println(dtfhh.format(localDateTime));
        
        // 24 小时记的小时数，0-23
        DateTimeFormatter dtfH = DateTimeFormatter.ofPattern("H");
        // 运行结果：20
        System.out.println(dtfH.format(localDateTime));
        // 24 小时记的小时数，0-23，固定2位，无值补 0
        DateTimeFormatter dtfHH = DateTimeFormatter.ofPattern("HH");
        // 运行结果：20
        System.out.println(dtfHH.format(localDateTime));
        
        // 24 小时记的小时数，1-24
        DateTimeFormatter dtfk = DateTimeFormatter.ofPattern("k");
        // 运行结果：20
        System.out.println(dtfk.format(localDateTime));
        // 24 小时记的小时数，1-24，固定2位，无值补 0
        DateTimeFormatter dtfkk = DateTimeFormatter.ofPattern("kk");
        // 运行结果：20
        System.out.println(dtfkk.format(localDateTime));
        
        // 12 小时记的小时数，0-11
        DateTimeFormatter dtfK = DateTimeFormatter.ofPattern("K");
        // 运行结果：8
        System.out.println(dtfK.format(localDateTime));
        // 12 小时记的小时数，0-11，固定2位，无值补 0
        DateTimeFormatter dtfKK = DateTimeFormatter.ofPattern("KK");
        // 运行结果：08
        System.out.println(dtfKK.format(localDateTime));
        
        // 分钟数
        DateTimeFormatter dtfm = DateTimeFormatter.ofPattern("m");
        // 运行结果：0
        System.out.println(dtfm.format(localDateTime));
        // 分钟数，固定 2 位，无值补 0
        DateTimeFormatter dtfmm = DateTimeFormatter.ofPattern("mm");
        // 运行结果：00
        System.out.println(dtfmm.format(localDateTime));
        
        // 秒数
        DateTimeFormatter dtfs = DateTimeFormatter.ofPattern("s");
        // 运行结果：50
        System.out.println(dtfs.format(localDateTime));
        // 秒数，固定 2 位，无值补 0
        DateTimeFormatter dtfss = DateTimeFormatter.ofPattern("ss");
        // 运行结果：50
        System.out.println(dtfss.format(localDateTime));
        
        // 豪秒数
        DateTimeFormatter dtfS = DateTimeFormatter.ofPattern("S");
        // 运行结果：8
        System.out.println(dtfS.format(localDateTime));
        // 毫秒数
        DateTimeFormatter dtfSSS = DateTimeFormatter.ofPattern("SSS");
        // 运行结果：837
        System.out.println(dtfSSS.format(localDateTime));
        // 纳秒数
        DateTimeFormatter dtfSmax = DateTimeFormatter.ofPattern("SSSSSSSSS");
        // 运行结果：837000000
        System.out.println(dtfSmax.format(localDateTime));
        
        // 此刻在当天的毫秒值
        DateTimeFormatter dtfA = DateTimeFormatter.ofPattern("A");
        // 运行结果：72050837
        System.out.println(dtfA.format(localDateTime));
        // 此刻在当天的毫秒值，最长 19 位，无值位补 0
        DateTimeFormatter dtfAmax = DateTimeFormatter.ofPattern("AAAAAAAAAAAAAAAAAAA");
        // 运行结果：0000000000072050837
        System.out.println(dtfAmax.format(localDateTime));
        
        // 一秒中的纳秒数
        DateTimeFormatter dtfn = DateTimeFormatter.ofPattern("n");
        // 运行结果：837000000
        System.out.println(dtfn.format(localDateTime));
        // 一秒中的纳秒数，最长 19 位，无值位补 0
        DateTimeFormatter dtfnmax = DateTimeFormatter.ofPattern("nnnnnnnnnnnnnnnnnnn");
        // 运行结果：0000000000837000000
        System.out.println(dtfnmax.format(localDateTime));
        
        // 一天中的纳秒数
        DateTimeFormatter dtfN = DateTimeFormatter.ofPattern("N");
        // 运行结果：72050837000000
        System.out.println(dtfN.format(localDateTime));
        // 一天中的纳秒数，最长 19 位，无值位补 0
        DateTimeFormatter dtfNmax = DateTimeFormatter.ofPattern("NNNNNNNNNNNNNNNNNNN");
        // 运行结果：0000072050837000000
        System.out.println(dtfNmax.format(localDateTime));
        
        // 时区名
        DateTimeFormatter dtfVV = DateTimeFormatter.ofPattern("VV");
        // 运行结果：Asia/Shanghai
        System.out.println(dtfVV.format(zoneDateTime));
        
        // 时区标准字母简称
        DateTimeFormatter dtfz = DateTimeFormatter.ofPattern("z");
        // 运行结果：CST
        System.out.println(dtfz.format(zoneDateTime));
        DateTimeFormatter dtfzz = DateTimeFormatter.ofPattern("zz");
        // 运行结果：CST
        System.out.println(dtfzz.format(zoneDateTime));
        DateTimeFormatter dtfzzz = DateTimeFormatter.ofPattern("zzz");
        // 运行结果：CST
        System.out.println(dtfzzz.format(zoneDateTime));
        DateTimeFormatter dtfzzzz = DateTimeFormatter.ofPattern("zzzz");
        // 时区标准中文全称，运行结果：中国标准时间
        System.out.println(dtfzzzz.format(zoneDateTime));
        
        // 本地时区偏移前缀
        DateTimeFormatter dtfO = DateTimeFormatter.ofPattern("O");
        // 运行结果：GMT+8
        System.out.println(dtfO.format(zoneDateTime));
        // 本地时区偏移前缀
        DateTimeFormatter dtfOOOO = DateTimeFormatter.ofPattern("OOOO");
        // 运行结果：GMT+08:00
        System.out.println(dtfOOOO.format(zoneDateTime));
        
        // 区域偏移量
        DateTimeFormatter dtfX = DateTimeFormatter.ofPattern("X");
        // 运行结果：+08
        System.out.println(dtfX.format(zoneDateTime));
        DateTimeFormatter dtfXX = DateTimeFormatter.ofPattern("XX");
        // 运行结果：+0800
        System.out.println(dtfXX.format(zoneDateTime));
        DateTimeFormatter dtfXXX = DateTimeFormatter.ofPattern("XXX");
        // 运行结果：+08:00
        System.out.println(dtfXXX.format(zoneDateTime));
        DateTimeFormatter dtfXXXX = DateTimeFormatter.ofPattern("XXXX");
        // 运行结果：+0800
        System.out.println(dtfXXXX.format(zoneDateTime));
        DateTimeFormatter dtfXXXXX = DateTimeFormatter.ofPattern("XXXXX");
        // 运行结果：+08:00
        System.out.println(dtfXXXXX.format(zoneDateTime));
        
        // 区域偏移量
        DateTimeFormatter dtfx = DateTimeFormatter.ofPattern("x");
        // 运行结果：+08
        System.out.println(dtfx.format(zoneDateTime));
        DateTimeFormatter dtfxx = DateTimeFormatter.ofPattern("xx");
        // 运行结果：+0800
        System.out.println(dtfxx.format(zoneDateTime));
        DateTimeFormatter dtfxxx = DateTimeFormatter.ofPattern("xxx");
        // 运行结果：+08:00
        System.out.println(dtfxxx.format(zoneDateTime));
        DateTimeFormatter dtfxxxx = DateTimeFormatter.ofPattern("xxxx");
        // 运行结果：+0800
        System.out.println(dtfxxxx.format(zoneDateTime));
        DateTimeFormatter dtfxxxxx = DateTimeFormatter.ofPattern("xxxxx");
        // 运行结果：+08:00
        System.out.println(dtfxxxxx.format(zoneDateTime));
        
        // 区域偏移量
        DateTimeFormatter dtfZ = DateTimeFormatter.ofPattern("Z");
        // 运行结果：+0800
        System.out.println(dtfZ.format(zoneDateTime));
        DateTimeFormatter dtfZZ = DateTimeFormatter.ofPattern("ZZ");
        // 运行结果：+0800
        System.out.println(dtfZZ.format(zoneDateTime));
        DateTimeFormatter dtfZZZ = DateTimeFormatter.ofPattern("ZZZ");
        // 运行结果：+0800
        System.out.println(dtfZZZ.format(zoneDateTime));
        DateTimeFormatter dtfZZZZ = DateTimeFormatter.ofPattern("ZZZZ");
        // 运行结果：GMT+08:00
        System.out.println(dtfZZZZ.format(zoneDateTime));
        DateTimeFormatter dtfZZZZZ = DateTimeFormatter.ofPattern("ZZZZZ");
        // 运行结果：+08:00
        System.out.println(dtfZZZZZ.format(zoneDateTime));

    }
}
