package com.gorge4j;

/**
 * @Title: Class2.java
 * @Description: [类与对象 -> 引用赋值]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

// 类 Class1 中已定义 Man 类，所以此处注释掉
// class Man {
// int height;
// int age;
// }

public class Class2 {
    public static void main(String[] ages) {
        Man man1 = new Man();
        Man man2;
        man2 = man1;
        man1.height = 180;
        man1.age = 20;
        System.out.println(man2.height + " " + man2.age);
    }
}
