package com.gorge4j;

/**
 * @Title: Excep5.java
 * @Description: [异常处理 -> Exception种类] | 异常捕获示例
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Excep5 {
    public static void main(String[] args) {
        try {
            int[] a = new int[-1]; // 发生 NegativeArraySizeException 异常
        } catch (Exception e) { // e 可捕获所有异常
            System.out.println(e);
        }
    }
}
