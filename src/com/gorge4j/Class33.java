package com.gorge4j;

/**
 * @Title: Class33.java
 * @Description: [类与对象 -> 实例块]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 10:51:47
 * @version v1.0
 */

public class Class33 {
    int a;
    // 示例块开始
    {
        System.out.println("实例块");
        a = 10;
    }
    // 示例块结束

    Class33(int a) {
        System.out.println("构造函数");
        this.a = a;
    }

    public static void main(String[] args) {
        Class33 ob = new Class33(30);
        System.out.println(ob.a);
    }
}
