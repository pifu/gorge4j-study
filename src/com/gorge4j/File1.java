package com.gorge4j;

import java.io.File;
import java.io.IOException;
import com.gorge4j.util.FileUtil;

/**
 * @Title: File1.java
 * @Description: [Java输入与输出 -> File类] | File 文件类的常用方法
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class File1 {
    public static void main(String[] args) {
        String filePath = FileUtil.generateFilePathByFileName("abc.txt"); // 文件路径

        File f = new File(filePath);

        if (f.exists()) { // 若文件存在
            System.out.println("文件已存在");
        } else { // 若文件不存在
            try {
                f.createNewFile(); // 创建文件
                System.out.println("文件创建完毕");
            } catch (IOException e) {
                System.out.println("创建文件出现异常，异常信息：" + e.getMessage());
            }
        }
    }
}
