package com.gorge4j;

import java.io.*;
import com.gorge4j.util.FileUtil;

/**
 * @Title: File10.java
 * @Description: [Java输入与输出 -> 流 -> FilterStream-DataInputStream和DataOutputStream] | DataInputStream
 *               使用
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class File10 {
    public static void main(String[] args) throws Exception {
        String filePath = FileUtil.generateFilePathByFileName("hjb.dat"); // 文件路径
        File f = new File(filePath);
        FileInputStream fis = new FileInputStream(f);
        DataInputStream dis = new DataInputStream(fis);
        String name, country;
        int age, height;
        // 读取时，按照存入的数据类型读出非常重要
        name = dis.readUTF();
        age = dis.readInt();
        height = dis.readInt();
        country = dis.readUTF();
        dis.close();
        System.out.println("名字：" + name);
        System.out.println("年龄：" + age);
        System.out.println("身高：" + height);
        System.out.println("国籍：" + country);
    }
}
