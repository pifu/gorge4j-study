package com.gorge4j;

/**
 * @Title: Operator9.java
 * @Description: [Java语言的基本语法 -> 运算符 -> 快速逻辑与和快速逻辑或]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Operator9 {
    public static void main(String[] args) {
        int a = 10;
        System.out.println(a > 0 && (a = 20) > 10);
        System.out.println(a);
        int b = 10;
        System.out.println((b < 10) && (b = 20) > 10);
        System.out.println(b);
    }
}
