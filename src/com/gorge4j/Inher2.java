package com.gorge4j;

/**
 * @Title: Inher2.java
 * @Description: [继承 -> 子类对象的创建]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 12:55:54
 * @version v1.0
 */

class Inher2_1 { // 父类
    int a;

    Inher2_1() { // 构造函数
        a = 10;
        System.out.println("调用父类构造函数");
    }
}


public class Inher2 extends Inher2_1 {
    int b;

    Inher2() { // 构造函数
        // super(); // 此行是隐含的
        b = 20;
        System.out.println("调用子类构造函数");
    }

    public static void main(String[] args) {
        Inher2 ob = new Inher2();
        System.out.println(ob.a + " " + ob.b);
    }
}
