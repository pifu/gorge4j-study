package com.gorge4j;

/**
 * @Title: Alone5_2.java
 * @Description: [类与对象 -> 成员方法 -> 多参方法] | 需求：编写一个方法并调用它，求取三个实数中的最大者
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 17:53:27
 * @version v1.0
 */

public class Alone5_2 {
    public static void main(String[] args) {
        Alone5_2 ob = new Alone5_2();
        double d = ob.max(10.2, 50.5, 30.2);
        System.out.println(d);
    }
    
    double max(double x, double y, double z) {
        // 可以嵌套两个三目运算符，但是不推荐，可读性太差
        double t;
        t = x > y ? x : y;
        return t > z ? t : z;
    }
    
}
