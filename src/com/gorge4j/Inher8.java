package com.gorge4j;

/**
 * @Title: Inher8.java
 * @Description: [继承 -> 多态]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 12:55:54
 * @version v1.0
 */

class Animal8 {
    String name;

    Animal8() {}

    Animal8(String name) {
        this.name = name;
    }

    void move() {
        System.out.println(name + "行走");
    }
}


class Elephant8 extends Animal8 {
    Elephant8() {}

    Elephant8(String name) {
        super(name);
    }

    void move() {
        System.out.println(name + "慢悠悠，一步步地行走");
    }
}


class Mouse8 extends Animal8 {
    Mouse8() {}

    Mouse8(String name) {
        super(name);
    }

    void move() {
        System.out.println(name + "静悄悄地，蹑手蹑脚地行走");
    }
}


public class Inher8 {
    public static void main(String[] args) {
        Elephant8 a = new Elephant8("Jacky");
        Mouse8 b = new Mouse8("Jerry");
        a.move();
        b.move();
    }
}
