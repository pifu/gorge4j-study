package com.gorge4j;

/**
 * @Title: Inher12.java
 * @Description: [继承 -> Object型引用变量] | Object 型引变量
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 12:55:54
 * @version v1.0
 */

public class Inher12 {
    public static void main(String[] args) {
        Object[] ob = new Object[3];
        ob[0] = new String("abc");
        ob[1] = new Boolean(true);
        ob[2] = new Integer(5);
        for (int i = 0; i < ob.length; i++) {
            System.out.println(ob[i]);
        }
    }
}
