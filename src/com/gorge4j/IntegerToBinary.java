package com.gorge4j;

/**
 * @Title: IntegerToBinary.java
 * @Description: [Java语言的基本语法 -> 数据类型 -> 基本数据类型 -> 整型] | 非标准示例，整型转二进制
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 13:22:58
 * @version v1.0
 */

public class IntegerToBinary {
    public static void main(String[] args) {
        System.out.println(Integer.toBinaryString(10));
    }
}
