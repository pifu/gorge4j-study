package com.gorge4j;

/**
 * @Title: Anony1.java
 * @Description: [内部类 -> 匿名类] | 匿名类定义
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

interface Inter {
    public void hi();
}


public class Anony1 {
    public static void main(String[] args) {
        Inter ob1 = new Inter() {
            public void hi() {
                System.out.println("您好~~~");
            }
        };
        Inter ob2 = new Inter() {
            public void hi() {
                System.out.println("hello~~~");
            }
        };
        ob1.hi();
        ob2.hi();
    }
}
