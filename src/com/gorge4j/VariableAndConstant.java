package com.gorge4j;

/**
 * @Title: VariableAndConstant.java
 * @Description: [Java语言的基本语法 -> 变量和常量]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 21:30:51
 * @version v1.0
 */

public class VariableAndConstant {

    public static void main(String[] args) {
        
        int dayOfYear = 365; // 这里 dayOfYear 是变量，365 是常量，此行语句的作用是将常量 365 赋值给变量 dayOfYear
        boolean isMondayOfWeek = false; // 这里的 isMondayOfWeek 是变量， false 是常量，此行语句的作用是将常量 false 赋值给变量 boo
        
        System.out.println(dayOfYear);
        System.out.println(isMondayOfWeek);
        
    }
    
}
