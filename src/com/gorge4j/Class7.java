package com.gorge4j;

/**
 * @Title: Class7.java
 * @Description: [类与对象 -> 成员方法 -> 多参方法]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Class7 {
    int sum(int a, int b) {
        return a + b;
    }

    int average(int a, int b, int c) {
        return (a + b + c) / 3;
    }

    public static void main(String[] args) {
        Class7 class7 = new Class7();
        int sum = class7.sum(10, 20);
        int aver = class7.average(10, 20, 30);
        System.out.println("合计= " + sum);
        System.out.println("平均= " + aver);
    }
}
