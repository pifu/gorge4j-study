package com.gorge4j;

import java.util.*;

/**
 * @Title: HashSet2.java
 * @Description: [常用API之二 -> Collection接口 -> Map接口] | 实现 map 接口的 Hashtable 类示例
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 12:55:54
 * @version v1.0
 */

public class Hashtable1 {
    public static void main(String[] args) {
        Hashtable<String, String> ht = new Hashtable<String, String>();

        // 添加指定的 key 与元素
        ht.put("0011-12345", "张三");
        ht.put("0022-23456", "李四");
        ht.put("0033-34567", "王五");
        ht.put("0044-45678", "赵六");

        if (ht.containsKey("0022-23456")) { // 若拥有指定 key 的元素存在
            ht.remove("0022-23456"); // 删除拥有指定 key 的元素
        }

        System.out.println("元素个数：" + ht.size()); // 输出元素的个数

        System.out.println("get测试" + ht.get("0033-34567"));
        System.out.println("取不存在的" + ht.get("123213213"));

        // 输出所有元素
        System.out.println("<目录>\n---------");

        // 以下要好好理解下
        for (Enumeration<String> e = ht.elements(); e.hasMoreElements();) { // hasMoreElements()是Enumeration接口的方法
            System.out.println(e.nextElement()); // nextElement() 是 Enumeration 接口的方法
        }

    }
}
