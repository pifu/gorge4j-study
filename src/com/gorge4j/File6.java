package com.gorge4j;

import java.io.*;
import com.gorge4j.util.FileUtil;

/**
 * @Title: File6.java
 * @Description: [Java输入与输出 -> 流 -> FileInputStream和FileOutputStream] | FileInputStream 常用构造函数
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class File6 {
    public static void main(String[] args) throws Exception {
        String filePath = FileUtil.generateFilePathByFileName("alphabet.txt"); // 文件路径
        File f = new File(filePath);
        FileInputStream fis = new FileInputStream(f);
        char a;
        for (int i = 0; i < f.length(); i++) { // 读取文件所有数据
            // read() 方法每次读取一个字节，并将其转换为 int 型，故需使用强制类型转换，将 int 型数据转换为 char
            a = (char) fis.read();
            System.out.print(a);
        }
        fis.close(); // 关闭文件输出流
    }
}
