package com.gorge4j;

import java.io.File;
import com.gorge4j.util.FileUtil;

/**
 * @Title: Alone13_1.java
 * @Description: [Java输入与输出 -> File类] | 查看特定路径下的目录及文件的属性
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 13:36:50
 * @version v1.0
 */

public class Alone13_1 {
    public static void main(String[] args) {
        String filePath = FileUtil.generateBaseFilePath(); // 以生成的基础路径为示例来说明
        File file = new File(filePath);
        // 也可以自行指明路径，上面两行可以换成下面的示例：
        // File file = new File("D:\\jsduty"); // Windows 环境下
        // File file = new File("/Users/username/jstudy"); // macOS 环境下，注意 username 替换成自己的用户名
        File[] files = file.listFiles();
        for (File file2 : files) {
            if (file2.isDirectory()) {
                System.out.println("目录：" + file2 + " 隐藏性：" + file2.isHidden());
            } else {
                System.out.println("文件：" + file2 + " 大小：" + file2.length() + " 隐藏性：" + file2.isHidden() + " 日期："
                        + file2.lastModified());
            }
        }
    }
}
