package com.gorge4j;

/**
 * @Title: Synchronization4.java
 * @Description: [线程 -> 线程同步 -> 同步块] | 同步块
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 23:51:46
 * @version v1.0
 */

class PrintThread3 extends Thread {
    char ch;
    static Object printer = new Object();

    public PrintThread3(char ch) {
        this.ch = ch;
    }

    void printCh10() {
        synchronized (printer) {
            for (int i = 1; i <= 10; i++)
                System.out.print(ch);
        }
    }

    public void run() {
        for (int i = 1; i <= 3; i++) {
            printCh10();
            System.out.println();
        }
    }
}


public class Synchronization4 {
    public static void main(String[] args) {
        PrintThread3 pt1 = new PrintThread3('A');
        PrintThread3 pt2 = new PrintThread3('B');
        PrintThread3 pt3 = new PrintThread3('C');
        pt1.start();
        pt2.start();
        pt3.start();
    }
}

/*
 * ���н���� AAAAAAAAAA CCCCCCCCCC BBBBBBBBBB CCCCCCCCCC AAAAAAAAAA CCCCCCCCCC BBBBBBBBBB AAAAAAAAAA
 * BBBBBBBBBB
 */
