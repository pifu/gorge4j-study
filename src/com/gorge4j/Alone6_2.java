package com.gorge4j;

/**
 * @Title: Alone6_2.java
 * @Description: [数组 -> 对象数组] | 需求：创建一个含有 10 个元素的 Alone6_2 的数组 ob，使其满足下列条件（ob[0].a = 1, ob[0].b = 2 且
 *               ob[i + 1].a = ob[i].a + ob[i].b 且 ob[i + 1].b = ob[i].a * ob[i].b），并按照注释的运行结果来输出
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-08 21:09:07
 * @version v1.0
 */

public class Alone6_2 {
    private int a, b;

    public Alone6_2() {

    }

    public void setAB(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    public static void main(String[] args) {
        Alone6_2[] ob = new Alone6_2[10];
        ob[0] = new Alone6_2();
        ob[0].setAB(1, 2);
        System.out.print("ob[0].a = " + ob[0].a + ", ");
        System.out.println("ob[0].b = " + ob[0].b);
        for (int i = 0; i < ob.length - 1; i++) {
            ob[i + 1] = new Alone6_2();
            ob[i + 1].a = ob[i].a + ob[i].b;
            ob[i + 1].b = ob[i].a * ob[i].b;
            ob[i + 1].setAB(ob[i + 1].a, ob[i + 1].b);
            System.out.print("ob[" + (i + 1) + "].a = " + ob[i + 1].a + ", ");
            System.out.println("ob[" + (i + 1) + "].b = " + ob[i + 1].b);
        }
    }
}

// 运行结果：
// ob[0].a = 1, ob[0].b = 2
// ob[1].a = 3, ob[1].b = 2
// ob[2].a = 5, ob[2].b = 6
// ob[3].a = 11, ob[3].b = 30
// ob[4].a = 41, ob[4].b = 330
// ob[5].a = 371, ob[5].b = 13530
// ob[6].a = 13901, ob[6].b = 5019630
// ob[7].a = 5033531, ob[7].b = 1058399894
// ob[8].a = 1063433425, ob[8].b = -1642014574
// ob[9].a = -578581149, ob[9].b = 117143346
