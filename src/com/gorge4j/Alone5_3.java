package com.gorge4j;

/**
 * @Title: Alone5_3.java
 * @Description: [类与对象 -> 成员方法 -> 多参方法] | 需求：设计一个方法，接收整数 n，然后根据 n 的大小，输出五角星组成的三角形图案（具体效果见运行结果注释）
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 19:51:36
 * @version v1.0
 */

public class Alone5_3 {
    public static void main(String[] args) {
        Alone5_3 ob = new Alone5_3();
        ob.star(7);
    }
    
    public void star(int n) {
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print('★');
            }
            System.out.println(); // 每打印完一行输出一个回车
        }
    }
}

// 运行结果：
// ★
// ★★
// ★★★
// ★★★★
// ★★★★★
// ★★★★★★
// ★★★★★★★