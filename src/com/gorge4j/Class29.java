package com.gorge4j;

/**
 * @Title: Class29.java
 * @Description: [类与对象 -> 构造函数] | 注意：此例是错误示例，无法编译通过
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 09:59:51
 * @version v1.0
 */

public class Class29 {
    int a;

    public Class29(int a) {
        this.a = a;
    }

    public static void main(String[] args) {
        Class29 ob = new Class29();
        System.out.println(ob.a);
    }
}
