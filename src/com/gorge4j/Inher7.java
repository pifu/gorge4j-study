package com.gorge4j;

/**
 * @Title: Inher7.java
 * @Description: [继承 -> 覆盖] | 重载父类成员方法
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 12:55:54
 * @version v1.0
 */

class Inher7_1 {
    void hello() {
        System.out.println("Hi~~~");
    }
}


public class Inher7 extends Inher7_1 {
    void hello(String a) {
        System.out.println(a);
    }

    public static void main(String[] args) {
        Inher7 ob = new Inher7();
        ob.hello();
        ob.hello("高兴");
    }
}

/*
 * ���н���� Hi~~~ ����
 */
