package com.gorge4j;

/**
 * @Title: If3.java
 * @Description: [程序流程控制语句 -> if语句格式3、4]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class If3 {
    public static void main(String[] args) {
        int average = 40;
        String judgement;
        if (average >= 90) {
            judgement = "A";
        } else if (average >= 80) {
            judgement = "B";
        } else if (average >= 70) {
            judgement = "C";
        } else if (average >= 60) {
            judgement = "D";
        } else {
            judgement = "E";
        }
        System.out.println("平均=" + average);
        System.out.println("评价=" + judgement);
    }
}
