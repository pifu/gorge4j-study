package com.gorge4j;

import java.math.*;

/**
 * @Title: BigDecimal1.java
 * @Description: [常用API之一 -> java.lang.Math.BigDecimal] | BigDecimal 常用方法
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class BigDecimal1 {
    public static void main(String[] args) {
        BigDecimal a = new BigDecimal("123456789.123456789");

        BigDecimal b = a.movePointLeft(3);
        System.out.println(b);

        BigDecimal c = a.divide(b, BigDecimal.ROUND_DOWN);
        System.out.println(c);

        c = a.divide(b, 5, BigDecimal.ROUND_DOWN);
        System.out.println(c);

        System.out.println(a.toBigInteger());
    }
}
