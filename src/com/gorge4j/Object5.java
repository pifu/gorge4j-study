package com.gorge4j;

/**
 * @Title: Object5.java
 * @Description: [对象与方法 -> 值传递调用]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Object5 {
    static void method(int a) {
        a = 20;
    }

    public static void main(String[] args) {
        int x = 10;
        method(x);
        System.out.println(x);
    }
}
