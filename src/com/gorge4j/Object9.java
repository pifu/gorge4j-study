package com.gorge4j;

/**
 * @Title: Object9.java
 * @Description: [对象与方法 -> 值传递调用]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Object9 {
    static void toHello(String s) {
        System.out.println(s);
        s = "Hello";
        System.out.println(s);
    }

    public static void main(String[] args) {
        String hi = "Hi";
        toHello(hi);
        System.out.println(hi);
    }
}
