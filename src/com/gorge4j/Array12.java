package com.gorge4j;

/**
 * @Title: Array12.java
 * @Description: [数组 -> 三维数组]
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class Array12 {
    public static void main(String[] args) {
        int[][][] a = new int[2][][];
        a[0] = new int[2][];
        a[1] = new int[3][];
        a[0][0] = new int[4];
        a[0][1] = new int[4];
        a[1][0] = new int[3];
        a[1][1] = new int[3];
        a[1][2] = new int[3];

        a[0][0][0] = 10;
        a[1][1][1] = 20;
        System.out.println(a.length);
        System.out.println(a[1].length);
        System.out.println(a[1][2].length);
        System.out.println(a[1][1][1]);
    }
}
