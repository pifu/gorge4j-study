package com.gorge4j;

import java.io.*;
import com.gorge4j.util.FileUtil;

/**
 * @Title: File14.java
 * @Description: [Java输入与输出 -> 流 -> 字符流 -> FileReader与FileWriter] | BufferedReader 使用
 * @Copyright: © 2019 ***
 * @Company: ***有限公司
 *
 * @author gorge.guo
 * @date 2019-10-06 11:27:23
 * @version v1.0
 */

public class File14 {
    public static void main(String[] args) throws Exception {
        String filePath = FileUtil.generateFilePathByFileName("autoexec.txt"); // 文件路径
        FileReader fr = new FileReader(filePath);
        BufferedReader br = new BufferedReader(fr);
        String s;
        while ((s = br.readLine()) != null) {
            System.out.println(s);
        }
        br.close();
    }
}
